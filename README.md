This is a [Next.js](https://nextjs.org/) project.
DevelopTeam: [TopOnTech](https://topon.tech/).
Support: [Chien.Steven](https://www.facebook.com/phanvanchien.97/) - 0816.652.685.

## Installation

First, you must clone source code from [Bitbucket](https://bitbucket.org/topontech/fe.money24h/src/master/).
Open Git Bash or any terminal and run this:

```bash
git clone https://bitbucket.org/topontech/fe.money24h/src/master/
# or
git clone https://[user]@bitbucket.org/topontech/fe.money24h.git
```

By default, this source will be in branch Master. To continue develop or run this project in your local, you must checkout to branch Develop:

```bash
git checkout develop
```

After, you must have enough package to run this project. Don't worry, all package will be in package.json, to install them, you just need to use the following command:

```bash
yarn install
# or
npm install
```
## Getting Started

Now, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:2707](http://localhost:2707) with your browser to see the result.

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

To learn more about Express.js, take a look at the following resources:

- [Express.js Documentation](http://expressjs.com/) - learn about Express.js features.
- [Custom server](https://nextjs.org/docs/advanced-features/custom-server) - learn about Custom server in Next.js App.

## Warning

- In this project, branch Mater only use for [Production environment](https://money24h.vn/), DO NOT push code from local to this branch.
- Branch Develop is used to develop, if you work with team, you must checkout new branch from here and create pull request from your local branch to branch Develop.
- Contact your Leader or anyone who have permission to merge your local branch.
- When code in branch Develop have up to [Staging environment](https://money24h.toponseek.com/) and pass testcase, you will create pull request from branch Develop to branch Master, and deploy it to [Production environment](https://money24h.vn/).