import { GetServerSideProps, GetServerSidePropsContext } from 'next';
import { fetchAPI, fetchApiCms } from './api';

const CustomGenerateSitemap = () => {};

export const getServerSideProps: GetServerSideProps = async (context: GetServerSidePropsContext) => {

  const [
    dataNewestPost,
    dataInterestRate,
    dataLoanRate,
    dataCurrency
  ] = await Promise.all([
    fetchApiCms("posts/get_basic_lastest_posts", `&total_post=200`),
    fetchAPI("/money24h/interest-save"),
    fetchAPI("/money24h/borrow-rate"),
    fetchAPI(`/coincotroller/list-coin?Start=1&Limit=50&Convert=usd`),
  ])
  
  let newestPost = null
  let interestRateData = null

  if(dataNewestPost && dataNewestPost.posts && dataNewestPost.posts.length > 0) {
    newestPost = dataNewestPost.posts
  }
  if(dataInterestRate && dataInterestRate.data && dataInterestRate.data.length > 0) {
    interestRateData = dataInterestRate.data
  }

  const dataBorrow = dataLoanRate && dataLoanRate.success ? dataLoanRate.data : null;

  const formatDate = (date) => {
    let year = date.getFullYear();
    let month = date.getMonth() + 1 < 10 ? `0${date.getMonth() + 1}` : date.getMonth() + 1;
    let day = date.getDate() < 10 ? `0${date.getDate()}` : date.getDate();

    return `${year}-${month}-${day}`;
  }

  const genrateNewestPost = (newestPost) => {
    let temp = ``
    if(newestPost) {
      newestPost.map((newestPostItem) => {
        temp += `
          <url>
            <changefreq>daily</changefreq>
            <priority>0.8</priority>
            <loc>https://money24h.vn/blog/${newestPostItem.slug}/</loc>
            <lastmod>${formatDate(new Date())}</lastmod>
          </url>
        `
      })
    }
    return temp
  }

  const genrateInterestRate = (interestRateData) => {
    let temp = `
      <url>
        <changefreq>daily</changefreq>
        <priority>0.8</priority>
        <loc>https://money24h.vn/lai-suat-gui-tiet-kiem-ngan-hang/</loc>
        <lastmod>${formatDate(new Date())}</lastmod>
      </url>
    `
    if(interestRateData) {
      interestRateData.map((interestRateItem) => {
        temp += `
          <url>
            <changefreq>daily</changefreq>
            <priority>0.8</priority>
            <loc>https://money24h.vn/lai-suat-gui-tiet-kiem-${interestRateItem.slug}/</loc>
            <lastmod>${formatDate(new Date())}</lastmod>
          </url>
        `
      })
    }
    return temp
  }
  
  const genrateLoanRate = (loanRateData) => {
    let temp = `
      <url>
        <changefreq>daily</changefreq>
        <priority>0.8</priority>
        <loc>https://money24h.vn/lai-suat-vay-ngan-hang/</loc>
        <lastmod>${formatDate(new Date())}</lastmod>
      </url>
    `
    if(loanRateData) {
      loanRateData.map((loanRateItem) => {
        temp += `
          <url>
            <changefreq>daily</changefreq>
            <priority>0.8</priority>
            <loc>https://money24h.vn/lai-suat-vay-ngan-hang-${loanRateItem.slug}/</loc>
            <lastmod>${formatDate(new Date())}</lastmod>
          </url>
        `
      })
    }
    return temp
  }

  const genrateCurrency = (currencyData) => {
    let temp = `
      <url>
        <changefreq>daily</changefreq>
        <priority>0.8</priority>
        <loc>https://money24h.vn/tien-ao/</loc>
        <lastmod>${formatDate(new Date())}</lastmod>
      </url>
    `
    if(currencyData) {
      currencyData.data.map((currencyItem) => {
        temp += `
          <url>
            <changefreq>daily</changefreq>
            <priority>0.8</priority>
            <loc>https://money24h.vn/tien-ao/${currencyItem.symbol}/</loc>
            <lastmod>${formatDate(new Date())}</lastmod>
          </url>
        `
      })
    }
    return temp
  }

  const genrateSitemap = async () => {

    const [
      newestPostXML,
      interestRateXML,
      loanRateXML,
      currencyXML
    ] = await Promise.all([
      genrateNewestPost(newestPost),
      genrateInterestRate(interestRateData),
      genrateLoanRate(dataBorrow),
      genrateCurrency(dataCurrency.data)
    ])

    return `<?xml version="1.0" encoding="UTF-8"?>
      <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
        <url>
          <changefreq>daily</changefreq>
          <priority>0.8</priority>
          <loc>https://money24h.vn/</loc>
          <lastmod>${formatDate(new Date())}</lastmod>
        </url>
        ${newestPostXML}
        ${interestRateXML}
        ${loanRateXML}
        ${currencyXML}
      </urlset>
    `;
  }

  let test = await genrateSitemap()

  if(test) {
    context.res.setHeader("Content-Type", "text/xml");
    context.res.write(test);
    context.res.end();
  }

  return {
    props: {
      
    },
  };
};

export default CustomGenerateSitemap;
