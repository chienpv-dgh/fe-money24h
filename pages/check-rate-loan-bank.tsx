import Head from 'next/head';
import { GetServerSideProps } from 'next';
import { fetchAPI, fetchApiCms } from './api';
import { CheckLoanRateBank } from '../components/CheckLoanRateBank';

export default function CheckLoanRateBankPage({ 
  lastestPost, 
  listBankOps, 
  loanRateAllBank,
  dataTableBank, 
  loanRateByBank, 
  postOfPage, 
  detalNewsPost,
  stockCode ,
  dataCrypto
}) {
  
  const formatDateTime = () => {
    
    let date = new Date()
    let getMonth = date.getMonth() + 1
    let getFullyear = date.getFullYear()

    return `${getMonth}/${getFullyear}`
  }

  return (
    <>
      <Head>
        <title>{loanRateByBank.length > 0 ? `Lãi suất vay ${loanRateByBank[0].bankName} Tháng ${formatDateTime()} | Money24h` : `Lãi suất vay ngân hàng Tháng ${formatDateTime()} | Money24h`}</title>
        <meta name="description" content={loanRateByBank.length > 0 ? `Xem lãi suất vay ${loanRateByBank[0].bankName} mới nhất tháng ${formatDateTime()}. Vay thế chấp, vay tín chấp, vay trả góp, vay mua nhà, vay vốn kinh doanh,...` : `Lãi suất vay ngân hàng tháng ${formatDateTime()} của ngân hàng nào tốt nhất? Bảng tra cứu so sánh lãi suất vay tốt nhất sau đây!`}></meta>
        <link rel="icon" href="/logos/favicon.ico"></link>
        <link rel="canonical" href={loanRateByBank.length > 0 ? `https://money24h.vn/lai-suat-vay-${loanRateByBank[0].slug}` : "https://money24h.vn/lai-suat-vay-ngan-hang"}></link>
        <meta property="og:site_name" content="Money24H"></meta>
        <meta property="og:type" content="website" />
        <meta property="og:title" content={loanRateByBank.length > 0 ? `Lãi suất vay ${loanRateByBank[0].bankName} Tháng ${formatDateTime()} | Money24h` : `Lãi suất vay ngân hàng Tháng ${formatDateTime()} | Money24h`} />
        <meta property="og:description" content={loanRateByBank.length > 0 ? `Xem lãi suất vay ${loanRateByBank[0].bankName} mới nhất tháng ${formatDateTime()}. Vay thế chấp, vay tín chấp, vay trả góp, vay mua nhà, vay vốn kinh doanh,...` : `Lãi suất vay ngân hàng tháng ${formatDateTime()} của ngân hàng nào tốt nhất? Bảng tra cứu so sánh lãi suất vay tốt nhất sau đây!`} />
        <meta property="og:url" content={loanRateByBank.length > 0 ? `https://money24h.vn/lai-suat-vay-${loanRateByBank[0].slug}` : "https://money24h.vn/lai-suat-vay-ngan-hang"} />
        <meta property="og:image" content={"/images/finance-banner.png"} />
      </Head> 
      <CheckLoanRateBank lastestPost={lastestPost} listBankOps={listBankOps} loanRateAllBank={loanRateAllBank} dataTableBank={dataTableBank} loanRateByBank={loanRateByBank} stockCode={stockCode} detalNewsPost={detalNewsPost} dataCrypto={dataCrypto}/>
    </>
  )
}

export const getServerSideProps: GetServerSideProps = async (context: {req}) => {
  
  const [
    lastestPost,
    dataListBank,
    loanRatelBank,
    // postOfPage,
    webSetting,
    resDataBorrow,
    listBank,
    listCurrency,
    resDataCrypto
  ] = await Promise.all([
    fetchApiCms("posts/get_newest_post", `&posts_per_page=5&page=1&offset=0`),
    fetchAPI("/money24h/get-list-bank?Type=borrow"),
    fetchAPI("/money24h/borrow-rate"),
    // fetchApiCms("posts/get_post_detail", "&slug=cach-tinh-lai-suat-vay-ngan-hang-nhanh-chong-don-gian-2"),
    fetchApiCms("setting/websetting", ""),
    fetchAPI("/money24h/borrow-rate"),
    fetchAPI("/money24h/interest-save"),
    fetchAPI(`/coincotroller/list-coin?Start=1&Limit=50&Convert=usd`),
    fetchAPI(`/coincotroller/sort-coin?Limit=10&Sort_dir=desc&Convert=vnd`)
  ])

  let originalUrl = context.req.originalUrl
  
  let getParam = originalUrl.replace("/lai-suat-vay-", "")


  let loanRateByBank = null

  let listBankOps = null
  if (dataListBank.success) {
    listBankOps = dataListBank.data
  }

  let loanRateAllBank = null
  if (loanRatelBank.success) {
    loanRateAllBank = loanRatelBank.data
    loanRateByBank = loanRateAllBank.filter((item) => item.slug == getParam)
  }

  let postOfPage = null
  let detalNewsPost = null

  if(loanRateByBank.length > 0) {
    let idPost = loanRateByBank[0].idPost
    if(idPost) {
      let dataDetailPost = await fetchApiCms("posts/get_post_detail", `&slug=${idPost}`)
      if(dataDetailPost){
        detalNewsPost = dataDetailPost
      }
    }
  } else {
    let dataDetailPost = await fetchApiCms("posts/get_post_detail", `&slug=${"cach-tinh-lai-suat-vay-ngan-hang-nhanh-chong-don-gian-2"}`)
    if(dataDetailPost){
      detalNewsPost = dataDetailPost
    }
  }

  let dataTableBank = null
  let stockCode = null

  if (loanRateByBank && loanRateByBank.length > 0) {
    dataTableBank = loanRateByBank
    stockCode = loanRateByBank[0].share
  } else {
    dataTableBank = loanRateAllBank
  }
  
  const dataBorrow = resDataBorrow && resDataBorrow.success ? resDataBorrow.data : null;
  const dataCrypto = resDataCrypto && resDataCrypto.success ? resDataCrypto.data : null;
  return {
    props: { 
      lastestPost: lastestPost.posts, 
      listBankOps, 
      loanRateAllBank, 
      loanRateByBank, 
      dataTableBank, 
      postOfPage, 
      detalNewsPost,
      stockCode,
      webSetting,
      listBank,
      listCurrency,
      dataBorrow,
      dataCrypto
    },
  }
}