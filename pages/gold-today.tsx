import { GetServerSideProps } from 'next';
import Head from 'next/head';
import GoldTodayComponent from '../components/GoldToday';
import { fetchAPI, fetchApiCms } from './api';
import { decode } from 'html-entities';

export default function PostPage({ 
  dataInterest, 
  tableVietlot, 
  dataExchange, 
  dataBorrow, 
  detailPost,
  newestPost,
  dataPostCustomCate,
  postOfPage
}) {

  const formatDateTime = () => {
    
    let date = new Date()
    let getDate = date.getDate()
    let getMonth = date.getMonth() + 1
    let getFullyear = date.getFullYear()

    return `${getDate}/${getMonth}/${getFullyear}`
  }

  return (
    <>
      <div>
        <Head>
          <title>{`Giá vàng hôm nay ${formatDateTime()} | Money24h`}</title>
          <meta name="description" content={decode(postOfPage.detail_post ? postOfPage.detail_post.seo_desc : "Giá vàng hôm nay vàng DOJI, giá vàng SJC, giá vàng Phú Quý, Bảo Tín Minh Châu, giá vàng 9999, giá vàng 24k")}></meta>
          <link rel="icon" href="/logos/favicon.ico"></link>
          <link rel="canonical" href={`https://money24h.vn/gia-vang-hom-nay`}></link>
          <meta property="og:site_name" content="Money24H"></meta>
          <meta property="og:type" content="website" />
          <meta property="og:title" content={`Giá vàng hôm nay ${formatDateTime()} | Money24h`} />
          <meta property="og:description" content={decode(postOfPage.detail_post ? postOfPage.detail_post.seo_desc : "Giá vàng hôm nay vàng DOJI, giá vàng SJC, giá vàng Phú Quý, Bảo Tín Minh Châu, giá vàng 9999, giá vàng 24k")} />
          <meta property="og:url" content={`https://money24h.vn/gia-vang-hom-nay`} />
          <meta property="og:image" content={"/images/image-gold-page.jpg"} />
        </Head>
        <GoldTodayComponent 
          dataInterest={dataInterest}
          tableVietlot={tableVietlot}
          dataExchange={dataExchange}
          dataBorrow={dataBorrow}
          detailPost={detailPost}
          newestPost={newestPost}
          dataPostByCate={dataPostCustomCate}
          postOfPage={postOfPage.detail_post}
        />
      </div>
    </>
  )
}

export const getServerSideProps: GetServerSideProps = async (ctx) => {

  const [
    newestPost,
    resDataInterest,
    resTableVietlot,
    resDataExchange,
    resDataBorrow,
    webSetting,
    dataPostCustomCate,
    listBank,
    listCurrency,
  ] = await Promise.all([
    fetchApiCms("posts/get_newest_post", `&posts_per_page=5&page=1&offset=0`),
    fetchAPI("/money24h/interest-save-vnex"),
    fetchAPI("/money24h/get-all-reward"),
    fetchAPI('/money24h/exchange-rate-by-bankid?BankId=1'),
    fetchAPI("/money24h/borrow-rate"),
    fetchApiCms("setting/websetting", ""),
    fetchApiCms("posts/get_post_by_cat", `&category_slug=${"vang"}&posts_per_page=20&page=1&offset=0`),
    fetchAPI("/money24h/interest-save"),
    fetchAPI(`/coincotroller/list-coin?Start=1&Limit=50&Convert=usd`),
  ])

  const detailPost = null

  const dataInterest = resDataInterest && resDataInterest.success ? resDataInterest.data : null;
  const tableVietlot = resTableVietlot && resTableVietlot.success ? resTableVietlot.data : null;
  const dataExchange = resDataExchange && resDataExchange.success ? resDataExchange.data : null;
  const dataBorrow = resDataBorrow && resDataBorrow.success ? resDataBorrow.data : null;
  let isBank = false
  let dataBank = null

  let postOfPage = null

  if(dataPostCustomCate && dataPostCustomCate.posts && dataPostCustomCate.posts.length > 0) {
    let res = await fetchApiCms("posts/get_post_detail", `&slug=${dataPostCustomCate.posts[0].slug}`)
    if(res){
      postOfPage = res
    }
  }

  return { 
    props: { 
      dataBank, 
      isBank, 
      dataInterest, 
      tableVietlot,
      dataExchange,
      dataBorrow,
      detailPost,
      webSetting,
      newestPost,
      dataPostCustomCate,
      postOfPage,
      listBank,
      listCurrency
    } 
  }
}