export function getApiTOT(path = "") {
  return `https://apimoney24h.toponseek.com${path}`
}

export async function fetchAPI(path) {
  const requestUrl = getApiTOT(path);
  console.log(requestUrl)

  const response = await fetch(requestUrl, {
    method: 'GET',
    headers: {
      'ApiKey': 'ajsfldkaj8483*$#&$*BBFHJDHJS',
    }
  });
  if(response.status >= 400) {
    return null
  } else {
    const data = await response.json();
    return data;
  }
}

export function getApiCms(path, query?) {
  let originApi = process.env.API_HOST
  let apiKey = process.env.API_KEY
  return `${originApi}/${path}?key=${apiKey}${query}`
}

export async function fetchApiCms(path, query?) {
  const requestUrl = getApiCms(path, query);
  console.log(requestUrl)

  const response = await fetch(requestUrl);
  if(response.status >= 400) {
    return null;
  } else {
    const data = await response.json();
    return data;
  }
}