import Head from 'next/head';
import { GetServerSideProps } from 'next';
import { fetchAPI, fetchApiCms } from '../api';
import dynamic from "next/dynamic";

const TagDetailPage = dynamic(() => import("../../components/TagDetail/tag-detail-page"));

export default function TagPage({  
  dataInterest, 
  tableVietlot, 
  tagPage, 
  dataPost,
  dataCrypto
}) {

  return (
    <>
      <Head>
        <title>{dataPost ? dataPost.category.seo_title ? `${dataPost.category.seo_title} | Money24h` : "Money24H - Tin Thị Trường Tài Chính Ngân Hàng Mới Nhất 24H Qua" : "Money24H - Tin Thị Trường Tài Chính Ngân Hàng Mới Nhất 24H Qua"}</title>
        <meta name="description" content={dataPost && dataPost.category.seo_desc ? dataPost.category.seo_desc : `Money24h cập nhật thông tin tài chính, tiền điện tử, lãi suất ngân hàng và ${dataPost.category.cat_name} mới nhất. Xem ngay!`}></meta>
        <link rel="icon" href="/logos/favicon.ico"></link>
        <link rel="canonical" href={dataPost ? `https://money24h.vn/tag/${dataPost.category.slug}` : "https://money24h.vn/"}></link>
        <meta property="og:site_name" content="Money24H"></meta>
        <meta property="og:type" content="website" />
        <meta property="og:title" content={dataPost ? dataPost.category.seo_title ? `${dataPost.category.seo_title} | Money24h` : "Money24H - Tin Thị Trường Tài Chính Ngân Hàng Mới Nhất 24H Qua" : "Money24H - Tin Thị Trường Tài Chính Ngân Hàng Mới Nhất 24H Qua"} />
        <meta property="og:description" content={dataPost && dataPost.category.seo_desc ? dataPost.category.seo_desc : `Money24h cập nhật thông tin tài chính, tiền điện tử, lãi suất ngân hàng và ${dataPost.category.cat_name} mới nhất. Xem ngay!`} />
        <meta property="og:url" content={dataPost ? `https://money24h.vn/tag/${dataPost.category.slug}` : "https://money24h.vn/"} />
        <meta property="og:image" content={dataPost ? dataPost.category.feature_image ? dataPost.category.feature_image : "/images/finance-banner.png" : "/images/finance-banner.png"} />
      </Head>
      <TagDetailPage dataInterest={dataInterest} tableVietlot={tableVietlot} tagPage={tagPage} dataPost={dataPost} dataCrypto={dataCrypto}/>
    </>
  )
}

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const [
    resDataInterest,
    resTableVietlot,
    dataPost,
    webSetting,
    listBank,
    listCurrency,
    resDataBorrow,
    resDataCrypto
  ] = await Promise.all([
    fetchAPI("/money24h/interest-save-vnex"),
    fetchAPI("/money24h/get-all-reward"),
    fetchApiCms("posts/get_post_by_cat", `&category_slug=${ctx.query.slug}&posts_per_page=20&page=1&offset=0`),
    fetchApiCms("setting/websetting", ""),
    fetchAPI("/money24h/interest-save"),
    fetchAPI(`/coincotroller/list-coin?Start=1&Limit=50&Convert=usd`),
    fetchAPI("/money24h/borrow-rate"),
    fetchAPI(`/coincotroller/sort-coin?Limit=10&Sort_dir=desc&Convert=vnd`)
  ])

  let tagPage = ctx.query.slug

  const dataInterest = resDataInterest && resDataInterest.success ? resDataInterest.data : null;
  const tableVietlot = resTableVietlot && resTableVietlot.success ? resTableVietlot.data : null;
  const dataBorrow = resDataBorrow && resDataBorrow.success ? resDataBorrow.data : null;
  const dataCrypto = resDataCrypto && resDataCrypto.success ? resDataCrypto.data : null;
  return {
    props: { 
      dataInterest, 
      tableVietlot, 
      tagPage, 
      dataPost,
      webSetting,
      listBank,
      listCurrency,
      dataBorrow,
      dataCrypto
    },
  }
}