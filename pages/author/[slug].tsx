import { GetServerSideProps } from 'next';
import Head from 'next/head';
import { fetchAPI, fetchApiCms } from '../api';
import dynamic from "next/dynamic";

const AuthorDetail = dynamic(() => import("../../components/AuthorDetail/author-page"));

export default function AuthorPage({  
  dataInterest, 
  tableVietlot,
  dataPostAuthor,
  authorSlug 
}) {

  return (
    <>
      <Head>
        <title>{dataPostAuthor && dataPostAuthor.author_object.display_name ? `Tổng hợp các bài viết của ${dataPostAuthor.author_object.display_name} | Money24h` : "Money24H - Tin Thị Trường Tài Chính Ngân Hàng Mới Nhất 24H Qua"}</title>
        <meta name="description" content={dataPostAuthor && dataPostAuthor.author_object.user_description ? dataPostAuthor.author_object.user_description : `Money24h cập nhật thông tin tài chính, tiền điện tử, lãi suất ngân hàng từ ${dataPostAuthor.author_object.display_name} mới nhất. Xem ngay!`}></meta>
        <link rel="icon" href="/logos/favicon.ico"></link>
        <link rel="canonical" href={dataPostAuthor ? `https://money24h.vn/author/${authorSlug}` : "https://money24h.vn/" }></link>
        <meta property="og:site_name" content="Money24H"></meta>
        <meta property="og:type" content="website" />
        <meta property="og:title" content={dataPostAuthor && dataPostAuthor.author_object.display_name ? `Tổng hợp các bài viết của ${dataPostAuthor.author_object.display_name} | Money24h` : "Money24H - Tin Thị Trường Tài Chính Ngân Hàng Mới Nhất 24H Qua"} />
        <meta property="og:description" content={dataPostAuthor && dataPostAuthor.author_object.user_description ? dataPostAuthor.author_object.user_description : `Money24h cập nhật thông tin tài chính, tiền điện tử, lãi suất ngân hàng từ ${dataPostAuthor.author_object.display_name} mới nhất. Xem ngay!`} />
        <meta property="og:url" content={dataPostAuthor ? `https://money24h.vn/author/${authorSlug}` : "https://money24h.vn/" } />
        <meta property="og:image" content={dataPostAuthor ? dataPostAuthor.author_object.avatar ? dataPostAuthor.author_object.avatar : "/images/finance-banner.png" : "/images/finance-banner.png"} />
      </Head>
      <AuthorDetail 
        dataInterest={dataInterest} 
        tableVietlot={tableVietlot} 
        dataPostAuthor={dataPostAuthor}
        authorSlug={authorSlug}
      />
    </>
  )
}

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  
  const [
    resDataInterest,
    resTableVietlot,
    dataPostAuthor,
    webSetting,
    listBank,
    listCurrency,
    resDataBorrow
  ] = await Promise.all([
    fetchAPI("/money24h/interest-save-vnex"),
    fetchAPI("/money24h/get-all-reward"),
    fetchApiCms("posts/get_post_by_author", `&author_slug=${ctx.query.slug}&posts_per_page=20&page=1&offset=0`),
    fetchApiCms("setting/websetting", ""),
    fetchAPI("/money24h/interest-save"),
    fetchAPI(`/coincotroller/list-coin?Start=1&Limit=50&Convert=usd`),
    fetchAPI("/money24h/borrow-rate"),
  ])

  const dataInterest = resDataInterest && resDataInterest.success ? resDataInterest.data : null;
  const tableVietlot = resTableVietlot && resTableVietlot.success ? resTableVietlot.data : null;
  let authorSlug = ctx.query.slug
  const dataBorrow = resDataBorrow && resDataBorrow.success ? resDataBorrow.data : null;

  return {
    props: { 
      dataInterest, 
      tableVietlot,
      dataPostAuthor,
      authorSlug,
      webSetting,
      listBank,
      listCurrency,
      dataBorrow
    },
  }
}