import { GetStaticProps } from "next";
import { fetchAPI, fetchApiCms } from "./api";

function Error({ }) {

  return (
    <div className="page-404-content">
      <img src="/images/image-404-page.png" alt="404 NotFound" width="100%" />
    </div>
  )
}

export default Error

export const getStaticProps: GetStaticProps = async () => { 

  const [
    resDataBorrow,
    webSetting,
    listBank,
    listCurrency
  ] = await Promise.all([
    fetchAPI("/money24h/borrow-rate"),
    fetchApiCms("setting/websetting", ""),
    fetchAPI("/money24h/interest-save"),
    fetchAPI(`/coincotroller/list-coin?Start=1&Limit=50&Convert=usd`),
  ])

  const dataBorrow = resDataBorrow && resDataBorrow.success ? resDataBorrow.data : null;
  
  return {
    props: {
      webSetting,
      listBank,
      listCurrency,
      dataBorrow
    }
  }
}