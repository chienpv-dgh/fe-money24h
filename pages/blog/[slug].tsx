import Head from 'next/head';
import { GetServerSideProps, GetServerSidePropsContext } from 'next';
import { fetchAPI, fetchApiCms } from '../api';
import { decode } from 'html-entities';
import NewsDetail from '../../components/modules/NewsDetail';

export interface PostDetailPageProps {
  dataBank: any;
  isBank: boolean;
  slugOfPost: string;
  dataInterest: {
    offline: {
      bankName: string;
      months1?: string;
      months3?: string;
      months6?: string;
      months9?: string;
      months12?: string;
      slug: string;
      type: number;
    }[];
    online: {
      bankName: string;
      months1?: string;
      months3?: string;
      months6?: string;
      months9?: string;
      months12?: string;
      slug: string;
      type: number;
    }[];
  };
  tableVietlot: {
    description: string;
    name: string;
    period: string;
    reward: number;
  }[];
  dataBorrow: {
    bankName: string;
    detail: string;
    idPost: string;
    months: string;
    results: {
      result?: string;
    };
    share?: string;
    slug: string;
  }[];
  dataExchange: {
    bankName: string;
    idPost: string;
    results: {
      cash: string;
      flagCash?: number;
      flagSell?: number;
      flagTrans?: number;
      fullSlug: string;
      moneyCode: string;
      moneyName: string;
      sellRate: string;
      share?: string;
      transfer: string;
    }[];
    share?: string;
    slug: string;
  };
  detailPost: any;
  dataCrypto: any;
}

export default function PostPage({ 
  dataBank, 
  isBank, 
  slugOfPost, 
  dataInterest, 
  tableVietlot, 
  dataExchange, 
  dataBorrow, 
  detailPost,
  dataCrypto
}: PostDetailPageProps) {

  return (
    <>
      <div>
        <Head>
          <script type="application/ld+json" dangerouslySetInnerHTML={{
            __html: `{
              "@context": "https://schema.org",
              "@type": "BreadcrumbList",
              "itemListElement": [{
                "@type": "ListItem",
                "position": 1,
                "name": "Trang chủ",
                "item": "https://money24h.vn"
              },{
                "@type": "ListItem",
                "position": 2,
                "name": "${detailPost && detailPost.detail_post.category && detailPost.detail_post.category.length > 0 ? detailPost.detail_post.category[0].name : ""}",
                "item": "https://money24h.vn/${detailPost && detailPost.detail_post.category && detailPost.detail_post.category.length > 0 ? detailPost.detail_post.category[0].slug : ""}"
              },{
                "@type": "ListItem",
                "position": 3,
                "name": "${detailPost && detailPost.detail_post.title}"
              }]
            }`
          }}></script>
          <script type="application/ld+json" dangerouslySetInnerHTML={{
            __html: `{
              "@context": "https://schema.org",
              "@type": "NewsArticle",
              "headline": "${detailPost && detailPost.detail_post && detailPost.detail_post.title}",
              "description": "${detailPost && detailPost.detail_post && detailPost.detail_post.seo_desc}",
              "url": "${`https://money24h.vn/blog/${detailPost && detailPost.detail_post.slug}/`}",
              "inLanguage": "vi",
              "image": [
                "${detailPost && detailPost.detail_post && detailPost.detail_post.feature_image ? detailPost.detail_post.feature_image : "/images/finance-banner.png"}"
              ],
              "datePublished": "${detailPost && detailPost.detail_post && detailPost.detail_post.publish_date ? new Date(detailPost.detail_post.publish_date.split("/").reverse().join("-")) : new Date()}",
              "dateModified": "${detailPost && detailPost.detail_post && detailPost.detail_post.publish_date ? new Date(detailPost.detail_post.publish_date.split("/").reverse().join("-")) : new Date()}",
              "author": {
                "@type": "Person",
                "name": "${detailPost && detailPost.detail_post && detailPost.detail_post.author && detailPost.detail_post.author.name}",
                "url": "https://money24h.vn/author/${detailPost && detailPost.detail_post && detailPost.detail_post.author && detailPost.detail_post.author.slug}"
              },
              "mainEntityOfPage": {
                "@type": "WebPage",
                "@id": "https://money24h.vn/"
              },
              "articleBody": "${detailPost && detailPost.detail_post && detailPost.detail_post.excerpt}",
              "publisher": {
                "@type": "Organization",
                "name": "Money24H",
                "logo": {
                  "@type": "ImageObject",
                  "url": "https://money24h.vn/logos/header-logo.png"
                }
              }
            }`
          }}></script>
          <script type="application/ld+json" dangerouslySetInnerHTML={{
            __html: `{
              "@context": "https://schema.org/",
              "@type": "CreativeWorkSeries",
              "name": "${detailPost && detailPost.detail_post && detailPost.detail_post.title}",
              "aggregateRating": {
                "@type": "AggregateRating",
                "ratingValue": "${detailPost && detailPost.detail_post && detailPost.detail_post.rating && detailPost.detail_post.rating > 0 ? detailPost.detail_post.rating : 5}",
                "ratingCount": "${detailPost && detailPost.detail_post && detailPost.detail_post.count_rating && detailPost.detail_post.count_rating > 0 ? detailPost.detail_post.count_rating : 2}"
              }
            }`
          }}></script>
          <title>{decode(`${detailPost && detailPost.detail_post.seo_title ? detailPost.detail_post.seo_title : detailPost.detail_post.title} | Money24h`)}</title>
          <meta name="description" content={detailPost && detailPost.detail_post.seo_desc ? detailPost.detail_post.seo_desc : detailPost.detail_post.excerpt || "Tin HOT nhất 24h qua! Cập nhật thông tin tài chính, ngân hàng, chứng khoán, tiền điện tử hôm nay cùng Money24h."}></meta>
          <link rel="icon" href="/logos/favicon.ico"></link>
          <link rel="canonical" href={`https://money24h.vn/blog/${detailPost && detailPost.detail_post.slug}/`}></link>
          <meta property="og:site_name" content="Money24H"></meta>
          <meta property="og:type" content="website" />
          <meta property="og:title" content={`${detailPost && detailPost.detail_post.seo_title ? detailPost.detail_post.seo_title : detailPost.detail_post.title} | Money24h` || "Money24H - Tin Thị Trường Tài Chính Ngân Hàng Mới Nhất 24H Qua"} />
          <meta property="og:description" content={detailPost && detailPost.detail_post.seo_desc ? detailPost.detail_post.seo_desc : detailPost.detail_post.excerpt || "Tin HOT nhất 24h qua! Cập nhật thông tin tài chính, ngân hàng, chứng khoán, tiền điện tử hôm nay cùng Money24h."} />
          <meta property="og:url" content={`https://money24h.vn/blog/${detailPost && detailPost.detail_post.slug}/`} />
          <meta property="og:image" content={detailPost && detailPost.detail_post.feature_image ? detailPost.detail_post.feature_image : "/images/image-404-page.png"} />
        </Head>
        <NewsDetail 
          dataBank={dataBank} 
          isBank={isBank} 
          slugOfPost={slugOfPost}
          dataInterest={dataInterest} 
          tableVietlot={tableVietlot} 
          dataExchange={dataExchange}
          dataBorrow={dataBorrow}
          detailPost={detailPost}
          dataCrypto = {dataCrypto}
        />
      </div>
    </>
  )
}

export const getServerSideProps: GetServerSideProps = async (context: GetServerSidePropsContext) => {
  const [
    resDataInterest,
    resTableVietlot,
    resDataExchange,
    resDataBorrow,
    detailPost,
    webSetting,
    listBank,
    listCurrency,
    resDataCrypto
  ] = await Promise.all([
    fetchAPI("/money24h/interest-save-vnex"),
    fetchAPI("/money24h/get-all-reward"),
    fetchAPI('/money24h/exchange-rate-by-bankid?BankId=1'),
    fetchAPI("/money24h/borrow-rate"),
    fetchApiCms("posts/get_post_detail", `&slug=${encodeURIComponent(context.query.slug.toString())}`),
    fetchApiCms("setting/websetting", ""),
    fetchAPI("/money24h/interest-save"),
    fetchAPI(`/coincotroller/list-coin?Start=1&Limit=50&Convert=usd`),
    fetchAPI(`/coincotroller/sort-coin?Limit=10&Sort_dir=desc&Convert=vnd`)
  ])

  const dataInterest = resDataInterest && resDataInterest.success ? resDataInterest.data : null;
  const tableVietlot = resTableVietlot && resTableVietlot.success ? resTableVietlot.data : null;
  const dataExchange = resDataExchange && resDataExchange.success ? resDataExchange.data : null;
  const dataBorrow = resDataBorrow && resDataBorrow.success ? resDataBorrow.data : null;
  const dataCrypto = resDataCrypto && resDataCrypto.success ? resDataCrypto.data : null;
  let isBank = false
  let dataBank = null

  let slugOfPost = context.query.slug

  if (detailPost && detailPost.detail_post && detailPost.detail_post.category && detailPost.detail_post.category.length > 0 && detailPost.detail_post.category[0].slug == "thong-tin-ngan-hang") {
    isBank = true
    let res = await fetchAPI("/money24h/get-list-bank?Type=post")
    if (res.success) {
      dataBank = res.data
    }
  }
  
  if(detailPost && !detailPost.detail_post) {
    return {
      notFound: true
    }
  }

  return { 
    props: { 
      dataBank, 
      isBank, 
      slugOfPost, 
      dataInterest, 
      tableVietlot,
      dataExchange,
      dataBorrow,
      detailPost,
      webSetting,
      listBank,
      listCurrency,
      dataCrypto
    }
  }
}
