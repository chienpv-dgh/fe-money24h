import { GetServerSideProps, GetServerSidePropsContext } from 'next';
import Head from 'next/head';
import CryptoCurrencyPageComponent from '../components/CryptoCurrency';
import { fetchAPI, fetchApiCms } from './api';


export interface quoteObject{
  fully_diluted_market_cap: number;
  last_updated: string;
  market_cap: number;
  market_cap_dominance: number;
  percent_change_1h: number;
  percent_change_7d: number;
  percent_change_24h: number;
  percent_change_30d: number;
  percent_change_60d: number;
  percent_change_90d: number;
  price: number;
  volume_24h: number;
  volume_change_24h: number;
}
export interface CryptoCurrencyPageProps {
  dataInterest: {
    offline: {
      bankName: string;
      months1?: string;
      months3?: string;
      months6?: string;
      months9?: string;
      months12?: string;
      slug: string;
      type: number;
    }[];
    online: {
      bankName: string;
      months1?: string;
      months3?: string;
      months6?: string;
      months9?: string;
      months12?: string;
      slug: string;
      type: number;
    }[];
  };
  tableVietlot: {
    description: string;
    name: string;
    period: string;
    reward: number;
  }[];
  dataBorrow: {
    bankName: string;
    detail: string;
    idPost: string;
    months: string;
    results: {
      result?: string;
    };
    share?: string;
    slug: string;
  }[];
  dataExchange: {
    bankName: string;
    idPost: string;
    results: {
      cash: string;
      flagCash?: number;
      flagSell?: number;
      flagTrans?: number;
      fullSlug: string;
      moneyCode: string;
      moneyName: string;
      sellRate: string;
      share?: string;
      transfer: string;
    }[];
    share?: string;
    slug: string;
  };
  currentPage: number;
  currencyDataEng: {
    status: {
      timestamp: string;
      total_count: number;
    };
    data: {
      circulating_supply: number;
      cmc_rank: number;
      id: number;
      last_updated: string;
      max_supply: number;
      total_supply: number;
      name: string;
      num_market_pairs: number;
      slug: string;
      symbol: string;
      quote: {
        fully_diluted_market_cap: number;
        last_updated: string;
        market_cap: number;
        market_cap_dominance: number;
        percent_change_1h: number;
        percent_change_7d: number;
        percent_change_24h: number;
        percent_change_30d: number;
        percent_change_60d: number;
        percent_change_90d: number;
        price: number;
        volume_24h: number;
        volume_change_24h: number;
      }[];
    }[];
  };
  currencyDataVi: {
    status: {
      timestamp: string;
      total_count: number;
    };
    data: {
      circulating_supply: number;
      cmc_rank: number;
      id: number;
      last_updated: string;
      max_supply: number;
      total_supply: number;
      name: string;
      num_market_pairs: number;
      slug: string;
      symbol: string;
      quote: {
        fully_diluted_market_cap: number;
        last_updated: string;
        market_cap: number;
        market_cap_dominance: number;
        percent_change_1h: number;
        percent_change_7d: number;
        percent_change_24h: number;
        percent_change_30d: number;
        percent_change_60d: number;
        percent_change_90d: number;
        price: number;
        volume_24h: number;
        volume_change_24h: number;
      }[];
    }[];
  };
  dataCrypto: {
    status: {
        timestamp: string;
        total_count: number;
    };
    data: {
      circulating_supply: number;
      cmc_rank: number;
      id: number;
      last_updated: string;
      max_supply: number;
      total_supply: number;
      name: string;
      num_market_pairs: number;
      slug: string;
      symbol: string;
      quote: quoteObject[];
    }[];
  };
  dataMakerCap: {
    btc_dominance: number;
    eth_dominance: number;
    quote: {
      total_market_cap: number;
      total_volume_24h: number;
    }[];
  }
}

export default function CryptoCurrencyPage({ 
  dataInterest,
  tableVietlot,
  dataBorrow,
  dataExchange,
  currentPage,
  currencyDataEng,
  currencyDataVi,
  dataCrypto,
  dataMakerCap
}: CryptoCurrencyPageProps) {

  return (
    <>
      <div>
        <Head>
          <title>{`Giá tiền ảo hôm nay, Cập nhật nhanh giá, vốn hóa thị trường tiền số mới nhất | Money24h`}</title>
          <meta name="description" content={`Cập nhật liên tục giá bitcoin, dogecoin, những đồng tiền mã hóa vốn hóa lớn liên tục. Xem biểu đồ, vốn hóa thị trường tiền số mới nhất | Money24h`}></meta>
          <link rel="icon" href="/logos/favicon.ico"></link>
          <link rel="canonical" href={`https://money24h.vn/tien-ao`}></link>
          <meta property="og:site_name" content="Money24H"></meta>
          <meta property="og:type" content="website" />
          <meta property="og:title" content={`Giá tiền ảo hôm nay, Cập nhật nhanh giá, vốn hóa thị trường tiền số mới nhất | Money24h`} />
          <meta property="og:description" content={`Cập nhật liên tục giá Bitcoin, Dogecoin, những đồng tiền mã hóa vốn hóa lớn liên tục. Xem biểu đồ, vốn hóa thị trường tiền số mới nhất | Money24h`} />
          <meta property="og:url" content={`https://money24h.vn/tien-ao`} />
          <meta property="og:image" content={"/images/image-gold-page.jpg"} />
        </Head>
        <CryptoCurrencyPageComponent 
          dataInterest={dataInterest}
          tableVietlot={tableVietlot}
          dataExchange={dataExchange}
          dataBorrow={dataBorrow}
          currentPage={currentPage}
          currencyDataEng={currencyDataEng}
          currencyDataVi={currencyDataVi}
          dataCrypto={dataCrypto}
          dataMakerCap={dataMakerCap}
        />
      </div>
    </>
  )
}

export const getServerSideProps: GetServerSideProps = async ( context: GetServerSidePropsContext ) => {

  if(context.query && context.query.page) {
    console.log(context.query)
  }

  let currentPage = context.query && context.query.page ? context.query.page : 1

  const formatData = (currencyData) => {
    return {
      ...currencyData,
      data: currencyData.data.map((item) => {
        return {
          ...item,
          quote: Object.values(item.quote)
        }
      })
    }
  }

  const formatMakerCap = (dataMakerCap) => {
    return {
      ...dataMakerCap,
      quote: Object.values(dataMakerCap.quote)
    }
  }

  const [
    webSetting,
    resDataInterest,
    resTableVietlot,
    resDataExchange,
    resDataBorrow,
    resEng,
    resVi,
    listBank,
    listCurrency,
    resDataCrypto,
    resMakerCap
  ] = await Promise.all([
    fetchApiCms("setting/websetting", ""),
    fetchAPI('/money24h/interest-save-vnex'),
    fetchAPI('/money24h/get-all-reward'),
    fetchAPI('/money24h/exchange-rate-by-bankid?BankId=1'),
    fetchAPI('/money24h/borrow-rate'),
    fetchAPI(`/coincotroller/list-coin?Start=${((Number(currentPage) - 1) * 100) + Number(currentPage)}&Limit=100&Convert=usd`),
    fetchAPI(`/coincotroller/list-coin?Start=${((Number(currentPage) - 1) * 100) + Number(currentPage)}&Limit=100&Convert=vnd`),
    fetchAPI("/money24h/interest-save"),
    fetchAPI(`/coincotroller/list-coin?Start=1&Limit=50&Convert=usd`),
    fetchAPI(`/coincotroller/sort-coin?Limit=10&Sort_dir=desc&Convert=vnd`),
    fetchAPI(`/coincotroller/market-cap?Convert=usd`)
  ])

  const dataInterest = resDataInterest && resDataInterest.success ? resDataInterest.data : null;
  const tableVietlot = resTableVietlot && resTableVietlot.success ? resTableVietlot.data : null;
  const dataExchange = resDataExchange && resDataExchange.success ? resDataExchange.data : null;
  const dataBorrow = resDataBorrow && resDataBorrow.success ? resDataBorrow.data : null;
  const dataEng = resEng && resEng.success ?  resEng.data : null;
  const dataVi = resVi && resVi.success ? resVi.data : null;
  const dataCrypto = resDataCrypto && resDataCrypto.success ? resDataCrypto.data : null;
  const dataMakerCap = resMakerCap && resMakerCap.success ? resMakerCap.data : null

  return { 
    props: { 
      webSetting,
      dataInterest,
      tableVietlot,
      dataExchange,
      dataBorrow,
      currentPage,
      currencyDataEng: formatData(dataEng),
      currencyDataVi: formatData(dataVi),
      listBank,
      listCurrency,
      dataCrypto,
      dataMakerCap: formatMakerCap(dataMakerCap.data)
    }
  }
}
