import Head from 'next/head';
import { GetServerSideProps } from 'next';
import { fetchAPI, fetchApiCms } from './api';
import { CheckInterestBankPage } from '../components/CheckInterestRateBank';

export default function CheckSavingRateBank({ 
  lastestPost, 
  listBankOps, 
  savingRateAllBank, 
  savingRateByBank, 
  dataTableBank, 
  postOfPage, 
  dataFooter, 
  detalNewsPost,
  stockCode ,
  dataCrypto
}) {
 
  const formatDateTime = () => {
    
    let date = new Date()
    let getMonth = date.getMonth() + 1
    let getFullyear = date.getFullYear()

    return `${getMonth}/${getFullyear}`
  }
  
  return (
    <>
      <Head>
        <title>{savingRateByBank.length > 0 ? `Lãi suất gửi tiết kiệm ${savingRateByBank[0].bankName} Tháng ${formatDateTime()} | Money24h` : `Lãi suất gửi tiết kiệm ngân hàng Tháng ${formatDateTime()} | Money24h`}</title>
        <meta name="description" content={savingRateByBank.length > 0 ? `Xem ngay Lãi suất gửi tiết kiệm ${savingRateByBank[0].bankName} Tháng ${formatDateTime()}. Tra cứu lãi suất tiền gửi không kỳ hạn, có kỳ hạn, theo số tháng` : `Xem ngay Lãi suất gửi tiết kiệm ngân hàng Cao Nhất Tháng ${formatDateTime()}. Tra cứu lãi suất tiền gửi không kỳ hạn, có kỳ hạn, theo số tháng`}></meta>
        <link rel="icon" href="/logos/favicon.ico"></link>
        <link rel="canonical" href={savingRateByBank.length > 0 ? `https://money24h.vn/lai-suat-gui-tiet-kiem-${savingRateByBank[0].slug}` : "https://money24h.vn/lai-suat-gui-tiet-kiem-ngan-hang"}></link>
        <meta property="og:site_name" content="Money24H"></meta>
        <meta property="og:type" content="website" />
        <meta property="og:title" content={savingRateByBank.length > 0 ? `Lãi suất gửi tiết kiệm ${savingRateByBank[0].bankName} Tháng ${formatDateTime()} | Money24h` : `Lãi suất gửi tiết kiệm ngân hàng Tháng ${formatDateTime()} | Money24h`} />
        <meta property="og:description" content={savingRateByBank.length > 0 ? `Xem ngay Lãi suất gửi tiết kiệm ${savingRateByBank[0].bankName} Tháng ${formatDateTime()}. Tra cứu lãi suất tiền gửi không kỳ hạn, có kỳ hạn, theo số tháng` : `Xem ngay Lãi suất gửi tiết kiệm ngân hàng Cao Nhất Tháng ${formatDateTime()}. Tra cứu lãi suất tiền gửi không kỳ hạn, có kỳ hạn, theo số tháng`} />
        <meta property="og:url" content={savingRateByBank.length > 0 ? `https://money24h.vn/lai-suat-gui-tiet-kiem-${savingRateByBank[0].slug}` : "https://money24h.vn/lai-suat-gui-tiet-kiem-ngan-hang"} />
        <meta property="og:image" content={"/images/finance-banner.png"} />
      </Head>
      <CheckInterestBankPage lastestPost={lastestPost} listBankOps={listBankOps} savingRateAllBank={savingRateAllBank} savingRateByBank={savingRateByBank} dataTableBank={dataTableBank} detalNewsPost={detalNewsPost} stockCode={stockCode} dataCrypto={dataCrypto}/>
    </>
  )
}

export const getServerSideProps: GetServerSideProps = async (context: {req}) => {
  const [
    lastestPost,
    dataListBank,
    savingRatelBank,
    // postOfPage,
    webSetting,
    listBank,
    listCurrency,
    resDataBorrow,
    resDataCrypto
  ] = await Promise.all([
    fetchApiCms("posts/get_newest_post", `&posts_per_page=5&page=1&offset=0`),
    fetchAPI("/money24h/get-list-bank?Type=interest"),
    fetchAPI("/money24h/interest-save"),
    // fetchApiCms("posts/get_post_detail", "&slug=cach-tinh-lai-suat-tiet-kiem-don-gian-khi-mo-so-tiet-kiem"),
    fetchApiCms("setting/websetting", ""),
    fetchAPI("/money24h/interest-save"),
    fetchAPI(`/coincotroller/list-coin?Start=1&Limit=50&Convert=usd`),
    fetchAPI("/money24h/borrow-rate"),
    fetchAPI(`/coincotroller/sort-coin?Limit=10&Sort_dir=desc&Convert=vnd`)
  ])

  let originalUrl = context.req.originalUrl

  let getParam = originalUrl.replace("/lai-suat-gui-tiet-kiem-", "")

  let savingRateByBank = null

  let listBankOps = null
  if (dataListBank.success) {
    listBankOps = dataListBank.data
  }

  let savingRateAllBank = null
  if (savingRatelBank.success) {
    savingRateAllBank = savingRatelBank.data
    savingRateByBank = savingRateAllBank.filter((item) => item.slug == getParam)
  }
  
  let postOfPage = null
  let detalNewsPost = null

  if(savingRateByBank.length > 0) {
    let idPost = savingRateByBank[0].idPost
    if(idPost) {
      let dataDetailPost = await fetchApiCms("posts/get_post_detail", `&slug=${idPost}`)
      if(dataDetailPost){
        detalNewsPost = dataDetailPost
      }
    }
  } else {
    let dataDetailPost = await fetchApiCms("posts/get_post_detail", `&slug=${"cach-tinh-lai-suat-tiet-kiem-don-gian-khi-mo-so-tiet-kiem"}`)
    if(dataDetailPost){
      detalNewsPost = dataDetailPost
    }
  }

  let dataTableBank = null
  let stockCode = null
  
  if (savingRateByBank && savingRateByBank.length > 0) {
    dataTableBank = savingRateByBank
    stockCode = savingRateByBank[0].share
  } else {
    dataTableBank = savingRateAllBank
  }

  const dataBorrow = resDataBorrow && resDataBorrow.success ? resDataBorrow.data : null;
  const dataCrypto = resDataCrypto && resDataCrypto.success ? resDataCrypto.data : null;
  return {
    props: { 
      lastestPost: lastestPost.posts, 
      listBankOps,  
      savingRateAllBank, 
      savingRateByBank, 
      dataTableBank, 
      postOfPage, 
      detalNewsPost,
      stockCode,
      webSetting,
      listBank,
      listCurrency,
      dataBorrow,
      dataCrypto
    },
  }
}