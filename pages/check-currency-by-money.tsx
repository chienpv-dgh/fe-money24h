import Head from 'next/head';
import { GetServerSideProps } from 'next';
import { fetchAPI, fetchApiCms } from './api';
import CheckCurrencyByMoney from '../components/CheckCurrency/check-currency-by-money';

export default function CheckCurrencyByMoneyPage ({ 
  lastestPost, 
  listBankOps, 
  listMoneyOps, 
  postOfPage, 
  dataTableCurrencyByMoney, 
  currentCurrency,
  detalNewsPost,
  dataCrypto
  
}) {

  return (
    <>
      <Head>
        <title>{`Tỷ Giá ${currentCurrency[0].moneyCode} - ${currentCurrency[0].moneyName} Hôm Nay | Money24h`}</title>
        <meta name="description" content={`Tỷ giá ${currentCurrency[0].moneyCode} - ${currentCurrency[0].moneyName} mới nhất hôm nay của Vietcombank, Agribank, BIDV, Sacombank, VPBank, TPBank, ACB, VIB, Dông Á Bank,...`} />
        <link rel="icon" href="/logos/favicon.ico"></link>
        <link rel="canonical" href={`https://money24h.vn/ty-gia-${currentCurrency[0].fullSlug}`}></link>
        <meta property="og:site_name" content="Money24H"></meta>
        <meta property="og:type" content="website" />
        <meta property="og:title" content={`Tỷ Giá ${currentCurrency[0].moneyCode} - ${currentCurrency[0].moneyName} Hôm Nay | Money24h`}></meta>
        <meta property="og:description" content={`Tỷ giá ${currentCurrency[0].moneyCode} - ${currentCurrency[0].moneyName} mới nhất hôm nay của Vietcombank, Agribank, BIDV, Sacombank, VPBank, TPBank, ACB, VIB, Dông Á Bank,...`}></meta>
        <meta property="og:url" content={`https://money24h.vn/ty-gia-${currentCurrency[0].fullSlug}`}></meta>
        <meta property="og:image" content={"/images/finance-banner.png"} />
      </Head>
      <CheckCurrencyByMoney lastestPost={lastestPost} listBankOps={listBankOps} listMoneyOps={listMoneyOps} postOfPage={postOfPage} dataTableCurrencyByMoney={dataTableCurrencyByMoney} currentCurrency={currentCurrency} detalNewsPost={detalNewsPost} dataCrypto={dataCrypto}/>
    </>
  )
}

export const getServerSideProps: GetServerSideProps = async (context: {req}) => {

  const [
    lastestPost,
    dataListBank,
    dataListMoney,
    dataPostPage,
    webSetting,
    resDataBorrow,
    listBank,
    listCurrency,
    resDataCrypto
  ] = await Promise.all([
    fetchApiCms("posts/get_newest_post", `&posts_per_page=5&page=1&offset=0`),
    fetchAPI("/money24h/get-list-bank?Type=exchange"),
    fetchAPI("/money24h/get-list-money"),
    fetchApiCms("posts/get_post_by_cat", `&category_slug=ngan-hang&posts_per_page=1&page=1&offset=0`),
    fetchApiCms("setting/websetting", ""),
    fetchAPI("/money24h/borrow-rate"),
    fetchAPI("/money24h/interest-save"),
    fetchAPI(`/coincotroller/list-coin?Start=1&Limit=50&Convert=usd`),
    fetchAPI(`/coincotroller/sort-coin?Limit=10&Sort_dir=desc&Convert=vnd`)
  ])

  let originalUrl = context.req.originalUrl

  let getParam = originalUrl.replace("/ty-gia-", "")

  let listBankOps = null
  if (dataListBank.success) {
    listBankOps = dataListBank.data
  }

  let listMoneyOps = null
  if (dataListMoney.success) {
    listMoneyOps = dataListMoney.data
  }

  let postOfPage = dataPostPage.posts && dataPostPage.posts.length > 0 ? dataPostPage.posts[0] : null

  let detalNewsPost = null

  if(postOfPage) {
    let dataDetailPost = await fetchApiCms("posts/get_post_detail", `&slug=${postOfPage.slug}`)
    if(dataDetailPost){
      detalNewsPost = dataDetailPost
    }
  }

  let currentCurrency = null
  if (dataListMoney.success) {
    currentCurrency = [...dataListMoney.data].filter((item) => item.fullSlug == getParam)
  }

  let dataTableCurrencyByMoney = null
  if(currentCurrency.length > 0) {
    let resultCurrencyByMoney = await fetchAPI(`/money24h/exchange-rate-by-money?MoneyId=${currentCurrency[0].id}`)
    if(resultCurrencyByMoney.success) {
      dataTableCurrencyByMoney = resultCurrencyByMoney.data
    }
  }

  const dataBorrow = resDataBorrow && resDataBorrow.success ? resDataBorrow.data : null;
  const dataCrypto = resDataCrypto && resDataCrypto.success ? resDataCrypto.data : null;
  
  return {
    props: { 
      lastestPost: lastestPost.posts, 
      listBankOps, 
      listMoneyOps, 
      postOfPage, 
      dataTableCurrencyByMoney, 
      currentCurrency,
      detalNewsPost,
      webSetting,
      listBank,
      listCurrency,
      dataBorrow,
      dataCrypto
    }
  }
}