import { GetServerSideProps } from 'next';
import Head from 'next/head';
import { fetchAPI, fetchApiCms } from './api';
import InfoFinanceComponent from '../components/InfoFinance';

export default function PostPage({ 
  dataInterest, 
  tableVietlot,
  newestPost,
}) {

  return (
    <>
      <div>
        <Head>
          <title>{`Thông tin tài chính mới nhất | Money24h`}</title>
          <meta name="description" content={"Tin HOT nhất 24h qua! Cập nhật thông tin tài chính, ngân hàng, chứng khoán, tiền điện tử hôm nay cùng Money24h."}></meta>
          <link rel="icon" href="/logos/favicon.ico"></link>
          <link rel="canonical" href={`https://money24h.vn/thong-tin-tai-chinh-moi-nhat`}></link>
          <meta property="og:site_name" content="Money24H"></meta>
          <meta property="og:type" content="website" />
          <meta property="og:title" content={`Thông tin tài chính mới nhất | Money24h`} />
          <meta property="og:description" content={"Tin HOT nhất 24h qua! Cập nhật thông tin tài chính, ngân hàng, chứng khoán, tiền điện tử hôm nay cùng Money24h."} />
          <meta property="og:url" content={`https://money24h.vn/thong-tin-tai-chinh-moi-nhat`} />
          <meta property="og:image" content={"/images/finance-banner.png"} />
        </Head>
        <InfoFinanceComponent dataInterest={dataInterest} tableVietlot={tableVietlot} dataPost={newestPost && newestPost.posts && newestPost.posts.length > 0 ? newestPost.posts : null} />
      </div>
    </>
  )
}

export const getServerSideProps: GetServerSideProps = async () => {

  const [
    newestPost,
    resDataInterest,
    resTableVietlot,
    webSetting,
    resDataBorrow,
    listBank,
    listCurrency
  ] = await Promise.all([
    fetchApiCms("posts/get_newest_post", `&posts_per_page=20&page=1&offset=0`),
    fetchAPI("/money24h/interest-save-vnex"),
    fetchAPI("/money24h/get-all-reward"),
    fetchApiCms("setting/websetting", ""),
    fetchAPI("/money24h/borrow-rate"),
    fetchAPI("/money24h/interest-save"),
    fetchAPI(`/coincotroller/list-coin?Start=1&Limit=50&Convert=usd`),
  ])

  const dataInterest = resDataInterest && resDataInterest.success ? resDataInterest.data : null;
  const tableVietlot = resTableVietlot && resTableVietlot.success ? resTableVietlot.data : null;
  const dataBorrow = resDataBorrow && resDataBorrow.success ? resDataBorrow.data : null;

  return { 
    props: { 
      dataInterest, 
      tableVietlot,
      webSetting,
      newestPost,
      listBank,
      listCurrency,
      dataBorrow
    }
  }
}