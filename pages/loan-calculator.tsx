import Head from 'next/head';
import { fetchAPI, fetchApiCms } from './api';
import { LoanCalculatorPage } from '../components/LoanCalculator';
import { GetServerSideProps } from 'next';

export default function LoanCalculator({ 
  AllPosts, 
  postOfPage,
  dataCrypto
}) {

  return (
    <>
      <Head>
        <title>Công cụ tính lãi suất vay tiền ngân hàng</title>
        <meta name="description" content="Cách tính lãi suất vay tiền ngân hàng: vay thế chấp, vay tín chấp, vay trả góp, vay mua nhà, vay vốn kinh doanh,..."></meta>
        <link rel="icon" href="/logos/favicon.ico"></link>
        <link rel="canonical" href="https://money24h.vn/cong-cu-tinh-toan-khoan-vay-ngan-hang" ></link>
        <meta property="og:site_name" content="Money24H"></meta>
        <meta property="og:type" content="website" />
        <meta property="og:title" content="Công cụ tính lãi suất vay tiền ngân hàng" />
        <meta property="og:description" content="Cách tính lãi suất vay tiền ngân hàng: vay thế chấp, vay tín chấp, vay trả góp, vay mua nhà, vay vốn kinh doanh,..." />
        <meta property="og:url" content="https://money24h.vn/cong-cu-tinh-toan-khoan-vay-ngan-hang" />
        <meta property="og:image" content="/images/finance-banner.png" />
        <script type="application/ld+json" dangerouslySetInnerHTML={{
            __html:`{
              "@context": "https://schema.org",
              "@type": "FAQPage",
              "mainEntity": [
                {
                  "@type": "Question",
                  "name": "Cách tính lãi suất vay ngân hàng theo dư nợ gốc như thế nào?",
                  "acceptedAnswer": {
                    "@type": "Answer",
                    "text": "<p>Chào bạn, cách tính lãi suất vay ngân hàng theo <strong>dư nợ gốc</strong> là: Số tiền bạn phải trả hàng tháng = <strong>Dư nợ gốc* lãi suất tháng</strong>. Bạn có thể sử dụng công cụ tính lãi suất vay của Money24h để tính toán khoản vay theo dư nợ gốc.</p>"
                  }
                },
                {
                  "@type": "Question",
                  "name": " Cách tính lãi suất vay ngân hàng theo dư nợ giảm dần như thế nào?",
                  "acceptedAnswer": {
                    "@type": "Answer",
                    "text": "<p>Chào bạn, cách tính lãi suất vay ngân hàng theo <strong>dư nợ giảm dần</strong> sẽ là:</p><ul><li>Tiền gốc hàng tháng = Số tiền vay/Số tháng vay</li><li>Tiền lãi tháng đầu = Số tiền vay * Lãi suất vay theo tháng</li><li>Tiền lãi các tháng tiếp theo = Số tiền gốc còn lại * Lãi suất vay</li></ul><p>Bạn có thể sử dụng công cụ tính lãi suất vay của Money24h để tính toán khoản vay theo dư nợ gốc.</p>"
                  }
                },
                {
                  "@type": "Question",
                  "name": "Lãi suất vay ngân hàng nào thấp nhất hiện nay?",
                  "acceptedAnswer": {
                    "@type": "Answer",
                    "text": "<p>Tùy theo từng thời điểm mà các ngân hàng sẽ điều chỉnh mức lãi suất cho vay. Ngoài lãi suất vay, bạn cũng cần tham khảo thêm ngân hàng đó có ưu đãi gì, thời hạn cho vay bao lâu, được vay dựa trên bao nhiêu % tài sản thế chấp. Để biết mức lãi suất vay ngân hàng, bạn hãy tham khảo <a href=https://money24h.vn/lai-suat-vay-ngan-hang>công cụ tra cứu lãi suất vay ngân hàng</a> của Money24h để biết thêm chi tiết.</p>"
                  }
                }
              ]
            }`
          }}></script>
      </Head>
      <LoanCalculatorPage AllPosts={AllPosts} postOfPage={postOfPage.detail_post ? postOfPage.detail_post : null} dataCrypto={dataCrypto}/>
    </>
  )
}

export const getServerSideProps: GetServerSideProps = async () => {

  const [
    AllPosts,  
    postOfPage,
    webSetting,
    resDataBorrow,
    listBank,
    listCurrency,
    resDataCrypto
  ] = await Promise.all([
    fetchApiCms("posts/get_newest_post", `&posts_per_page=5&page=1&offset=0`),
    fetchApiCms("posts/get_post_detail", "&slug=cach-tinh-lai-suat-vay-ngan-hang-nhanh-chong-don-gian-2"),
    fetchApiCms("setting/websetting", ""),
    fetchAPI("/money24h/borrow-rate"),
    fetchAPI("/money24h/interest-save"),
    fetchAPI(`/coincotroller/list-coin?Start=1&Limit=50&Convert=usd`),
    fetchAPI(`/coincotroller/sort-coin?Limit=10&Sort_dir=desc&Convert=vnd`)
  ])
  
  const dataBorrow = resDataBorrow && resDataBorrow.success ? resDataBorrow.data : null;
  const dataCrypto = resDataCrypto && resDataCrypto.success ? resDataCrypto.data : null;
  return {
    props: { 
      AllPosts: AllPosts.posts,  
      postOfPage,
      webSetting,
      listBank,
      listCurrency,
      dataBorrow,
      dataCrypto
    },
  }
}