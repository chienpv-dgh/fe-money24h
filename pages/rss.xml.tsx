import { GetServerSideProps, GetServerSidePropsContext } from 'next';
import { SITE_DESCIPTION, SITE_TITLE, SITE_URL } from '../lib/constants';
import { fetchApiCms } from './api';

const CustomRss = () => {};

export const getServerSideProps: GetServerSideProps = async (context: GetServerSidePropsContext) => {

  const [
    dataNewestPost,
  ] = await Promise.all([
    fetchApiCms("posts/get_basic_lastest_posts", `&total_post=200`),
  ])
  
  let newestPost = null

  if(dataNewestPost && dataNewestPost.posts && dataNewestPost.posts.length > 0) {
    newestPost = dataNewestPost.posts
  }

  const genrateNewestPost = (newestPost) => {

    let temp = ``
    if(newestPost) {
      newestPost.map((newestPostItem) => {
        temp += `
          <item>
            <title>${newestPostItem.title}</title>
            <description>${newestPostItem.title}</description>
            <pubDate>${newestPostItem.publish_date}</pubDate>
            <link>${SITE_URL}/blog/${newestPostItem.slug}</link>
            <guid>${SITE_URL}/blog/${newestPostItem.slug}</guid>
          </item>
        `
      })
    }
    return temp
  }

  const genrateSitemap = async () => {

    const [
      newestPostXML,
    ] = await Promise.all([
      genrateNewestPost(newestPost),
    ])

    return `
      <rss version="2.0" xmlns:slash="http://purl.org/rss/1.0/modules/slash/">
        <channel>
          <title>${SITE_TITLE}</title>
          <description>${SITE_DESCIPTION}</description>
          <pubDate>${new Date().toUTCString()}</pubDate>
          <generator>Money24h</generator>
          <link>${SITE_URL}/rss</link>
          ${newestPostXML}
        </channel>
      </rss>
    `;
  }

  let test = await genrateSitemap()

  if(test) {
    context.res.setHeader("Content-Type", "text/xml");
    context.res.write(test);
    context.res.end();
  }

  return {
    props: {
      
    },
  };
};

export default CustomRss;