import { fetchAPI, fetchApiCms } from "./api"
import Head from 'next/head'
import dynamic from "next/dynamic";
// import { generateRss } from "../lib/rss";
// import * as fs from 'fs';

const HomePageModule = dynamic(() => import("../components/modules/HomePage"));

export default function HomePage({ 
  dataInterest, 
  tableVietlot,  
  dataBorrow, 
  dataExchange, 
  dataHomepage,
  dataCategory,
  dataCrypto,
  currencyDataEng,
  currencyDataVi
 }) {

  return (
    <>
      <div className="homepage-container">
        <Head>
          <title>{dataHomepage && dataHomepage.seo_title ? dataHomepage.seo_title : "Money24H - Tin Thị Trường Tài Chính Ngân Hàng Mới Nhất 24H Qua"}</title>
          <meta name="description" content={dataHomepage && dataHomepage.seo_desc ? dataHomepage.seo_desc : "Tin HOT nhất 24h qua! Cập nhật thông tin tài chính, ngân hàng, chứng khoán, tiền điện tử hôm nay cùng Money24h."}></meta>
          <link rel="icon" href="/logos/favicon.ico"></link>
          <link rel="canonical" href="https://money24h.vn/"></link>
          <meta property="og:site_name" content="Money24H"></meta>
          <meta property="og:type" content="website" />
          <meta property="og:title" content={dataHomepage && dataHomepage.seo_title ? dataHomepage.seo_title : "Money24H - Tin Thị Trường Tài Chính Ngân Hàng Mới Nhất 24H Qua"} />
          <meta property="og:description" content={dataHomepage && dataHomepage.seo_desc ? dataHomepage.seo_desc : "Tin HOT nhất 24h qua! Cập nhật thông tin tài chính, ngân hàng, chứng khoán, tiền điện tử hôm nay cùng Money24h."} />
          <meta property="og:url" content="https://money24h.vn/" />
          <meta property="og:image" content={dataHomepage && dataHomepage.feature_image ? dataHomepage.feature_image : "/images/finance-banner.png"} />

        </Head>
        <HomePageModule 
          dataInterest={dataInterest} 
          tableVietlot={tableVietlot}
          dataExchange={dataExchange} 
          dataBorrow={dataBorrow} 
          dataNewest={dataHomepage.recent_posts && dataHomepage.recent_posts.length > 0 ? dataHomepage.recent_posts : null}
          dataCategory={dataCategory}
          dataCrypto={dataCrypto}
          currencyDataEng={currencyDataEng}
          currencyDataVi={currencyDataVi}
        />
      </div>
    </>
  )
}

export async function getStaticProps() {

  const formatData = (currencyData) => {
    if (currencyData?.data?.length > 0) {
      return {
        ...currencyData,
        data: currencyData.data.map((item) => {
          return {
            ...item,
            quote: item.quote ? Object.values(item.quote) : null,
          }
        })
      }
    } else {
      return {
        ...currencyData,
        data: []
      }
    }
  }
  
  const [
    resDataInterest,
    resTableVietlot,
    resDataExchange,
    resDataBorrow,
    dataHomepage,
    webSetting,
    listBank,
    listCurrency,
    resDataCrypto,
    resEng,
    resVi,
  ] = await Promise.all([
    fetchAPI("/money24h/interest-save-vnex"),
    fetchAPI("/money24h/get-all-reward"),
    fetchAPI('/money24h/exchange-rate-by-bankid?BankId=1'),
    fetchAPI("/money24h/borrow-rate"),
    fetchApiCms("page/homepage", ""),
    fetchApiCms("setting/websetting", ""),
    fetchAPI("/money24h/interest-save"),
    fetchAPI(`/coincotroller/list-coin?Start=1&Limit=50&Convert=usd`),
    fetchAPI(`/coincotroller/sort-coin?Limit=10&Sort_dir=desc&Convert=vnd`),
    fetchAPI(`/coincotroller/list-coin?Start=1&Limit=10&Convert=usd`),
    fetchAPI(`/coincotroller/list-coin?Start=1&Limit=10&Convert=vnd`),
  ])
  
  const dataInterest = resDataInterest && resDataInterest.success ? resDataInterest.data : null;
  const tableVietlot = resTableVietlot && resTableVietlot.success ? resTableVietlot.data : null;

  const dataExchange = resDataExchange && resDataExchange.success ? resDataExchange.data : null;
  const dataBorrow = resDataBorrow && resDataBorrow.success ? resDataBorrow.data : null;

  let dataCategory = null
  if (dataHomepage && dataHomepage.section_category) {
    let dataTemp = Object.keys(dataHomepage.section_category).map((item) => dataHomepage.section_category[item])
    dataCategory = dataTemp
  }

  const dataEng = resEng && resEng.success ?  resEng.data : null;
  const dataVi = resVi && resVi.success ? resVi.data : null;

  // const rss = await generateRss(dataRss.posts)
  // fs.writeFileSync('./public/rss.xml', rss)

  const dataCrypto = resDataCrypto && resDataCrypto.success ? resDataCrypto.data : null;

  return {
    props: {  
      dataInterest, 
      tableVietlot, 
      dataExchange, 
      dataBorrow,
      dataHomepage,
      dataCategory,
      webSetting,
      listBank,
      listCurrency,
      dataCrypto,
      currencyDataEng: formatData(dataEng),
      currencyDataVi: formatData(dataVi),
    },
    revalidate: 3600,
  }
}
