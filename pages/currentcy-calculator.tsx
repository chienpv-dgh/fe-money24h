import Head from 'next/head';
import { GetServerSideProps } from 'next';
import { fetchAPI, fetchApiCms } from './api';
import { CurrentcyCalculatorPage } from '../components/CurrentcyCalculator';

export default function CurrentcyCalculator({ 
  AllPosts, 
  listBankOps, 
  postOfPage,
  detalNewsPost,
  dataCrypto
}) {
  
  return (
    <>
      <Head>
        <title>Công cụ tính tỷ giá ngoại tệ ngân hàng</title>
        <meta name="description" content="Tỷ giá mua bán ngoại tệ ngân hàng hôm nay VND, Đô la Mỹ USD, EURO, Nhân dân tệ, Úc, Bảng Anh, Won Hàn Quốc, HongKong, Đồng Yên Nhật, Singapore, Thái Lan."></meta>
        <link rel="icon" href="/logos/favicon.ico"></link>
        <link rel="canonical" href="https://money24h.vn/cong-cu-tinh-ti-gia-ngoai-te"></link>
        <meta property="og:site_name" content="Money24H"></meta>
        <meta property="og:type" content="website" />
        <meta property="og:title" content="Công cụ tính tỷ giá ngoại tệ ngân hàng" />
        <meta property="og:description" content="Tỷ giá mua bán ngoại tệ ngân hàng hôm nay VND, Đô la Mỹ USD, EURO, Nhân dân tệ, Úc, Bảng Anh, Won Hàn Quốc, HongKong, Đồng Yên Nhật, Singapore, Thái Lan." />
        <meta property="og:url" content="https://money24h.vn/cong-cu-tinh-ti-gia-ngoai-te" />
        <meta property="og:image" content="/images/finance-banner.png" />
      </Head>
      <CurrentcyCalculatorPage AllPosts={AllPosts} listBankOps={listBankOps} postOfPage={postOfPage.posts} detalNewsPost={detalNewsPost} dataCrypto={dataCrypto}/>
    </>
  )
}

export const getServerSideProps: GetServerSideProps = async () => {
  
  const [
    AllPosts,
    dataListBank,
    postOfPage,
    webSetting,
    resDataBorrow,
    listBank,
    listCurrency,
    resDataCrypto
  ] = await Promise.all([
    fetchApiCms("posts/get_newest_post", `&posts_per_page=5&page=1&offset=0`),
    fetchAPI("/money24h/get-list-bank?Type=exchange"),
    fetchApiCms("posts/get_post_by_cat", "&category_slug=ngan-hang&posts_per_page=1&page=1&offset=0"),
    fetchApiCms("setting/websetting", ""),
    fetchAPI("/money24h/borrow-rate"),
    fetchAPI("/money24h/interest-save"),
    fetchAPI(`/coincotroller/list-coin?Start=1&Limit=50&Convert=usd`),
    fetchAPI(`/coincotroller/sort-coin?Limit=10&Sort_dir=desc&Convert=vnd`)
  ])
  
  let listBankOps = null
  if (dataListBank.success) {
    listBankOps = dataListBank.data
  }

  let detalNewsPost = null
  
  if(postOfPage && postOfPage.posts && postOfPage.posts.length > 0) {
    let dataDetailPost = await fetchApiCms("posts/get_post_detail", `&slug=${postOfPage.posts[0].slug}`)
    if(dataDetailPost){
      detalNewsPost = dataDetailPost
    }
  }

  const dataBorrow = resDataBorrow && resDataBorrow.success ? resDataBorrow.data : null;
  const dataCrypto = resDataCrypto && resDataCrypto.success ? resDataCrypto.data : null;
  return {
    props: { 
      AllPosts: AllPosts.posts, 
      listBankOps,  
      postOfPage,
      detalNewsPost,
      webSetting,
      listBank,
      listCurrency,
      dataBorrow,
      dataCrypto
    },
  }
}
