import Head from 'next/head';
import Script from 'next/script';

import 'swiper/swiper-bundle.min.css';
import 'bootstrap/dist/css/bootstrap.min.css';

//all css variable in global.css file
import '../styles/globals.scss'

import '../styles/desktop_header.scss'
import '../styles/mobile_header.scss'
import '../styles/footer.scss'
import '../styles/contact.scss'
import "../styles/error-page.scss"

import '../styles/CommonComponentCss/dropdown.css'
import '../styles/CommonComponentCss/facebook_comment_component.scss'
import "../styles/CommonComponentCss/custom-toc.scss"

import "../styles/Widgets/DataBank/data-bank.scss"
import "../styles/Widgets/DataVietlot/data-vietlot.scss"
import "../styles/Widgets/GlobalToolBox/global-tool-box.scss"
import "../styles/Widgets/PostDetailCurrency/post-detail-currency.scss"

import "../styles/PageComponentCss/HomePage/homepage.scss"
import "../styles/PageComponentCss/SavingInterestPage/saving-interest-page.scss"
import "../styles/PageComponentCss/NewsDetail/news-detail.scss"
import "../styles/PageComponentCss/LoanCalculatorPage/loan-calculator-page.scss"
import "../styles/PageComponentCss/CurrentcyCalculatorPage/currentcy-calculator-page.scss"
import "../styles/PageComponentCss/TagDetail/tag-detail-page.scss"
import "../styles/PageComponentCss/AuthorDetail/author-detail-page.scss"
import "../styles/PageComponentCss/InterestRateBank/check-interest-rate-bank.scss"
import "../styles/PageComponentCss/LoanRateBank/check-loan-rate-bank.scss"
import "../styles/PageComponentCss/CheckCurrency/check-currency-all.scss"
import "../styles/PageComponentCss/BorrowRate/borrow-rate.scss"
import "../styles/PageComponentCss/GoldToday/gold-today.scss"
import "../styles/PageComponentCss/CryptoCurrency/crypto-currency.scss"
import "../styles/PageComponentCss/CryptoCurrency/crypto-currency-detail.scss"
import '../styles/Widgets/DataCrypto/DataCrypto.scss'
import "../public/library/font-awesome/css/font-awesome.min.css"

import Layout from '../components/layout';

const MyApp = ({ Component, pageProps }) => {

  return (
    <>
      <Head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      </Head>
      <Script id="google-analytics" strategy="afterInteractive">
        {
          `(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
          new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
          j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
          'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
          })(window,document,'script','dataLayer','GTM-PN5NRWZ');`
        }
      </Script>
      <Script src="/library/jquery/jquery.min.js" strategy="beforeInteractive"></Script>
      <Layout {...pageProps}>
        <Component {...pageProps} />
      </Layout>
    </>
  )
}

export default MyApp