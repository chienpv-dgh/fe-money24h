import Document, { Html, Head, Main, NextScript } from 'next/document';

interface IRecipeProps {
  isLighthouse?: boolean;
}
class MyDocument extends Document<IRecipeProps> {

  static async getInitialProps(ctx) {

    const isServer = !!ctx.req
    const userAgent = isServer ? ctx.req.headers['user-agent'] : navigator.userAgent

    const isLighthouse = userAgent ? userAgent.toString().includes("Chrome-Lighthouse") : false

    const initialProps = await Document.getInitialProps(ctx);
    return { ...initialProps, isLighthouse };
  }

  render() {

    let dataHeadScript = this.props.__NEXT_DATA__.props.pageProps && this.props.__NEXT_DATA__.props.pageProps.webSetting ? this.props.__NEXT_DATA__.props.pageProps.webSetting.head_tag_code : null
    let dataBodyScript = this.props.__NEXT_DATA__.props.pageProps && this.props.__NEXT_DATA__.props.pageProps.webSetting ? this.props.__NEXT_DATA__.props.pageProps.webSetting.after_open_body_code : null;

    return (
      <Html lang="vi">
        <Head >
          <link rel="preconnect" href="https://fonts.gstatic.com" />
          <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-5581868270899806" crossOrigin="anonymous"></script>
          <script>
            (adsbygoogle = window.adsbygoogle || []).push({});
          </script>
          {/* <script dangerouslySetInnerHTML={{
            __html: `(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-PN5NRWZ');`
          }}></script> */}
          <script dangerouslySetInnerHTML={{ 
            __html: `!function(s,u,b,i,z){var o,t,r,y;s[i]||(s._sbzaccid=z,s[i]=function(){s[i].q.push(arguments)},s[i].q=[],s[i]("setAccount",z),r=["widget.subiz.net","storage.googleapis"+(t=".com"),"app.sbz.workers.dev",i+"a"+(o=function(k,t){var n=t<=6?5:o(k,t-1)+o(k,t-3);return k!==t?n:n.toString(32)})(20,20)+t,i+"b"+o(30,30)+t,i+"c"+o(40,40)+t],(y=function(k){var t,n;s._subiz_init_2094850928430||r[k]&&(t=u.createElement(b),n=u.getElementsByTagName(b)[0],t.async=1,t.src="https://"+r[k]+"/sbz/app.js?accid="+z,n.parentNode.insertBefore(t,n),setTimeout(y,2e3,k+1))})(0))}(window,document,"script","subiz", "acrbglqzghjwguvnwdvo")`
          }}></script>
          <script type="application/ld+json" dangerouslySetInnerHTML={{
            __html: `
            {
              "@context": "http://schema.org",
              "@type": "LocalBusiness",
              "image": "https://money24h.vn/logos/header-logo.png",
              "name": "Money24H",
              "url": "https://money24h.vn/",
              "telephone": "+84-938-678-642",
              "priceRange": "$$$",
              "address": {
                  "@type": "PostalAddress",
                  "streetAddress": "68 Hoàng Diệu, Phường 12, Quận 04",
                  "addressRegion": "HCM",
                  "addressLocality": "Hồ Chí Minh",
                  "postalCode": "700000",
                  "addressCountry": "VN"
              },
              "openingHoursSpecification": [{
                  "@type": "OpeningHoursSpecification",
                  "dayOfWeek": [
                      "Monday",
                      "Tuesday",
                      "Wednesday",
                      "Thursday",
                      "Friday"
                  ],
                  "opens": "08:00",
                  "closes": "20:30"
              },{
                  "@type": "OpeningHoursSpecification",
                  "dayOfWeek": "Saturday",
                  "opens": "08:00",
                  "closes": "12:00"
              }],
              "sameAs": [
              "https://www.youtube.com/channel/UCSpa8iwEcpH0LdR0LKZISXA",
              "https://twitter.com/Money24H1",
              "https://www.facebook.com/money24H.vn"
              ]
            }
          }`}}></script>
          <noscript dangerouslySetInnerHTML={{ 
            __html: `
              Develop by: TopOnSeek 
              Website: https://www.toponseek.com
          ` }} />
          <meta name="theme-color" content="#505050" />
        </Head>
        <body>
          <div id="fb-root"></div>
          <Main />
          {/* <script src="/library/jquery/jquery.min.js"></script >
          <script src="/library/toc/toc.js"></script > */}
          <NextScript />
          <style dangerouslySetInnerHTML={{
            __html: `
              @import "//fonts.googleapis.com/css2?family=Noto+Serif:wght@100;300;400;500;600;700&display=swap";
              @import "//fonts.googleapis.com/icon?family=Material+Icons&display=swap";
          `}}></style>
          <div
            dangerouslySetInnerHTML={{
              __html: `
              ${dataHeadScript && dataHeadScript}
            `,
            }}
          ></div>
          <div
            dangerouslySetInnerHTML={{
              __html: `
              ${dataBodyScript && dataBodyScript}
            `,
            }}
          ></div>
        </body>
      </Html>
    )
  }
}

export default MyDocument