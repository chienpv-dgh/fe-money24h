import { GetServerSideProps, GetServerSidePropsContext } from 'next';
import Head from 'next/head';
import CryptoCurrencyDetailPageComponent from '../../components/CryptoCurrencyDetail';
import { fetchAPI, fetchApiCms } from '../api';
import Script from 'next/script'


export interface quoteObject{
  fully_diluted_market_cap: number;
  last_updated: string;
  market_cap: number;
  market_cap_dominance: number;
  percent_change_1h: number;
  percent_change_7d: number;
  percent_change_24h: number;
  percent_change_30d: number;
  percent_change_60d: number;
  percent_change_90d: number;
  price: number;
  volume_24h: number;
  volume_change_24h: number;
}
export interface CryptoCurrencyDetailPageProps {
  dataInterest: {
    offline: {
      bankName: string;
      months1?: string;
      months3?: string;
      months6?: string;
      months9?: string;
      months12?: string;
      slug: string;
      type: number;
    }[];
    online: {
      bankName: string;
      months1?: string;
      months3?: string;
      months6?: string;
      months9?: string;
      months12?: string;
      slug: string;
      type: number;
    }[];
  };
  tableVietlot: {
    description: string;
    name: string;
    period: string;
    reward: number;
  }[];
  dataBorrow: {
    bankName: string;
    detail: string;
    idPost: string;
    months: string;
    results: {
      result?: string;
    };
    share?: string;
    slug: string;
  }[];
  dataExchange: {
    bankName: string;
    idPost: string;
    results: {
      cash: string;
      flagCash?: number;
      flagSell?: number;
      flagTrans?: number;
      fullSlug: string;
      moneyCode: string;
      moneyName: string;
      sellRate: string;
      share?: string;
      transfer: string;
    }[];
    share?: string;
    slug: string;
  };
  dataCurrencyEng: {
    id: number;
    name: string;
    symbol: string;
    slug: string;
    num_market_pairs: number;
    max_supply: number;
    circulating_supply: number;
    total_supply: number;
    cmc_rank: number;
    last_updated: string;
    quote: {
      fully_diluted_market_cap: number;
      last_updated: string;
      market_cap: number;
      market_cap_dominance: number;
      percent_change_1h: number;
      percent_change_7d: number;
      percent_change_24h: number;
      percent_change_30d: number;
      percent_change_60d: number;
      percent_change_90d: number;
      price: number;
      volume_24h: number;
      volume_change_24h: number;
    }[];
  };
  dataCurrencyVi: {
    id: number;
    name: string;
    symbol: string;
    slug: string;
    num_market_pairs: number;
    max_supply: number;
    circulating_supply: number;
    total_supply: number;
    cmc_rank: number;
    last_updated: string;
    quote: {
      fully_diluted_market_cap: number;
      last_updated: string;
      market_cap: number;
      market_cap_dominance: number;
      percent_change_1h: number;
      percent_change_7d: number;
      percent_change_24h: number;
      percent_change_30d: number;
      percent_change_60d: number;
      percent_change_90d: number;
      price: number;
      volume_24h: number;
      volume_change_24h: number;
    }[];
  };
  dataNav: {
    status: {
      timestamp: string;
      total_count: number;
    };
    data: {
      circulating_supply: number;
      cmc_rank: number;
      id: number;
      last_updated: string;
      max_supply: number;
      total_supply: number;
      name: string;
      num_market_pairs: number;
      slug: string;
      symbol: string;
      quote: {
        fully_diluted_market_cap: number;
        last_updated: string;
        market_cap: number;
        market_cap_dominance: number;
        percent_change_1h: number;
        percent_change_7d: number;
        percent_change_24h: number;
        percent_change_30d: number;
        percent_change_60d: number;
        percent_change_90d: number;
        price: number;
        volume_24h: number;
        volume_change_24h: number;
      }[];
    }[];
  };
  detailCurrencyPost: {
    detail_post: {
      content: string;
      excerpt: string;
      feature_image: string;
      slug: string;
      title: string;
      seo_desc: string;
      seo_title: string;
    };
    newest_post: {
      feature_image: string;
      publish_date: string;
      slug: string;
      title: string;
    }[];
  }
  lastestPost: any;
  dataCrypto: {
    status: {
      timestamp: string;
      total_count: number;
    };
    data: {
      circulating_supply: number;
      cmc_rank: number;
      id: number;
      last_updated: string;
      max_supply: number;
      total_supply: number;
      name: string;
      num_market_pairs: number;
      slug: string;
      symbol: string;
      quote: quoteObject[];
    }[];
  };
  resDetailCoin?: {
    data: {
      description: string;
      id: number;
      image: string;
      name: string;
      sourceCode: string;
      symbol: string;
      type: string;
      listCommunity: {
        linkCoins_Communities: string;
        nameCommunity: string;
      }[];
      listExplorer: {
        linkCoins_Explorers: string;
        nameExplorer: string;
      }[];
      listWebsite: {
        linkCoins_Websites: string;
        nameWebsite: string;
      }[];
    }[];
  };
}

export default function CryptoCurrencyDetailPage({ 
  dataInterest,
  tableVietlot,
  dataExchange,
  dataBorrow,
  dataCurrencyEng,
  dataCurrencyVi,
  dataNav,
  detailCurrencyPost,
  lastestPost,
  dataCrypto,
  resDetailCoin
}: CryptoCurrencyDetailPageProps) {
  
  const formatNumberWithComma = (x) => {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }

  return (
    <>
      <div>
        <Head>
          <script type="application/ld+json" dangerouslySetInnerHTML={{
            __html: `{
              "@context": "https://schema.org",
              "@type": "BreadcrumbList",
              "itemListElement": [{
                "@type": "ListItem",
                "position": 1,
                "name": "Trang chủ",
                "item": "https://money24h.vn"
              },{
                "@type": "ListItem",
                "position": 2,
                "name": "Tiền ảo",
                "item": "https://money24h.vn/tien-ao"
              },{
                "@type": "ListItem",
                "position": 3,
                "name": "${dataCurrencyEng && dataCurrencyEng.name ? dataCurrencyEng.name : ""}"
              }]
            }`
          }}></script>
          <title>{ detailCurrencyPost && detailCurrencyPost.detail_post && detailCurrencyPost.detail_post.seo_title ? detailCurrencyPost.detail_post.seo_title : `${dataCurrencyEng.name}: Giá ${dataCurrencyEng.symbol}, Chart thị trường, vốn hóa ${dataCurrencyEng.name} Hôm nay | Money24h`}</title>
          <meta name="description" content={ detailCurrencyPost && detailCurrencyPost.detail_post && detailCurrencyPost.detail_post.seo_desc ? detailCurrencyPost.detail_post.seo_desc : `Cập nhật thứ hạng Đồng tiền số ${dataCurrencyEng.name} - Xếp hạng: ${dataCurrencyEng.cmc_rank}, vốn hóa thị trường hôm nay là ${formatNumberWithComma(Math.round(dataCurrencyVi.quote[0].market_cap * 100) / 100)} VNĐ. Cập nhật bảng giá đồng coin ${dataCurrencyEng.name} và thông tin thị trường coin mới nhất từ Money24h`}></meta>
          <link rel="icon" href="/logos/favicon.ico"></link>
          <link rel="canonical" href={`https://money24h.vn/tien-ao/${dataCurrencyEng.symbol}`}></link>
          <meta property="og:site_name" content="Money24H"></meta>
          <meta property="og:type" content="website" />
          <meta property="og:title" content={ detailCurrencyPost && detailCurrencyPost.detail_post && detailCurrencyPost.detail_post.seo_title ? detailCurrencyPost.detail_post.seo_title : `${dataCurrencyEng.name}: Giá ${dataCurrencyEng.symbol}, Chart thị trường, vốn hóa ${dataCurrencyEng.name} Hôm nay | Money24h`} />
          <meta property="og:description" content={ detailCurrencyPost && detailCurrencyPost.detail_post && detailCurrencyPost.detail_post.seo_desc ? detailCurrencyPost.detail_post.seo_desc : `Cập nhật thứ hạng Đồng tiền số ${dataCurrencyEng.name} - Xếp hạng: ${dataCurrencyEng.cmc_rank}, vốn hóa thị trường hôm nay là ${formatNumberWithComma(Math.round(dataCurrencyVi.quote[0].market_cap * 100) / 100)} VNĐ. Cập nhật bảng giá đồng coin ${dataCurrencyEng.name} và thông tin thị trường coin mới nhất từ Money24h`} />
          <meta property="og:url" content={`https://money24h.vn/tien-ao/${dataCurrencyEng.symbol}`} />
          <meta property="og:image" content={"/images/image-gold-page.jpg"} />
          {/* <script type="text/javascript" src="https://d33t3vvu2t2yu5.cloudfront.net/tv.js"></script> */}
        </Head>
        <Script src="https://d33t3vvu2t2yu5.cloudfront.net/tv.js" strategy="lazyOnload" />
        <CryptoCurrencyDetailPageComponent
          dataInterest={dataInterest}
          tableVietlot={tableVietlot}
          dataExchange={dataExchange}
          dataBorrow={dataBorrow}
          dataCurrencyVi={dataCurrencyVi}
          dataCurrencyEng={dataCurrencyEng}
          dataNav={dataNav}
          detailCurrencyPost={detailCurrencyPost}
          lastestPost={lastestPost}
          dataCrypto={dataCrypto}
          resDetailCoin={resDetailCoin}
        />
      </div>
    </>
  )
}

export const getServerSideProps: GetServerSideProps = async (context: GetServerSidePropsContext) => {

  const formatData = (currencyData) => {
    return {
      ...currencyData,
      data: currencyData.data.map((item) => {
        return {
          ...item,
          quote: Object.values(item.quote)
        }
      })
    }
  }

  const formatDataDetail = (currencyData) => {
    return {
      ...currencyData,
      quote: Object.values(currencyData.quote)
    }
  }

  const [
    webSetting,
    resDataInterest,
    resTableVietlot,
    resDataExchange,
    resDataBorrow,
    resNav ,
    resEng,
    resVi,
    listBank,
    listCurrency,
    lastestPost,
    resDataCrypto,
    resDetailCoin
  ] = await Promise.all([
    fetchApiCms("setting/websetting", ""),
    fetchAPI('/money24h/interest-save-vnex'),
    fetchAPI('/money24h/get-all-reward'),
    fetchAPI('/money24h/exchange-rate-by-bankid?BankId=1'),
    fetchAPI('/money24h/borrow-rate'),
    fetchAPI('/coincotroller/list-coin?Start=1&Limit=20&Convert=usd'),
    fetchAPI(`/coincotroller/detail-coin?Symbol=${context.query.slug}&Convert=USD`),
    fetchAPI(`/coincotroller/detail-coin?Symbol=${context.query.slug}&Convert=VND`),
    fetchAPI("/money24h/interest-save"),
    fetchAPI(`/coincotroller/list-coin?Start=1&Limit=50&Convert=usd`),
    fetchApiCms("posts/get_newest_post", `&posts_per_page=5&page=1&offset=0`),
    fetchAPI(`/coincotroller/sort-coin?Limit=10&Sort_dir=desc&Convert=vnd`),
    fetchAPI(`/coincotroller/info-coin?Symbol=${context.query.slug}`)
  ])

  if(!resEng.success || !resVi.success || !resNav.success) {
    return {
      notFound: true
    }
  }

  const dataInterest = resDataInterest && resDataInterest.success ? resDataInterest.data : null;
  const tableVietlot = resTableVietlot && resTableVietlot.success ? resTableVietlot.data : null;
  const dataExchange = resDataExchange && resDataExchange.success ? resDataExchange.data : null;
  const dataBorrow = resDataBorrow && resDataBorrow.success ? resDataBorrow.data : null;
  const dataNav = resNav && resNav.success ? resNav.data : null;
  const dataGetCurrencyEng = resEng && resEng.success ? resEng.data : null;
  const dataGetCurrencyVi = resVi && resVi.success ? resVi.data : null;
  const dataCrypto = resDataCrypto && resDataCrypto.success ? resDataCrypto.data : null;

  let currencyDetailEng = []
  let detailCurrencyPost = null

  if(dataGetCurrencyEng && dataGetCurrencyEng.status && dataGetCurrencyEng.status.error_code) {
    currencyDetailEng = []
  } else {
    currencyDetailEng = Object.values(dataGetCurrencyEng.data)
    let resDetail = await fetchApiCms("posts/get_post_detail", `&slug=ma-${currencyDetailEng[0].slug}`)
    if(resDetail && resDetail.detail_post) {
      detailCurrencyPost = resDetail
    }
  }

  let currencyDetailVi = []

  if(dataGetCurrencyVi && dataGetCurrencyVi.status && dataGetCurrencyVi.status.error_code) {
    currencyDetailVi = []
  } else {
    currencyDetailVi = Object.values(dataGetCurrencyVi.data)
  }

  return { 
    props: { 
      webSetting,
      dataInterest,
      tableVietlot,
      dataExchange,
      dataBorrow,
      dataCurrencyEng: formatDataDetail(currencyDetailEng[0]),
      dataCurrencyVi: formatDataDetail(currencyDetailVi[0]),
      dataNav: formatData(dataNav),
      listBank,
      listCurrency,
      detailCurrencyPost,
      lastestPost,
      dataCrypto,
      resDetailCoin
    } 
  }
}