import Head from 'next/head';
import { GetServerSideProps } from 'next';
import { SavingInterestPage } from '../components/SavingInterest';
import { fetchAPI, fetchApiCms } from './api';

export default function SavingInterest({ 
  AllPosts, 
  postOfPage,
  dataCrypto
 }) {

  return (
    <>
      <Head>
        <title>Công cụ tính tiền lãi gửi ngân hàng</title>
        <meta name="description" content="Cách tính lãi suất gửi tiết kiệm ngân hàng và tra cứu lãi suất tiền gửi không kỳ hạn, có kỳ hạn, theo số tháng."></meta>
        <link rel="icon" href="/logos/favicon.ico"></link>
        <link rel="canonical" href="https://money24h.vn/cong-cu-tinh-tien-lai-gui-ngan-hang"></link>
        <meta property="og:site_name" content="Money24H"></meta>
        <meta property="og:type" content="website" />
        <meta property="og:title" content="Công cụ tính tiền lãi gửi ngân hàng" />
        <meta property="og:description" content="Cách tính lãi suất gửi tiết kiệm ngân hàng và tra cứu lãi suất tiền gửi không kỳ hạn, có kỳ hạn, theo số tháng." />
        <meta property="og:url" content="https://money24h.vn/cong-cu-tinh-tien-lai-gui-ngan-hang" />
        <meta property="og:image" content="/images/finance-banner.png" />
      </Head>
      <SavingInterestPage AllPosts={AllPosts} postOfPage={postOfPage.detail_post ? postOfPage.detail_post : null} dataCrypto={dataCrypto} />
    </>
  )
}

export const getServerSideProps: GetServerSideProps = async () => {
  
  const [
    AllPosts,
    postOfPage,
    webSetting,
    resDataBorrow,
    listBank,
    listCurrency,
    resDataCrypto
  ] = await Promise.all([
    fetchApiCms("posts/get_newest_post", `&posts_per_page=5&page=1&offset=0`),
    fetchApiCms("posts/get_post_detail", "&slug=cach-tinh-lai-suat-tiet-kiem-don-gian-khi-mo-so-tiet-kiem"),
    fetchApiCms("setting/websetting", ""),
    fetchAPI("/money24h/borrow-rate"),
    fetchAPI("/money24h/interest-save"),
    fetchAPI(`/coincotroller/list-coin?Start=1&Limit=50&Convert=usd`),
    fetchAPI(`/coincotroller/sort-coin?Limit=10&Sort_dir=desc&Convert=vnd`)
  ])

  const dataBorrow = resDataBorrow && resDataBorrow.success ? resDataBorrow.data : null;
  const dataCrypto = resDataCrypto && resDataCrypto.success ? resDataCrypto.data : null;
  return {
    props: { 
      AllPosts: AllPosts.posts,
      postOfPage,
      webSetting,
      listBank,
      listCurrency,
      dataBorrow,
      dataCrypto
    },
  }
}