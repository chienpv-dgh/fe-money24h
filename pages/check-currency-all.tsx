import Head from 'next/head';
import { GetServerSideProps } from 'next';
import { fetchAPI, fetchApiCms } from './api';
import { CheckCurrencyAll } from '../components/CheckCurrency';

export default function CheckCurrencyAllPage ({ 
  lastestPost,  
  postOfPage, 
  listBankOps, 
  listMoneyOps,
  detalNewsPost,
  dataCrypto
}) {

  return (
    <>
      <Head>
        <title>{`Tỷ Giá Ngoại Tệ/Hối Đoái Ngân Hàng Cao Nhất Hôm Nay | Money24h`}</title>
        <meta name="description" content={`Tỷ giá mua bán ngoại tệ hôm nay VND, Đô la Mỹ USD, EURO, Nhân dân tệ, Úc, Bảng Anh, Won Hàn Quốc, HongKong, Đồng Yên Nhật, Singapore, Thái Lan.`}></meta>
        <link rel="icon" href="/logos/favicon.ico"></link>
        <link rel="canonical" href={"https://money24h.vn/ty-gia-ngoai-te-hoi-doai-ngan-hang-hom-nay"}></link>
        <meta property="og:site_name" content="Money24H"></meta>
        <meta property="og:type" content="website" />
        <meta property="og:title" content={`Tỷ Giá Ngoại Tệ/Hối Đoái Ngân Hàng Cao Nhất Hôm Nay | Money24h`}></meta>
        <meta property="og:description" content={`Tỷ giá mua bán ngoại tệ hôm nay VND, Đô la Mỹ USD, EURO, Nhân dân tệ, Úc, Bảng Anh, Won Hàn Quốc, HongKong, Đồng Yên Nhật, Singapore, Thái Lan.`}></meta>
        <meta property="og:url" content={"https://money24h.vn/ty-gia-ngoai-te-hoi-doai-ngan-hang-hom-nay"}></meta>
        <meta property="og:image" content={"/images/finance-banner.png"} />
      </Head>
      <CheckCurrencyAll lastestPost={lastestPost} postOfPage={postOfPage} listBankOps={listBankOps} listMoneyOps={listMoneyOps} detalNewsPost={detalNewsPost} dataCrypto={dataCrypto}/>
    </>
  )
}

export const getServerSideProps: GetServerSideProps = async (context: {req}) => {
  
  const [
    lastestPost,
    dataListBank,
    dataListMoney,
    dataPostPage,
    webSetting,
    resDataBorrow,
    listBank,
    listCurrency,
    resDataCrypto
  ] = await Promise.all([
    fetchApiCms("posts/get_newest_post", `&posts_per_page=5&page=1&offset=0`),
    fetchAPI("/money24h/get-list-bank?Type=exchange"),
    fetchAPI("/money24h/get-list-money"),
    fetchApiCms("posts/get_post_by_cat", `&category_slug=ngan-hang&posts_per_page=1&page=1&offset=0`),
    fetchApiCms("setting/websetting", ""),
    fetchAPI("/money24h/borrow-rate"),
    fetchAPI("/money24h/interest-save"),
    fetchAPI(`/coincotroller/list-coin?Start=1&Limit=50&Convert=usd`),
    fetchAPI(`/coincotroller/sort-coin?Limit=10&Sort_dir=desc&Convert=vnd`)
  ])

  let postOfPage = dataPostPage.posts && dataPostPage.posts.length > 0 ? dataPostPage.posts[0] : null

  let detalNewsPost = null

  if(postOfPage) {
    let dataDetailPost = await fetchApiCms("posts/get_post_detail", `&slug=${postOfPage.slug}`)
    if(dataDetailPost){
      detalNewsPost = dataDetailPost
    }
  }

  let listBankOps = null
  if (dataListBank.success) {
    listBankOps = dataListBank.data
  }

  let listMoneyOps = null
  if (dataListMoney.success) {
    listMoneyOps = dataListMoney.data
  }

  const dataBorrow = resDataBorrow && resDataBorrow.success ? resDataBorrow.data : null;
  const dataCrypto = resDataCrypto && resDataCrypto.success ? resDataCrypto.data : null;
  return {
    props: { 
      lastestPost: lastestPost.posts,  
      postOfPage, 
      listBankOps, 
      listMoneyOps,
      detalNewsPost,
      webSetting,
      listBank,
      listCurrency,
      dataBorrow,
      dataCrypto
    }
  }
}