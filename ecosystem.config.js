module.exports = {
  apps: [
    {
      name: 'money24h',
      script: 'nohup',
      args: 'yarn start',
      env: {
        NODE_ENV: 'production',
      },
      exp_backoff_restart_delay: 100,
    },
  ],
};
