const express = require('express')
const useragent = require('express-useragent')
const next = require('next')
const path = require('path');
const axios = require('axios')
const port = parseInt(process.env.PORT, 10) || 2707
const dev = process.env.NODE_ENV !== 'production'
const app = next({ dev })
const handle = app.getRequestHandler()

const API_TOT = "https://apimoney24h.toponseek.com/money24h"
const API_CMS = "https://admin.money24h.vn/wp-json/totapi"
const KEY_CMS = "4746a0cc735cb3ea921a01ffb1e5c257"

const getSlugInterest = async () => {
  let result = await axios.get(`${API_TOT}/interest-save`, {
    headers: {
      'ApiKey': 'ajsfldkaj8483*$#&$*BBFHJDHJS'
    }
  })
  return result.data
}

const getSlugBorrow = async () => {
  let result = await axios.get(`${API_TOT}/borrow-rate`, {
    headers: {
      'ApiKey': 'ajsfldkaj8483*$#&$*BBFHJDHJS'
    }
  })
  return result.data
}

const getSlugExchange = async () => {
  let result = await axios.get(`${API_TOT}/get-list-bank?Type=exchange`, {
    headers: {
      'ApiKey': 'ajsfldkaj8483*$#&$*BBFHJDHJS'
    }
  })
  return result.data
}

const getRedirect = async () => {
  let result = await axios.get(`${API_CMS}/redirect/get_redirect_link?key=${KEY_CMS}`)
  return result.data
}

app.prepare().then(() => {
  const server = express()

  server.get('/robots.txt', (req, res) => {
    res.sendFile(`robots.txt`, {root: path.join(__dirname,"../") })
  })

  server.get('/sitemap_index.xml', (req, res) => {
    return app.render(req, res, '/sitemap_index.xml', req.query)
  })

  server.get('/ads.txt', (req, res) => {
    res.sendFile(`ads.txt`, {root: path.join(__dirname,"../") })
  })

  server.get('/favicon.ico', (req, res) => {
    res.sendFile(`favicon.ico`, {root: path.join(__dirname,"../public/logos/") })
  })

  server.get('/rss', (req, res) => {
    return app.render(req, res, '/rss.xml', req.query)
  })

  server.get('/', (req, res) => {
    return app.render(req, res, '/', req.query)
  })

  server.get('/cong-cu-tinh-tien-lai-gui-ngan-hang', (req, res) => {
    return app.render(req, res, '/saving-interest-rate', req.query)
  })

  server.get('/cong-cu-tinh-toan-khoan-vay-ngan-hang', (req, res) => {
    return app.render(req, res, '/loan-calculator', req.query)
  })

  server.get('/cong-cu-tinh-ty-gia-ngoai-te', (req, res) => {
    return app.render(req, res, '/currentcy-calculator', req.query)
  })

  server.get('/article-preview-beta', (req, res) => {
    return app.render(req, res, '/preview', req.query)
  })

  server.get('/lai-suat-gui-tiet-kiem-ngan-hang', (req, res) => {
    return app.render(req, res, '/check-interest-rate-bank', req.query)
  })

  server.get('/lai-suat-vay-ngan-hang', (req, res) => {
    return app.render(req, res, '/check-rate-loan-bank', req.query)
  })

  server.get('/thong-tin-tai-chinh-moi-nhat', (req, res) => {
    return app.render(req, res, '/info-finance', req.query)
  })

  server.get('/ty-gia-ngoai-te-hoi-doai-ngan-hang-hom-nay', (req, res) => {
    return app.render(req, res, '/check-currency-all', req.query)
  })

  server.get('/gia-vang-hom-nay', (req, res) => {
    return app.render(req, res, '/gold-today', req.query)
  })

  server.get('/tien-ao', (req, res) => {
    return app.render(req, res, '/crypto-currency', req.query)
  })

  server.get('/tien-ao/:slug', (req, res) => {
    return app.render(req, res, `/crypto-currency-detail/${req.params.slug}`, req.query)
  })

  server.get('/images/*', (req, res) => {
    return handle(req, res)
  })

  server.get('/logos/*', (req, res) => {
    return handle(req, res)
  })

  server.get("/*", async (req, res) => {
  // console.log("===>", res.end, res.statusCode)

    let dataRedirect = await getRedirect()
    let currentPath = req.path

    let tempCheck = dataRedirect.find((item) => item.old_url == currentPath)

    if(tempCheck) {
      return res.redirect(tempCheck.new_url);
    } else {
      if (req.path.includes("/lai-suat-vay")) {
 
        if (req.path == "/lai-suat-vay-ngan-hang") {
          return app.render(req, res, '/check-rate-loan-bank', req.query)
        } else {
          let result = await getSlugBorrow()
          let itemFound = result.data.find((item) => `/lai-suat-vay-${item.slug}` == req.path)
          
          if(itemFound){
  
            return app.render(req, res, '/check-rate-loan-bank', req.query)
          } else {
            return res.redirect("/lai-suat-vay-ngan-hang");
          }
        }
      } else if (req.path.includes("/lai-suat-gui-tiet-kiem")) {
        if (req.path == "/lai-suat-gui-tiet-kiem-ngan-hang") {
        
          return app.render(req, res, '/check-interest-rate-bank', req.query)
        } else {
          let result = await getSlugInterest()
          let itemFound = result.data.find((item) => `/lai-suat-gui-tiet-kiem-${item.slug}` == req.path)
          
          if(itemFound){
            return app.render(req, res, '/check-interest-rate-bank', req.query)
          } else {
            return res.redirect("/lai-suat-gui-tiet-kiem-ngan-hang");
          }
        }
      } else if (req.path.includes("/ty-gia-ngoai-te")) {
        let result = await getSlugExchange()
        let itemFound = result.data.find((item) => `/ty-gia-ngoai-te-${item.slug}` == req.path)
        if(itemFound){
          return app.render(req, res, '/check-currency-by-bank', req.query)
        } else {
          return res.redirect("/ty-gia-ngoai-te-hoi-doai-ngan-hang-hom-nay")
        }
      } else if (req.path.includes("/ty-gia")) {
        return app.render(req, res, '/check-currency-by-money', req.query)
      } else {
        return handle(req, res)
      }
    }
  })

  server.all('*', (req, res) => {
    return handle(req, res)
  })

  server.listen(port, (err) => {
    if (err) throw err
    console.log(`> Ready on http://localhost:${port}`)
  })
})
.catch((err) => {
  console.log('> App failed to start', err);
});