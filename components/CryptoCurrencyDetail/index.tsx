import React, { useEffect, useState } from 'react';
import CryptoCurrencyNavbar from '../CryptoCurrency/navbar-currency';
import WidgetBorrowData from '../widgets/DataBorrow';
import ExchangeRate from '../widgets/DataExchange';
import { ContentPageSaving } from '../SavingInterest';
import CurrencyDetailPost from '../widgets/PostDetailCurrency';
import CurrencyInfoDetail from './currency-info-detail';
import WidgetDataByBank from '../widgets/DataByBank';
import VietlotData from '../widgets/DataVietlot';
import WidgetsDataCrypto from '../widgets/DataCryptoCurrency';

export interface quoteObject{
  fully_diluted_market_cap: number;
  last_updated: string;
  market_cap: number;
  market_cap_dominance: number;
  percent_change_1h: number;
  percent_change_7d: number;
  percent_change_24h: number;
  percent_change_30d: number;
  percent_change_60d: number;
  percent_change_90d: number;
  price: number;
  volume_24h: number;
  volume_change_24h: number;
}
export interface CryptoCurrencyDetailProps {
  dataInterest: {
    offline: {
      bankName: string;
      months1?: string;
      months3?: string;
      months6?: string;
      months9?: string;
      months12?: string;
      slug: string;
      type: number;
    }[];
    online: {
      bankName: string;
      months1?: string;
      months3?: string;
      months6?: string;
      months9?: string;
      months12?: string;
      slug: string;
      type: number;
    }[];
  };
  tableVietlot: {
    description: string;
    name: string;
    period: string;
    reward: number;
  }[];
  dataBorrow: {
    bankName: string;
    detail: string;
    idPost: string;
    months: string;
    results: {
      result?: string;
    };
    share?: string;
    slug: string;
  }[];
  dataExchange: {
    bankName: string;
    idPost: string;
    results: {
      cash: string;
      flagCash?: number;
      flagSell?: number;
      flagTrans?: number;
      fullSlug: string;
      moneyCode: string;
      moneyName: string;
      sellRate: string;
      share?: string;
      transfer: string;
    }[];
    share?: string;
    slug: string;
  };
  dataCurrencyEng: {
    id: number;
    name: string;
    symbol: string;
    slug: string;
    num_market_pairs: number;
    max_supply: number;
    circulating_supply: number;
    total_supply: number;
    cmc_rank: number;
    last_updated: string;
    quote: {
      fully_diluted_market_cap: number;
      last_updated: string;
      market_cap: number;
      market_cap_dominance: number;
      percent_change_1h: number;
      percent_change_7d: number;
      percent_change_24h: number;
      percent_change_30d: number;
      percent_change_60d: number;
      percent_change_90d: number;
      price: number;
      volume_24h: number;
      volume_change_24h: number;
    }[];
  };
  dataCurrencyVi: {
    id: number;
    name: string;
    symbol: string;
    slug: string;
    num_market_pairs: number;
    max_supply: number;
    circulating_supply: number;
    total_supply: number;
    cmc_rank: number;
    last_updated: string;
    quote: {
      fully_diluted_market_cap: number;
      last_updated: string;
      market_cap: number;
      market_cap_dominance: number;
      percent_change_1h: number;
      percent_change_7d: number;
      percent_change_24h: number;
      percent_change_30d: number;
      percent_change_60d: number;
      percent_change_90d: number;
      price: number;
      volume_24h: number;
      volume_change_24h: number;
    }[];
  };
  dataNav: {
    status: {
      timestamp: string;
      total_count: number;
    };
    data: {
      circulating_supply: number;
      cmc_rank: number;
      id: number;
      last_updated: string;
      max_supply: number;
      total_supply: number;
      name: string;
      num_market_pairs: number;
      slug: string;
      symbol: string;
      quote: {
        fully_diluted_market_cap: number;
        last_updated: string;
        market_cap: number;
        market_cap_dominance: number;
        percent_change_1h: number;
        percent_change_7d: number;
        percent_change_24h: number;
        percent_change_30d: number;
        percent_change_60d: number;
        percent_change_90d: number;
        price: number;
        volume_24h: number;
        volume_change_24h: number;
      }[];
    }[];
  };
  detailCurrencyPost: {
    detail_post: {
      content: string;
      excerpt: string;
      feature_image: string;
      slug: string;
      title: string;
    };
    newest_post: {
      feature_image: string;
      publish_date: string;
      slug: string;
      title: string;
    }[];
  };
  lastestPost: any;
  dataCrypto: {
    status: {
        timestamp: string;
        total_count: number;
    };
    data: {
      circulating_supply: number;
      cmc_rank: number;
      id: number;
      last_updated: string;
      max_supply: number;
      total_supply: number;
      name: string;
      num_market_pairs: number;
      slug: string;
      symbol: string;
      quote: quoteObject[];
    }[];
  };
  resDetailCoin?: {
    data: {
      description: string;
      id: number;
      image: string;
      name: string;
      sourceCode: string;
      symbol: string;
      type: string;
      listCommunity: {
        linkCoins_Communities: string;
        nameCommunity: string;
      }[];
      listExplorer: {
        linkCoins_Explorers: string;
        nameExplorer: string;
      }[];
      listWebsite: {
        linkCoins_Websites: string;
        nameWebsite: string;
      }[];
    }[];
  };
}

export default function CryptoCurrencyDetailPageComponent({ 
  dataInterest,
  tableVietlot,
  dataExchange,
  dataBorrow,
  dataCurrencyEng,
  dataCurrencyVi,
  dataNav,
  detailCurrencyPost,
  lastestPost,
  dataCrypto,
  resDetailCoin
}: CryptoCurrencyDetailProps) {

  return (
    <div className='currency-detail-page-container'>

      <p className="bread-scrum-post-page">
        <a href="/">Trang chủ</a>
        <i className="fa fa-angle-right bread-scrum-divider" aria-hidden="true"></i>
        <a href="/tien-ao">Tiền ảo</a>
        <i className="fa fa-angle-right bread-scrum-divider post" aria-hidden="true"></i>
        <span>{dataCurrencyEng.name}</span>
      </p>

      <CryptoCurrencyNavbar currencyDataEng={dataNav}/>

      <div className='currency-detail-container'>
        <div className='row'>
          <div className='col-12 col-lg-8 currency-detail-body-left'>
            <CurrencyInfoDetail
              dataCurrencyEng={dataCurrencyEng}
              dataCurrencyVi={dataCurrencyVi}
            />
            {
              detailCurrencyPost && detailCurrencyPost.detail_post ?
                <CurrencyDetailPost showAuthor={false} postOfPage={detailCurrencyPost && detailCurrencyPost.detail_post ? detailCurrencyPost.detail_post : null} stockCode={null} />
              : 
              <div style={{marginTop: "24px"}}>
                {
                  resDetailCoin && resDetailCoin.data && resDetailCoin.data.length > 0 &&
                  <>
                    <h1 style={{marginBottom: "16px"}}>{`${resDetailCoin.data[0].name} là gì?`}</h1>
                    <p dangerouslySetInnerHTML={{ __html: resDetailCoin.data[0].description }}></p>
                  </>
                }   
              </div>
            }
          </div>
          <div className='col-12 col-lg-4 currency-detail-body-right'>
            <WidgetsDataCrypto dataCrypto = {dataCrypto}/>
            <WidgetDataByBank dataInterest={dataInterest} />
            <WidgetBorrowData dataBorrow={dataBorrow} />
            {dataExchange && <ExchangeRate dataExchange={dataExchange} />}
            <VietlotData tableVietlot={tableVietlot} />
          </div>
        </div>
      </div>

    </div>
  )
}