import React, { useEffect, useState } from 'react';
import { Table, Spinner } from 'reactstrap';
import {default as VNnum2words} from 'vn-num2words';

export interface CurrencyInfoDetailProps {
  dataCurrencyEng: {
    id: number;
    name: string;
    symbol: string;
    slug: string;
    num_market_pairs: number;
    max_supply: number;
    circulating_supply: number;
    total_supply: number;
    cmc_rank: number;
    last_updated: string;
    quote: {
      fully_diluted_market_cap: number;
      last_updated: string;
      market_cap: number;
      market_cap_dominance: number;
      percent_change_1h: number;
      percent_change_7d: number;
      percent_change_24h: number;
      percent_change_30d: number;
      percent_change_60d: number;
      percent_change_90d: number;
      price: number;
      volume_24h: number;
      volume_change_24h: number;
    }[];
  };
  dataCurrencyVi: {
    id: number;
    name: string;
    symbol: string;
    slug: string;
    num_market_pairs: number;
    max_supply: number;
    circulating_supply: number;
    total_supply: number;
    cmc_rank: number;
    last_updated: string;
    quote: {
      fully_diluted_market_cap: number;
      last_updated: string;
      market_cap: number;
      market_cap_dominance: number;
      percent_change_1h: number;
      percent_change_7d: number;
      percent_change_24h: number;
      percent_change_30d: number;
      percent_change_60d: number;
      percent_change_90d: number;
      price: number;
      volume_24h: number;
      volume_change_24h: number;
    }[];
  };
}

export interface DataCurrencyDetailProps {
  id: number;
  name: string;
  symbol: string;
  slug: string;
  num_market_pairs: number;
  max_supply: number;
  circulating_supply: number;
  total_supply: number;
  cmc_rank: number;
  last_updated: string;
  quote: {
    fully_diluted_market_cap: number;
    last_updated: string;
    market_cap: number;
    market_cap_dominance: number;
    percent_change_1h: number;
    percent_change_7d: number;
    percent_change_24h: number;
    percent_change_30d: number;
    percent_change_60d: number;
    percent_change_90d: number;
    price: number;
    volume_24h: number;
    volume_change_24h: number;
  }[];
}

export default function CurrencyInfoDetail({ 
  dataCurrencyEng,
  dataCurrencyVi,
}: CurrencyInfoDetailProps) {

  const [dataCurrencyDefault, setDataCurrencyDefault] = useState<DataCurrencyDetailProps>(dataCurrencyEng)
  const [currentLang, setCurrentLang] = useState('USD')

  useEffect(() => {
    
    switch (currentLang) {
      case 'USD':
        setDataCurrencyDefault(dataCurrencyEng)
        break;
      case 'VND':
        setDataCurrencyDefault(dataCurrencyVi)
        break;
      default:
        setDataCurrencyDefault(dataCurrencyEng)
        break;
    }
  }, [dataCurrencyEng, dataCurrencyVi, currentLang])

  const changeUnitLang = (unit) => {
    setCurrentLang(unit)
  }

  const formatNumberWithComma = (x) => {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }

  const renderColorPercent = (percent) => {
    if(Number(percent)) {
      if (Number(percent) > 0) {
        return "text-green"
      } else {
        return "text-red"
      }
    } else {
      return ""
    }
  }

  const renderUnitCurrency = (unitLang) => {
    if(unitLang == 'USD' ) {
      return '$'
    } else {
      return 'VNĐ'
    }
  }

  return (
    <div className='currency-detail'>
      <h1 className='title-currency'>{`Vốn hoá thị trường tiền ảo ${dataCurrencyEng.name} (${dataCurrencyEng.symbol})`}</h1>
      <div className='box-choose-unit'>
        <span className='unit-currency'>Đơn vị:</span>
        <div className='unit-container'>
          <span className={`unit-item ${currentLang == 'USD' ? 'active' : ''}`} onClick={() => changeUnitLang('USD')}>
            <img width={16} height={11} src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAMAAABBPP0LAAAAt1BMVEWSmb66z+18msdig8La3u+tYX9IaLc7W7BagbmcUW+kqMr/q6n+//+hsNv/lIr/jIGMnNLJyOP9/fyQttT/wb3/////aWn+YWF5kNT0oqz0i4ueqtIZNJjhvt/8gn//WVr/6+rN1+o9RKZwgcMPJpX/VFT9UEn+RUX8Ozv2Ly+FGzdYZrfU1e/8LS/lQkG/mbVUX60AE231hHtcdMb0mp3qYFTFwNu3w9prcqSURGNDaaIUMX5FNW5wYt7AAAAAjklEQVR4AR3HNUJEMQCGwf+L8RR36ajR+1+CEuvRdd8kK9MNAiRQNgJmVDAt1yM6kSzYVJUsPNssAk5N7ZFKjVNFAY4co6TAOI+kyQm+LFUEBEKKzuWUNB7rSH/rSnvOulOGk+QlXTBqMIrfYX4tSe2nP3iRa/KNK7uTmWJ5a9+erZ3d+18od4ytiZdvZyuKWy8o3UpTVAAAAABJRU5ErkJggg==" alt="USD" />
            USD
          </span>
          <span className={`unit-item ${currentLang == 'VND' ? 'active' : ''}`} onClick={() => changeUnitLang('VND')}>
            <img width={16} height={11} src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAMAAABBPP0LAAAATlBMVEX+AAD2AADvAQH/eXn+cXL9amr8YmL9Wlr8UlL7TkvoAAD8d0f6Pz/3ODf2Ly/0KSf6R0f6wTv60T31IBz6+jr4+Cv3QybzEhL4bizhAADgATv8AAAAW0lEQVR4AQXBgU3DQBRAMb+7jwKVUPefkQEQTYJqByBENpKUGoZslXoN5LPONH8G9WWZ7pGlOn6XZmaGRce1J/seei4dl+7dPWDqkk7+58e3+igdlySPcYbwBG+lPhCjrtt9EgAAAABJRU5ErkJggg==" alt="VND" />
            VND
          </span>
        </div>
      </div>
      <div className='row'>
        <div className='col-xs-12 col-sm-4 col-md-3 text-center'>
          <div className='coin-logo'>
            <img src={`https://coinmarketcap.vn/assets/images/coins/128x128/${dataCurrencyEng.id}.png`} width={64} height={64} alt="Bitcoin (BTC)" />
          </div>
          <div className='coin-info'>
            <h2 className='coin-info-name'>
              {dataCurrencyEng.name} <small>({dataCurrencyEng.symbol})</small> 
            </h2>
            <a className="btn btn-xs btn-success" href={`/tien-ao/${dataCurrencyEng.symbol}`} title="Bảng giá tiền ảo, tiền điện tử">{`Xếp hạng: ${dataCurrencyEng.cmc_rank}`}</a>
            <br />
            <a className="btn btn-xs btn-danger" href="/tien-ao" title="Bảng giá tiền ảo, tiền điện tử">Xem các đồng khác</a>
          </div>
        </div>
        <div className='col-xs-12 col-sm-8 col-md-9'>
          <div className='coin-price'>
            <span className='price-text'>{`${formatNumberWithComma(Math.round(dataCurrencyDefault.quote[0].price * 100) / 100)} ${renderUnitCurrency(currentLang)}`}</span>
            {" "}
            <span className={`price-text ${renderColorPercent(Math.round(dataCurrencyDefault.quote[0].percent_change_1h * 100) / 100)}`}>{`(${Math.round(dataCurrencyDefault.quote[0].percent_change_1h * 100) / 100}%)`}</span>
            <br />
            <small className="text-gray">{`1 ${dataCurrencyDefault.symbol} = 1.00000000 ${dataCurrencyDefault.symbol}`}</small>
          </div>
          <div className='table-container-currency-detail'>
            <Table className='currency-table-detail'>
              <tbody>
                <tr>
                  <th>Vốn hóa thị trường</th>
                  <td>
                    {`${formatNumberWithComma(Math.round(dataCurrencyDefault.quote[0].market_cap * 100) / 100)} ${renderUnitCurrency(currentLang)}`}
                  </td>
                </tr>
                <tr>
                  <th>Thanh khoản (24h)</th>
                  <td>
                    {`${formatNumberWithComma(Math.round(dataCurrencyDefault.quote[0].volume_24h * 100) / 100)} ${renderUnitCurrency(currentLang)}`}
                  </td>
                </tr>
                <tr>
                  <th>Tổng BTC hiện có</th>
                  <td>
                    {`${formatNumberWithComma(dataCurrencyDefault.circulating_supply)} ${dataCurrencyDefault.symbol}`}
                  </td>
                </tr>
                <tr>
                  <th>Dao động 1 giờ</th>
                  <td>
                    <span className={renderColorPercent(dataCurrencyDefault.quote[0].percent_change_1h)}>{`${Math.round(dataCurrencyDefault.quote[0].percent_change_1h * 100) / 100}%`}</span>
                  </td>
                </tr>
                <tr>
                  <th>Dao động 24 giờ</th>
                  <td>
                    <span className={renderColorPercent(dataCurrencyDefault.quote[0].percent_change_24h)}>{`${Math.round(dataCurrencyDefault.quote[0].percent_change_24h * 100) / 100}%`}</span>
                  </td>
                </tr>
                <tr>
                  <th>Dao động 7 ngày</th>
                  <td>
                    <span className={renderColorPercent(dataCurrencyDefault.quote[0].percent_change_7d)}>{`${Math.round(dataCurrencyDefault.quote[0].percent_change_7d * 100) / 100}%`}</span>
                  </td>
                </tr>
              </tbody>
            </Table>
          </div>
        </div>
        <div className='col-12'>
          {
            dataCurrencyDefault && <>
              <p className='description-currency'>
                Cập nhật giá đồng <strong>{`${dataCurrencyDefault.name} (${dataCurrencyDefault.symbol})`}</strong> hôm nay là <strong>{`${formatNumberWithComma(Math.round(dataCurrencyDefault.quote[0].price * 100) / 100)} ${renderUnitCurrency(currentLang)}`}</strong>, quy đổi ra tiền Việt Nam là <strong>{`${formatNumberWithComma(Math.round(dataCurrencyVi.quote[0].price * 100) / 100)} VNĐ`}</strong> (bằng chữ: <strong>{VNnum2words(Math.round(dataCurrencyVi.quote[0].price))} đồng</strong>) với khối lượng giao dịch trong 24 giờ là <span>{`${formatNumberWithComma(Math.round(dataCurrencyDefault.quote[0].volume_24h * 100) / 100)} ${renderUnitCurrency(currentLang)} (${formatNumberWithComma(Math.round(dataCurrencyVi.quote[0].volume_24h * 100) / 100)} VNĐ)`}</span>.
              </p>
              <p className='text-currency-detail'>
                <span>
                  24h gần nhất Giá
                  <strong>{` ${dataCurrencyDefault.name} (${dataCurrencyDefault.symbol}) `}</strong>
                </span>
                <span className={renderColorPercent(dataCurrencyDefault.quote[0].percent_change_24h)}>
                  {`${Number(dataCurrencyDefault.quote[0].percent_change_24h) > 0 ? 'tăng' : 'giảm'} ${Math.round(dataCurrencyDefault.quote[0].percent_change_24h * 100) / 100}%`}
                </span>
                .{" "}
                <span>
                  7 ngày gần nhất, Đồng
                  <strong>{` ${`${dataCurrencyDefault.name} (${dataCurrencyDefault.symbol})`} `}</strong>
                </span>
                <span className={renderColorPercent(dataCurrencyDefault.quote[0].percent_change_7d)}>
                  {`${Number(dataCurrencyDefault.quote[0].percent_change_7d) > 0 ? 'tăng' : 'giảm'} ${Math.round(dataCurrencyDefault.quote[0].percent_change_7d * 100) / 100}%`}
                </span>
                .
              </p>
              <p className='text-currency-detail'>
              Bảng giá này được đồng bộ trực tiếp từ hệ thống bảng giá tiền điện tử <a target='_blank' href='https://coinmarketcap.com/' rel='noreferrer'>CoinMarketCap</a> lớn nhất thế giới.
              </p>
            </>
          }
        </div>
        <div className="col-12 coin-chart">
          <iframe 
            id="tradingview_ba264" 
            src={`https://s.tradingview.com/widgetembed/?frameElementId=tradingview_ba264&symbol=BINANCE:${dataCurrencyEng.symbol}USDT&interval=60&symboledit=1&saveimage=1&toolbarbg=f1f3f6&details=1&studies=%5B%5D&hideideas=1&theme=light&style=1&timezone=Asia%2FHo_Chi_Minh&withdateranges=1&studies_overrides=%7B%7D&overrides=%7B%7D&enabled_features=%5B%5D&disabled_features=%5B%5D&locale=vi_VN&referral_id=1713&utm_source=webgia.com&utm_medium=widget&utm_campaign=chart&utm_term=BINANCE%3ABTCUSDT`} 
            style={{width: "100%", height: "100%", margin: "0 !important", padding: "0 !important"}} 
            frameBorder="0" 
            allowTransparency 
            scrolling="no" 
            allowFullScreen
          ></iframe>
        </div>
      </div>
    </div>
  )
}