import React, { useEffect, useState } from 'react';

export interface CurrencyDetailProps {}

export default function CurrencyDetail({ 

}: CurrencyDetailProps) {

  return (
    <div className='currency-detail'>
      <div className='row'>
        <div className='col-xs-12 col-sm-4 col-md-3 text-center'>
          <div className='coin-logo'>
            <img src="https://coinmarketcap.vn/assets/images/coins/128x128/1.png" width={64} height={64} alt="Bitcoin (BTC)" />
          </div>
          <div className='coin-info'>
            <h2 className='coin-info-name'>
              Bitcoin <small>(BTC)</small> 
            </h2>
            <a className="btn btn-xs btn-success" href="#" title="Bảng giá tiền ảo, tiền điện tử">Xếp hạng: 1</a>
            <br />
            <a className="btn btn-xs btn-danger" href="#" title="Bảng giá tiền ảo, tiền điện tử">Xem các đồng khác</a>
          </div>
        </div>
        <div className='col-xs-12 col-sm-8 col-md-9'></div>
      </div>
    </div>
  )
}