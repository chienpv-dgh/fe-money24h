import React, { Component } from 'react';
import NumberFormat from 'react-number-format';

interface IRecipeProps {}
interface IRecipeState {
  deposits?: number;
  depositRate?: number;
  depositTerm?: number;
  interestReceive?: number;
  totalReceive?: number;
}
class ToolCalculateSaving extends Component<IRecipeProps,IRecipeState> {

  constructor(props) {
    super(props);
    this.state = {
      deposits: null,
      depositRate: null,
      depositTerm: null,
      interestReceive: null,
      totalReceive: null
    }
  }

  numberWithCommas = (number) => {
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  }

  calculatorSaving = async () => {
    let deposits = this.state.deposits
    let depositRate = this.state.depositRate
    let depositTerm = this.state.depositTerm

    let errorDeposits = ""
    let errorDepositRate = ""
    let errorDepositTerm = ""

    if(deposits) {
      errorDeposits = ""
    } else {
      errorDeposits = "Error"
    }

    if(depositRate) {
      errorDepositRate = ""
    } else {
      errorDepositRate = "Error"
    }

    if(depositTerm) {
      errorDepositTerm = ""
    } else {
      errorDepositTerm = "Error"
    }

    if(errorDeposits || errorDepositRate || errorDepositTerm) {
      return
    } else {
      let interestReceive = ((deposits * (depositRate / 100)) / 12) * depositTerm
      let totalReceive = interestReceive + deposits
      await this.setState({
        interestReceive: Math.round(interestReceive),
        totalReceive: Math.round(totalReceive)
      })
    }
  }

  render() {
    return (
      <div className="block-tool-calc-saving">
        <div className="contain-calc-tool">
          <p className="calc-tool-main-title">CÔNG CỤ TÍNH LÃI TIỀN GỬI NGÂN HÀNG</p>
          <div className="block-calc-content row">
            <div className="block-calc-content-left col-12 col-lg-5">
              <div className="calc-content-left-item">
                <p className="content-left-title">Số tiền gửi *</p>
                <NumberFormat className="custom-input-calc-left" value={this.state.deposits} thousandSeparator={"."} decimalSeparator={","}
                  onValueChange={(values) => {
                    const {formattedValue, value} = values;
                    this.setState({deposits: Number(value)}, () => this.calculatorSaving())
                  }}
                />
                <span className="icon-end-input"><img src="/images/icon-viet-nam-dong.svg" alt=""/></span>
              </div>
              <div className="calc-content-left-item">
                <p className="content-left-title">Lãi suất gửi *</p>
                <NumberFormat className="custom-input-calc-left" value={this.state.depositRate} thousandSeparator={"."} decimalSeparator={","}
                  onValueChange={(values) => {
                    const {formattedValue, value} = values;
                    this.setState({depositRate: Number(value)}, () => this.calculatorSaving())
                  }}
                />
                <span className="icon-end-input">%/Năm</span>
              </div>
              <div className="calc-content-left-item">
                <p className="content-left-title">Kỳ hạn gửi *</p>
                <NumberFormat className="custom-input-calc-left" value={this.state.depositTerm} thousandSeparator={"."} decimalSeparator={","}
                  onValueChange={(values) => {
                    const {formattedValue, value} = values;
                    this.setState({depositTerm: Number(value)}, () => this.calculatorSaving())
                  }}
                />
                <span className="icon-end-input">Tháng</span>
              </div>
              <p className="description-text">(*): Thông tin bắt buộc</p>
              <p className="description-text">Lưu ý: Lãi tiền gửi ước tính theo phương thức trả lãi cuối kỳ</p>
            </div>
            <div className="block-calc-content-right col-12 col-lg-5">
              <div className="calc-tool-box">
                <p className="calc-tool-box-tile">Số tiền lãi nhận được</p>
                <div className="calc-tool-box-value">
                  <span>{this.numberWithCommas(this.state.interestReceive ? this.state.interestReceive : 0)}</span>
                  <img src="/images/icon-unit-blue.svg" alt="icon-unit-blue"/>
                </div>
                <p className="calc-tool-box-tile">Tổng số tiền nhận được khi đến hạn</p>
                <div className="calc-tool-box-value">
                  <span>{this.numberWithCommas(this.state.totalReceive ? this.state.totalReceive : 0)}</span>
                  <img src="/images/icon-unit-blue.svg" alt="icon-unit-blue"/>
                </div>
              </div>
              <div className="view-current-interest-rate">
                <div className="border-box">
                  <p className="border-box-title">XEM LÃI SUẤT HIỆN TẠI</p>
                  <img className="border-box-image" src="/images/icon-arow-blue.svg" alt="icon-arow-blue"/>
                </div>
              </div>
            </div>
          </div>  
        </div>
      </div>
    );
  }
}

export default ToolCalculateSaving;