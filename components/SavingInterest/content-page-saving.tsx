import React, { Component } from 'react';
import WidgetsDataCrypto from '../widgets/DataCryptoCurrency';

export interface quoteObject{
  fully_diluted_market_cap: number;
  last_updated: string;
  market_cap: number;
  market_cap_dominance: number;
  percent_change_1h: number;
  percent_change_7d: number;
  percent_change_24h: number;
  percent_change_30d: number;
  percent_change_60d: number;
  percent_change_90d: number;
  price: number;
  volume_24h: number;
  volume_change_24h: number;
}
interface IRecipeProps {
  AllPosts?: any;
  postOfPage?: any;
  stockCode?: string;
  showAuthor: boolean;
  dataCrypto: {
    status: {
        timestamp: string;
        total_count: number;
    };
    data: {
      circulating_supply: number;
      cmc_rank: number;
      id: number;
      last_updated: string;
      max_supply: number;
      total_supply: number;
      name: string;
      num_market_pairs: number;
      slug: string;
      symbol: string;
      quote: quoteObject[];
    }[];
  };
}
interface IRecipeState {}

class ContentPageSaving extends Component<IRecipeProps,IRecipeState> {
  
  formatDateTime = (data) => {

    let date = new Date(data)

    let getDate = date.getDate()
    let getMonth = date.getMonth() + 1
    let getFullyear = date.getFullYear()
    
    return `${getDate} tháng ${getMonth} ${getFullyear}`
  }

  componentDidMount() {
    const script = document.createElement('script');
    
    script.src = "/library/toc/handle.js";
    script.async = true;

    document.body.appendChild(script);
    
    return () => {
      document.body.removeChild(script);
    }
  }

  clickToShare = (type, url, title?) => {
    switch (type) {
      case 1:
        window.open(`https://www.facebook.com/sharer/sharer.php?u=${url}&amp;src=sdkpreparse`, '_blank');
        break;
      case 2:
        window.open(`https://twitter.com/share?text=${title};url=${url}`, '_blank');
        break;
      case 3:
        window.open(`https://www.reddit.com/submit?url=${url}&title=${title}`, '_blank');
        break;
      case 4:
        window.open(`https://www.linkedin.com/sharing/share-offsite/?url=${url}`, '_blank');
        break;
      case 5:
        window.open(`https://pinterest.com/pin/create/button/?url=${url}`, '_blank');
        break;
      case 6:
        window.open(`whatsapp://send?text=${url}`, '_blank');
        break;
      default:
        break;
    }
  }
  
  render() {
    
    return (
      <div className="block-content-page">
        <div className="row">
          <div className="content-page-detail-left col-12 col-lg-8">
            {this.props.postOfPage ?
              <>
              {this.props.postOfPage && <p className="content-page-detail-left-title" dangerouslySetInnerHTML={{ __html: this.props.postOfPage.title }}></p>}
              {this.props.postOfPage.author && this.props.showAuthor && <div className="box-info-author">
                <div className="info-author-left">
                  <a href={`/author/${this.props.postOfPage.author.slug}`}>
                    <img className="box-info-author-image" src={this.props.postOfPage.author && this.props.postOfPage.author.profile_image ? this.props.postOfPage.author.profile_image : "/images/default-user-avatar.svg"} width="100%" height="100%" alt="author-image"/>
                  </a>
                </div>
                <div className="info-author-right">
                  <p className="info-author-public">{this.props.postOfPage.publish_date}</p>
                  <p className="info-author-name">
                    <span>By {" "}</span>
                    <a href={`/author/${this.props.postOfPage.author.slug}`} className="info-author-name-link">
                      <span>{this.props.postOfPage.author.name}</span>
                    </a>
                  </p>
                </div>
              </div>}
              {this.props.postOfPage.feature_image && <img className="image-cover-article" src={this.props.postOfPage.feature_image ? this.props.postOfPage.feature_image : "/images/image-post-default.png"} width="100%" height="auto" alt="image-cover"/>}
              {this.props.postOfPage && <div className="box-article-html">
              <div className="box-share-artilce">
                <div className="box-share-artilce-stiky">
                  <div className="box-share-artilce-item">
                    <span style={{ cursor: "pointer" }} onClick={() => this.clickToShare(1, `https://money24h.vn/blog/${this.props.postOfPage.slug}`)}>
                      <img src="/images/icon-share-facebook.svg" alt="icon-share-facebook" width="36px" height="36px"/>
                    </span>
                  </div>
                  <div className="box-share-artilce-item">
                    <span style={{ cursor: "pointer" }} onClick={() => this.clickToShare(2, `https://money24h.vn/blog/${this.props.postOfPage.slug}`)}>
                      <img src="/images/icon-share-twitter.svg" alt="icon-share-twitter" width="36px" height="36px"/>
                    </span>
                  </div>
                  <div className="box-share-artilce-item">
                    <span style={{ cursor: "pointer" }} onClick={() => this.clickToShare(3, `https://money24h.vn/blog/${this.props.postOfPage.slug}`, this.props.postOfPage.title)}>
                      <img src="/images/icon-share-reddit.svg" alt="icon-share-reddit" width="36px" height="36px"/>
                    </span>
                  </div>
                  <div className="box-share-artilce-item">
                    <span style={{ cursor: "pointer" }} onClick={() => this.clickToShare(4, `https://money24h.vn/blog/${this.props.postOfPage.slug}`)}>
                      <img src="/images/icon-share-linkedin.svg" alt="icon-share-linkedin" width="36px" height="36px"/>
                    </span>
                  </div>
                  <div className="box-share-artilce-item">
                    <span style={{ cursor: "pointer" }} onClick={() => this.clickToShare(5, `https://money24h.vn/blog/${this.props.postOfPage.slug}`)}>
                      <img src="/images/icon-share-pinterest.svg" alt="icon-share-pinterest" width="36px" height="36px"/>
                    </span>
                  </div>
                </div>
              </div>
              <aside className="toc"></aside>
              <div className="render-html-page" dangerouslySetInnerHTML={{ __html: this.props.postOfPage.content }}></div>
              <div className="box-share-mobile">
                <div className="share-mobile-block">
                  <div className="share-mobile-block-item">
                    <span onClick={() => this.clickToShare(1, `https://money24h.vn/blog/${this.props.postOfPage.slug}`)}>
                      <div className="box-mobile-share-contain facebook">
                        <img className="image-share-mobile" src="/images/icon-share-facebook-mobile.svg" width="15px" height="15px" alt="icon-share-facebook-mobile"/>
                        <span className="text-share-mobile">Share</span>
                      </div>
                    </span>
                  </div>
                  <div className="share-mobile-block-item">
                    <span onClick={() => this.clickToShare(2, `https://money24h.vn/blog/${this.props.postOfPage.slug}`, this.props.postOfPage.title)}>
                      <div className="box-mobile-share-contain tweet">
                        <img className="image-share-mobile" src="/images/icon-share-twitter-mobile.svg" width="15px" height="15px" alt="icon-share-twitter-mobile"/>
                        <span className="text-share-mobile">Tweet</span>
                      </div>
                    </span>
                  </div>
                  <div className="share-mobile-block-item">
                    <span onClick={() => this.clickToShare(6, `https://money24h.vn/blog/${this.props.postOfPage.slug}`)}>
                      <div className="box-mobile-share-contain send">
                        <img className="image-share-mobile" src="/images/icon-share-whatapp-mobile.svg" width="15px" height="15px" alt="icon-share-whatapp-mobile"/>
                        <span className="text-share-mobile">Send</span>
                      </div>
                    </span>
                  </div>
                </div>
              </div>
            </div>}
              </> : <p>Đang cập nhật...</p>
            }
          </div>
          <div className="content-page-detail-right col-12 col-lg-4">
            <div className="block-lastest-news">
              <p className="lastest-news-box-highlight">PHÂN TÍCH KỸ THUẬT</p>
              <div className="divider-block-suggest-right"></div>
              <div className="box-trading-view" dangerouslySetInnerHTML={{__html: `
                <!-- TradingView Widget BEGIN -->
                <div class="tradingview-widget-container">
                  <div class="tradingview-widget-container__widget"></div>
                  <div class="tradingview-widget-copyright"><a href="https://vn.tradingview.com/symbols/${this.props.stockCode ? this.props.stockCode : "HOSE-VCB"}/technicals/" rel="noreferrer" target="_blank"><span class="blue-text">Phân tích Kỹ thuật cho ${this.props.stockCode ? this.props.stockCode.split("-")[1] : "VCB"}</span></a> bởi TradingView</div>
                  <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-technical-analysis.js" async>
                  {
                  "interval": "1W",
                  "width": 425,
                  "isTransparent": true,
                  "height": 450,
                  "symbol": "${this.props.stockCode ? this.props.stockCode.split("-").join(":") : "HOSE:VCB"}",
                  "showIntervalTabs": true,
                  "locale": "vi_VN",  
                  "colorTheme": "light"
                }
                  </script>
                </div>
                <!-- TradingView Widget END -->
              `}}>
              </div>
            </div>
            <WidgetsDataCrypto dataCrypto={this.props.dataCrypto} />
            <div className="block-lastest-news">
              <p className="lastest-news-box-highlight">BÀI VIẾT MỚI NHẤT</p>
              <div className="divider-block-suggest-right"></div>
              <div className="box-news-lastest-article">
                {this.props.AllPosts && 
                  <>
                    <a href={`/blog/${this.props.AllPosts[0].slug}`}>
                      <div className="hot-item-lastest">
                        <img className="hot-item-image" src={this.props.AllPosts[0].feature_image ? this.props.AllPosts[0].feature_image : "/images/image-post-default.png"} alt="image-lastest-article" width="100%" height="100%"/>
                        <h3 className="hot-item-title">{this.props.AllPosts[0].title}</h3>
                      </div>
                    </a>
                    <div className="divider-article"></div>
                  </>
                }
                <div className="news-item-normal">
                  {this.props.AllPosts && this.props.AllPosts.slice(1, 5).map((item, index) => (
                    <React.Fragment key={index}>
                      <a href={`/blog/${item.slug}`}>
                        <div className="news-item-normal-detail">
                          <img className="item-detail-image" src={item.feature_image ? item.feature_image : "/images/image-post-default.png"} alt="image-lastest-article" width="100%" height="100%"/>
                          <h3 className="item-detail-title">{item.title}</h3>
                        </div>
                      </a>
                      <div className="divider-article"></div>
                    </React.Fragment>
                  ))}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ContentPageSaving;