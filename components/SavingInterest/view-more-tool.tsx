import React, { Component } from 'react';

const arrTag = [
  {
    itemTitle: "Tính lãi suất tiết kiệm",
    itemLink: "/cong-cu-tinh-tien-lai-gui-ngan-hang",
    itemImage: "/images/cong-cu-ltinh-lai-suat-gui-tiet-kiem.svg",
  },
  {
    itemTitle: "Tính lãi suất vay",
    itemLink: "/cong-cu-tinh-toan-khoan-vay-ngan-hang",
    itemImage: "/images/cong-cu-tinh-lai-suat-vay.svg"
  },
  {
    itemTitle: "Tính tỷ giá ngoại tệ",
    itemLink: "/cong-cu-tinh-ty-gia-ngoai-te",
    itemImage: "/images/cong-cu-tinh-ti-gia-ngoai-te.svg"
  },
  {
    itemTitle: "Tra cứu lãi suất tiết kiệm",
    itemLink: "/lai-suat-gui-tiet-kiem-ngan-hang",
    itemImage: "/images/tra-cuu-lai-suat-tiet-kiem.svg"
  },
  {
    itemTitle: "Tra cứu lãi suất vay",
    itemLink: "/lai-suat-vay-ngan-hang",
    itemImage: "/images/tra-cuu-lai-suat-vay.svg"
  },
  {
    itemTitle: "Tra cứu tỷ giá ngoại tệ",
    itemLink: "/cong-cu-tinh-ty-gia-ngoai-te",
    itemImage: "/images/tra-cuu-ti-gia-ngoai-te.svg"
  }
]

class ViewMoreTool extends Component {
  render() {
    return (
      <div className="block-view-more-tool">
        <p className="view-more-tool-title">CÔNG CỤ TÀI CHÍNH</p>
        <div className="view-more-tool-item row">
          {arrTag.map((item, index) => 
            <div className="view-more-tool-item-detail col-4" key={index}>
              <a href={item.itemLink}>
                <div className="item-detail-box">
                  <div className="item-detail-box-border">
                    <p className="view-more-tool-item-detai-text">{item.itemTitle}</p>
                    <img className="border-box-image" src="/images/icon-arow-blue.svg" alt="icon-arow-blue"/>
                  </div>
                </div>
                <div className="item-detail-box-moblie">
                  <img src={item.itemImage} alt="image-more-tool" width="50px" height="50px"/>
                  <p className="item-detail-box-moblie-text">{item.itemTitle}</p>
                </div>
              </a>
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default ViewMoreTool;