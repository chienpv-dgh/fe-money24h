import React, { useEffect, useState } from 'react';
import dynamic from "next/dynamic";
// import DatePicker from "react-datepicker";
import LazyLoad from 'react-lazyload';
import Image from 'next/image';
// import { addDays } from "date-fns";
// import { Table } from 'reactstrap';
// import "react-datepicker/dist/react-datepicker.css";
// import MultiAxisLine from './gold-chart';
// import { ArticleInterested } from '../NewsDetail';

// const StockHome = dynamic(() => import("../modules/HomePage/stock-home"), {ssr: false});
const WidgetDataByBank = dynamic(() => import("../widgets/DataByBank"));
const VietlotData = dynamic(() => import("../widgets/DataVietlot"));
// const ArticleInterested = dynamic(() => import("../NewsDetail/article-interested"));
const WidgetBorrowData = dynamic(() => import('../widgets/DataBorrow'));
const ExchangeRate = dynamic(() => import('../widgets/DataExchange'));

export default function GoldTodayComponent({ 
  dataInterest, 
  tableVietlot, 
  dataExchange, 
  dataBorrow, 
  detailPost,
  newestPost,
  dataPostByCate,
  postOfPage
}) {

  const dataTable = [
    {
      name: "DOJI HN",
      buyToday: "55500000",
      buyRate: 1,
      sellToday: "56100000",
      sellRate: 0,
      buyYesterday: "55400000",
      sellYesterday: "56600000"
    },
    {
      name: "DOJI HN",
      buyToday: "55500000",
      buyRate: 0,
      sellToday: "56100000",
      sellRate: 1,
      buyYesterday: "55400000",
      sellYesterday: "56600000"
    },{
      name: "DOJI HN",
      buyToday: "55500000",
      buyRate: -1,
      sellToday: "56100000",
      sellRate: 0,
      buyYesterday: "55400000",
      sellYesterday: "56600000"
    },{
      name: "DOJI HN",
      buyToday: "55500000",
      buyRate: 1,
      sellToday: "56100000",
      sellRate: -1,
      buyYesterday: "55400000",
      sellYesterday: "56600000"
    },{
      name: "DOJI HN",
      buyToday: "55500000",
      buyRate: 1,
      sellToday: "56100000",
      sellRate: 0,
      buyYesterday: "55400000",
      sellYesterday: "56600000"
    },{
      name: "DOJI HN",
      buyToday: "55500000",
      buyRate: 1,
      sellToday: "56100000",
      sellRate: 0,
      buyYesterday: "55400000",
      sellYesterday: "56600000"
    },{
      name: "DOJI HN",
      buyToday: "55500000",
      buyRate: 1,
      sellToday: "56100000",
      sellRate: 0,
      buyYesterday: "55400000",
      sellYesterday: "56600000"
    },
  ]

  const goldCode = [
    {
      name: "HCM",
      code: 1
    },
    {
      name: "HN",
      code: 2
    },
    {
      name: "AG",
      code: 3
    },
    {
      name: "KG",
      code: 4
    },
  ]

  const [dateSelected, setDateSelected] = useState(new Date())

  const numberWithCommas = (number) => {
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  }

  const renderRateCompare = (rate) => {
    let image = ""
    switch (rate) {
      case 1:
        image = "icon-increase-curentcy.svg"
        break;
      case -1:
        image = "icon-decrease-currentcy.svg"
        break;
      default:
        break;
    }
    return image
  }

  const getDatChart = (code) => {
    console.log("-----> Code", code)
  }

  const formatDateTime = () => {
    
    let date = new Date()
    let getDate = date.getDate()
    let getMonth = date.getMonth() + 1
    let getFullyear = date.getFullYear()

    return `${getDate}/${getMonth}/${getFullyear}`
  }

  const clickToShare = (type, url, title?) => {
    switch (type) {
      case 1:
        window.open(`https://www.facebook.com/sharer/sharer.php?u=${url}&amp;src=sdkpreparse`, '_blank');
        break;
      case 2:
        window.open(`https://twitter.com/share?text=${url}`, '_blank');
        break;
      case 3:
        window.open(`https://www.reddit.com/submit?url=${url}&title=${title}`, '_blank');
        break;
      case 4:
        window.open(`https://www.linkedin.com/sharing/share-offsite/?url=${url}`, '_blank');
        break;
      case 5:
        window.open(`https://pinterest.com/pin/create/button/?url=${url}`, '_blank');
        break;
      case 6:
        window.open(`whatsapp://send?text=${url}`, '_blank');
        break;
      default:
        break;
    }
  }

  return (
    <div className="container-block-new-detail gold-to-day">
      {detailPost && detailPost.detail_post && <p className="bread-scrum-post-page">
        <a href="/">Trang chủ</a>
        <i className="fa fa-angle-right bread-scrum-divider" aria-hidden="true"></i>
        <a href={`/tag/${detailPost.detail_post.category[0].slug}`}>{detailPost.detail_post.category[0].name}</a>
        <i className="fa fa-angle-right bread-scrum-divider post" aria-hidden="true"></i>
        <span dangerouslySetInnerHTML={{ __html: detailPost.detail_post.title }}></span>
      </p>}
      {/* <ins className="adsbygoogle"
        style={{ display: "block", textAlign: "center" }}
        data-ad-layout="in-article"
        data-ad-format="fluid"
        data-ad-client="ca-pub-5581868270899806"
        data-ad-slot="1179910330">
      </ins> */}
      <div className="row" style={{
        width: "100%",
        margin: "0px"
      }}>
        <div className="block-detail-left col-12 col-lg-8">
          <h1 className="title-blog-detail">{`Giá vàng hôm nay ${formatDateTime()}`}</h1>
          {/* <div className="block-body-gold-today">
            <div className="block-table-gold">
              <div className="date-choose-box">
                <p className="title">Giá vàng</p>
                <div className="box-contain-date">
                  <DatePicker dateFormat="dd/MM/yyyy" maxDate={new Date()} selected={dateSelected} onChange={date => setDateSelected(date)} className="custom-chooose-date"/>
                  <img className="icon-calendar" src="images/calendar-icon.svg" alt="icon-calendar"/>
                </div>
              </div>
              <Table responsive>
                <thead>
                  <tr>
                    <th></th>
                    <th colSpan={2}>Hôm nay (03/03/2021)</th>
                    <th colSpan={2}>Hôm qua (02/03/2021)</th>
                  </tr>
                  <tr>
                    <th>Đơn vị: đồng/lượng</th>
                    <th>Giá mua</th>
                    <th>Giá bán</th>
                    <th>Giá mua</th>
                    <th>Giá bán</th>
                  </tr>
                </thead>
                <tbody>
                  {dataTable ?
                    dataTable.map((item, index) => (
                      <tr key={index}>
                        <td>{item.name}</td>
                        <td>
                          <span>{item.buyToday ? numberWithCommas(item.buyToday) : "-"}</span>
                          {item.buyRate ? <img className="" width={15} height={11} src={`/images/${renderRateCompare(item.buyRate)}`} alt="status"/> : null}
                        </td>
                        <td>
                          <span>{item.sellToday ? numberWithCommas(item.sellToday) : "-"}</span>
                          {item.sellRate ? <img className="" width={15} height={11} src={`/images/${renderRateCompare(item.sellRate)}`} alt="status"/> : null}
                        </td>
                        <td>{item.buyYesterday ? numberWithCommas(item.buyYesterday) : "-"}</td>
                        <td>{item.sellYesterday ? numberWithCommas(item.sellYesterday) : "-"}</td>
                      </tr>
                    ))
                    :
                    null
                  }
                </tbody>
              </Table>
              <div className="description-table">
                <div className="update">
                  <p>Cập nhật 23:58 (03/03/2021)</p>
                </div>
                <div className="introduce">
                  <img className="" width={15} height={11} src={`/images/icon-increase-curentcy.svg`} alt="status"/>
                  <img className="" width={15} height={11} src={`/images/icon-decrease-currentcy.svg`} alt="status"/>
                  <span>Tăng giảm so sánh với ngày trước đó</span>
                </div>
              </div>
            </div>

            <div className="divide-gold-table"></div>

            <div className="block-chart-gold">
              <div className="box-choose-gold-code">
                <p className="title">Biểu đồ giá vàng 30 ngày gần nhất</p>
                <div className="choose-code">
                  <span>Chọn mã vàng</span>
                  <select onChange={(e) => getDatChart(e.target.value)} name="gold-code" id="gold-code" className="gold-code">
                    {goldCode && goldCode.map((item, index) => <option key={index} value={item.code}>{item.name}</option>)}
                  </select>
                </div>
              </div>
              <div>
                <MultiAxisLine />
              </div>
            </div>

            <div className="divide-gold-table"></div>

          </div> */}
          
          <div className="box-article-html">
            <iframe src="https://tygia.com/api.php?column=3&amp;cols=2&amp;title=0&amp;chart=1&amp;gold=1&amp;rate=1&amp;expand=2&amp;color=aa1111&amp;nganhang=VIETCOM&amp;fontsize=80" title="W3Schools Free Online Web Tutorials" width="100%" height="880px"></iframe>
          </div>

          {postOfPage ?
              <>
              {postOfPage && <h2 className="content-page-detail-left-title" dangerouslySetInnerHTML={{ __html: postOfPage.title }}></h2>}
              {postOfPage.author && <div className="box-info-author">
                <div className="info-author-left">
                  <a href={`/author/${postOfPage.author.slug}`}>
                    <img className="box-info-author-image" src={postOfPage.author && postOfPage.author.profile_image ? postOfPage.author.profile_image : "/images/default-user-avatar.svg"} width="100%" height="100%" alt="author-image"/>
                  </a>
                </div>
                <div className="info-author-right">
                  <p className="info-author-public">{postOfPage.publish_date}</p>
                  <p className="info-author-name">
                    <span>By {" "}</span>
                    <a href={`/author/${postOfPage.author.slug}`} className="info-author-name-link">
                      <span>{postOfPage.author.name}</span>
                    </a>
                  </p>
                </div>
              </div>}
              {postOfPage.feature_image && <img className="image-cover-article" src={postOfPage.feature_image ? postOfPage.feature_image : "/images/image-post-default.png"} width="100%" height="auto" alt="image-cover"/>}
              {postOfPage && <div className="box-article-html">
              <div className="box-share-artilce">
                <div className="box-share-artilce-stiky">
                  <div className="box-share-artilce-item">
                    <span style={{ cursor: "pointer" }} onClick={() => clickToShare(1, `https://money24h.vn/blog/${postOfPage.slug}`)}>
                      <img src="/images/icon-share-facebook.svg" alt="icon-share-facebook" width="36px" height="36px"/>
                    </span>
                  </div>
                  <div className="box-share-artilce-item">
                    <span style={{ cursor: "pointer" }} onClick={() => clickToShare(2, `https://money24h.vn/blog/${postOfPage.slug}`, postOfPage.title)}>
                      <img src="/images/icon-share-twitter.svg" alt="icon-share-twitter" width="36px" height="36px"/>
                    </span>
                  </div>
                  <div className="box-share-artilce-item">
                    <span style={{ cursor: "pointer" }} onClick={() => clickToShare(3, `https://money24h.vn/blog/${postOfPage.slug}`, postOfPage.title)}>
                      <img src="/images/icon-share-reddit.svg" alt="icon-share-reddit" width="36px" height="36px"/>
                    </span>
                  </div>
                  <div className="box-share-artilce-item">
                    <span style={{ cursor: "pointer" }} onClick={() => clickToShare(4, `https://money24h.vn/blog/${postOfPage.slug}`)}>
                      <img src="/images/icon-share-linkedin.svg" alt="icon-share-linkedin" width="36px" height="36px"/>
                    </span>
                  </div>
                  <div className="box-share-artilce-item">
                    <span style={{ cursor: "pointer" }} onClick={() => clickToShare(5, `https://money24h.vn/blog/${postOfPage.slug}`)}>
                      <img src="/images/icon-share-pinterest.svg" alt="icon-share-pinterest" width="36px" height="36px"/>
                    </span>
                  </div>
                </div>
              </div>
              <aside className="toc"></aside>
              <div className="render-html-page" dangerouslySetInnerHTML={{ __html: postOfPage.content }}></div>
              <div className="box-share-mobile">
                <div className="share-mobile-block">
                  <div className="share-mobile-block-item">
                    <span onClick={() => clickToShare(1, `https://money24h.vn/blog/${postOfPage.slug}`)}>
                      <div className="box-mobile-share-contain facebook">
                        <img className="image-share-mobile" src="/images/icon-share-facebook-mobile.svg" width="15px" height="15px" alt="icon-share-facebook-mobile"/>
                        <span className="text-share-mobile">Share</span>
                      </div>
                    </span>
                  </div>
                  <div className="share-mobile-block-item">
                    <span onClick={() => clickToShare(2, `https://money24h.vn/blog/${postOfPage.slug}`, postOfPage.title)}>
                      <div className="box-mobile-share-contain tweet">
                        <img className="image-share-mobile" src="/images/icon-share-twitter-mobile.svg" width="15px" height="15px" alt="icon-share-twitter-mobile"/>
                        <span className="text-share-mobile">Tweet</span>
                      </div>
                    </span>
                  </div>
                  <div className="share-mobile-block-item">
                    <span onClick={() => clickToShare(6, `https://money24h.vn/blog/${postOfPage.slug}`)}>
                      <div className="box-mobile-share-contain send">
                        <img className="image-share-mobile" src="/images/icon-share-whatapp-mobile.svg" width="15px" height="15px" alt="icon-share-whatapp-mobile"/>
                        <span className="text-share-mobile">Send</span>
                      </div>
                    </span>
                  </div>
                </div>
              </div>
            </div>}
              </> : <p>Đang cập nhật...</p>
            }

        </div>
        <div className="block-suggest-right col-12 col-lg-4">
          <div className="block-check-interest-rate">
            <WidgetDataByBank dataInterest={dataInterest} />
            <WidgetBorrowData dataBorrow={dataBorrow} />
            {dataExchange && <ExchangeRate dataExchange={dataExchange} />}
            {/* <StockHome /> */}
            <VietlotData tableVietlot={tableVietlot} />
          </div>
          <div className="block-lastest-news">
            <h3 className="lastest-news-box-highlight">Bài viết mới nhất</h3>
            <div className="divider-block-suggest-right"></div>
            <div className="box-news-lastest-article">
              {newestPost && newestPost.posts && newestPost.posts.length > 0 &&
                <>
                  <a href={`/blog/${newestPost.posts[0].slug}`}>
                    <div className="hot-item-lastest">
                      <img className="hot-item-image" src={newestPost.posts[0].feature_image ? newestPost.posts[0].feature_image : "/images/image-post-default.png"} alt="image-lastest-article" width="100%" height="100%" />
                      <p className="hot-item-title" dangerouslySetInnerHTML={{__html: newestPost.posts[0].title}}></p>
                    </div>
                  </a>
                  <div className="divider-article"></div>
                </>
              }
              <div className="news-item-normal">
                {newestPost && newestPost.posts && newestPost.posts.length > 0 && newestPost.posts.slice(1, 5).map((item, index) => (
                  <React.Fragment key={index}>
                    <a href={`/blog/${item.slug}`}>
                      <div className="news-item-normal-detail">
                        <img className="item-detail-image" src={item.feature_image ? item.feature_image : "/images/image-post-default.png"} alt="image-lastest-article" width="100%" height="100%" />
                        <p className="item-detail-title" dangerouslySetInnerHTML={{__html: item.title}}></p>
                      </div>
                    </a>
                    <div className="divider-article"></div>
                  </React.Fragment>
                ))}
              </div>
            </div>
          </div>
        </div>
        <div className="box-category-custom">
            <div className="row" style={{
        width: "100%",
        margin: "0px"
      }}>
              <div className="col-12 box-divider-custom">
                <h2 className="title-block-update-interest">ĐÁNG CHÚ Ý</h2>
                <div className="divide-title-block"></div>
              </div>
            </div>
            <div className="block-article-first row" style={{
        width: "100%",
        margin: "0px"
      }}>
              {dataPostByCate && dataPostByCate.posts.length > 0 && dataPostByCate.posts.map((item, index) => {
                if(index !== 0) {
                  return (
                    <LazyLoad once={true} placeholder={"Loading..."} className="col-12 col-lg-4 article-first-item" key={index}>
                  <div className="article-first-item-box-image">
                    <a href={`/blog/${item.slug}`}>
                      <LazyLoad once={true} placeholder={"Loading..."}>
                        <Image
                          className="article-item-detail-image"
                          alt={item.title}
                          src={item.feature_image ? item.feature_image : "/images/finance-banner.png"}
                          layout="responsive"
                          width={420}
                          height={260}
                        />
                      </LazyLoad>
                    </a>
                  </div>
                  <div className="article-first-item-box-content">
                    <p className="article-item-detail-tag">
                      <span className="article-item-detail-tag-date">{item.publish_date}</span>
                      <span className="article-item-detail-tag-seprator"></span>
                      {item.category && item.category.length > 0 && item.category[0].slug && <a href={`/tag/${item.category[0].slug}`} className="article-item-detail-tag-name">{item.category[0].name}</a>}
                    </p>
                    <a href={`/blog/${item.slug}`}>
                      <p className="article-item-detail-text" dangerouslySetInnerHTML={{ __html: item.title }}></p>
                      <p className="article-item-detail-excerpt" dangerouslySetInnerHTML={{ __html: item.excerpt }}></p>
                    </a>
                  </div>
                </LazyLoad>
                  )
                } else {
                  return
                }
              })}
            </div>
          </div>
      </div>
    </div>
  );
}