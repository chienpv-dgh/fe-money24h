import React from 'react';

const data = {
  labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun"],
  datasets: [
    {
      label: "First dataset",
      data: [57150000, 57150000, 56900000, 57450000, 57250000],
      fill: false,
      borderColor: "rgba(75,192,192,1)"
    },
    {
      label: "Second dataset",
      data: [57900000, 57900000, 58200000, 57950000, 57970000],
      fill: false,
      borderColor: "#742774"
    }
  ]
};

function numberWithCommas(value) {
  return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

const options = {
  scales: {
    yAxes: [
      {
        ticks: {
          callback: (value) => {
            return `${numberWithCommas(value)}`
          }
        },
        scaleLabel: {
          display: true,
          fontColor: "#3B8BC6",
          fontSize: 14,
          labelString: "Site health"
        }
      }
    ]
  }
};

const MultiAxisLine = () => (
  <>
    <div className='header'>
      <h1 className='title'>Multi Axis Line Chart</h1>
      <div className='links'>
        <a
          className='btn btn-gh'
          href='https://github.com/reactchartjs/react-chartjs-2/blob/master/example/src/charts/MultiAxisLine.js'
        >
          Github Source
        </a>
      </div>
    </div>
  </>
);

export default MultiAxisLine;