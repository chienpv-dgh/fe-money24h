import React, { Component } from 'react';
import dynamic from "next/dynamic";
import BoxChooseTypeCheck from './box-choose-type-check';
import TableDataCurrencyByBank from './table-data-currency-by-bank';
import { ContentPageSaving } from '../SavingInterest';
import ArticleInterested from '../widgets/ArticleInterested/article-interested';

const GlobalToolBox = dynamic(() => import("../widgets/GlobalToolBox"));

interface IRecipeProps {
  postOfPage?: any;
  lastestPost?: any;
  listBankOps?: any;
  listMoneyOps?: any;
  currentBank?: any;
  dataTableCurrencyByBank?: any;
  stockCode?: string;
  detalNewsPost?: any;
  dataCrypto ?: any; 
}
interface IRecipeState {}

class CheckCurrencyByBank extends Component<IRecipeProps,IRecipeState> {
  render() {
    return (
      <div className="check-currency-page-container">
        <BoxChooseTypeCheck listBankOps={this.props.listBankOps} listMoneyOps={this.props.listMoneyOps} typeCheck={"bank"} nameBankCheck={this.props.currentBank[0].fullName}/>
        {/* <h1>12312321</h1> */}
        <TableDataCurrencyByBank nameBank={this.props.currentBank[0].bankName} dataTableCurrencyByBank={this.props.dataTableCurrencyByBank}/>
        <GlobalToolBox />
        <div className="divider-saving-interest-page"></div>
        {this.props.detalNewsPost.detail_post && <ContentPageSaving showAuthor={false} AllPosts={this.props.lastestPost} postOfPage={this.props.detalNewsPost.detail_post} stockCode={this.props.stockCode} dataCrypto={this.props.dataCrypto}/>}
        <div className="block-interested-article">
          <p className="interested-article-box-highlight">CÓ THỂ BẠN QUAN TÂM</p>
          <div className="divider-box-highlight"></div>
          <ArticleInterested AllPosts={this.props.lastestPost}/>
        </div>
      </div>
    );
  }
}

export default CheckCurrencyByBank;