import React, { Component } from 'react';
import { Table } from 'reactstrap';

interface IRecipeProps {
  dataTableCurrencyByMoney?: any;
  nameBank?: string;
}

interface IRecipeState {
  col?: number;
  sortType?: boolean;
  dataSort?: any;
}

class TableDataCurrencyByMoney extends Component<IRecipeProps,IRecipeState> {
  
  constructor(props) {
    super(props);
    this.state = {
      col: 1,
      sortType: true,
      dataSort: null
    }
  }
  

  checkRenderTimeDate = (date) => {
    let receiveDate = new Date(date)
    let year = receiveDate.getFullYear()
    let month = receiveDate.getMonth() + 1
    let day = receiveDate.getDate()
    let result = `${day}/${month}/${year}`

    return result
  }

  detectImage = (code) => {
    let image = ""
    switch (code) {
      case 1:
        image = "icon-increase-curentcy.svg"
        break;
      case -1:
        image = "icon-decrease-currentcy.svg"
        break;
      default:
        break;
    }
    return image
  }

  formatNumber = (number) => {
    let arrFormat = number.split(".")
    if(arrFormat.length > 0) {
      return arrFormat.join("").replace(",", ".")
    } else {
      return arrFormat.replace(",", ".")
    }

  }

  handleSort = async (type, col) => {
    let data = [...this.props.dataTableCurrencyByMoney.results]
    switch (col) {
      case 1:
        if(type == true) {
          let tempSort = [...data].sort((a, b) => Number(this.formatNumber(b.cash)) - Number(this.formatNumber(a.cash)))
          await this.setState({dataSort: tempSort})
        } else {
          let tempSort = [...data].sort((a, b) => Number(this.formatNumber(a.cash)) - Number(this.formatNumber(b.cash)))
          let tempArr = []
          let tempUnUse = []
          for(let i = 0; i < tempSort.length; i++) {
            if(tempSort[i].cash == "0,00") {
              tempUnUse.push(tempSort[i])
            } else {
              tempArr.push(tempSort[i])
            }
          }
          let totalArr = tempArr.concat(tempUnUse)
          await this.setState({dataSort: totalArr})
        }
      break;
      case 2:
        if(type == true) {
          let tempSort = [...data].sort((a, b) => Number(this.formatNumber(b.transfer)) - Number(this.formatNumber(a.transfer)))
          await this.setState({dataSort: tempSort})
        } else {
          let tempSort = [...data].sort((a, b) => Number(this.formatNumber(a.transfer)) - Number(this.formatNumber(b.transfer)))
          let tempArr = []
          let tempUnUse = []
          for(let i = 0; i < tempSort.length; i++) {
            if(tempSort[i].transfer == "0,00") {
              tempUnUse.push(tempSort[i])
            } else {
              tempArr.push(tempSort[i])
            }
          }
          let totalArr = tempArr.concat(tempUnUse)
          await this.setState({dataSort: totalArr})
        }
      break;
      case 3:
        if(type == true) {
          let tempSort = [...data].sort((a, b) => Number(this.formatNumber(b.sellRate)) - Number(this.formatNumber(a.sellRate)))
          await this.setState({dataSort: tempSort})
        } else {
          let tempSort = [...data].sort((a, b) => Number(this.formatNumber(a.sellRate)) - Number(this.formatNumber(b.sellRate)))
          let tempArr = []
          let tempUnUse = []
          for(let i = 0; i < tempSort.length; i++) {
            if(tempSort[i].sellRate == "0,00") {
              tempUnUse.push(tempSort[i])
            } else {
              tempArr.push(tempSort[i])
            }
          }
          let totalArr = tempArr.concat(tempUnUse)
          await this.setState({dataSort: totalArr})
        }
      break;
    }
  }

  componentDidMount() {
    this.handleSort(true, 1)
  }
  
  render() {
    
    return (
      <div className="table-data-currency-by-bank">
        <h1 className="table-data-title-by-bank">{`SO SÁNH TỈ GIÁ NGOẠI TỆ ${this.props.nameBank}`}</h1>
        <div className="description-table">
          <p>{`(Cập nhật mới nhất ngày ${this.checkRenderTimeDate(new Date())})`}</p>
        </div>
        <div className="custom-table-currentcy">
          <Table responsive>
            <thead>
              <tr>
                <th rowSpan={2}>Ngân hàng</th>
                <th rowSpan={2}>Mã ngân hàng</th>
                <th colSpan={2}>Tỷ giá mua</th>
                <th style={{cursor: "pointer"}} rowSpan={2} onClick={() => this.setState({sortType: !this.state.sortType, col: 3}, () => this.handleSort(this.state.sortType, this.state.col))}>
                  <div className="sort-thead">
                    <span>Tỷ giá bán</span>
                    <div className="sort-box-icon">
                      <i className="fa fa-caret-up" aria-hidden="true" style={{color: this.state.col == 3 && this.state.sortType ? "#FFFFFF" : "rgba(255, 255, 255, 0.2)"}}></i>
                      <i className="fa fa-caret-down" aria-hidden="true" style={{color: this.state.col == 3 && !this.state.sortType ? "#FFFFFF" : "rgba(255, 255, 255, 0.2)"}}></i>
                    </div>
                  </div>
                </th>
              </tr>
              <tr>
                <th style={{cursor: "pointer"}} onClick={() => this.setState({sortType: !this.state.sortType, col: 1}, () => this.handleSort(this.state.sortType, this.state.col))}>
                  <div className="sort-thead">
                    <span>Tiền mặt</span>
                    <div className="sort-box-icon">
                      <i className="fa fa-caret-up" aria-hidden="true" style={{color: this.state.col == 1 && this.state.sortType ? "#FFFFFF" : "rgba(255, 255, 255, 0.2)"}}></i>
                      <i className="fa fa-caret-down" aria-hidden="true" style={{color: this.state.col == 1 && !this.state.sortType ? "#FFFFFF" : "rgba(255, 255, 255, 0.2)"}}></i>
                    </div>
                  </div>
                </th>
                <th style={{cursor: "pointer"}} onClick={() => this.setState({sortType: !this.state.sortType, col: 2}, () => this.handleSort(this.state.sortType, this.state.col))}>
                  <div className="sort-thead">
                    <span>Chuyển khoản</span>
                    <div className="sort-box-icon">
                      <i className="fa fa-caret-up" aria-hidden="true" style={{color: this.state.col == 2 && this.state.sortType ? "#FFFFFF" : "rgba(255, 255, 255, 0.2)"}}></i>
                      <i className="fa fa-caret-down" aria-hidden="true" style={{color: this.state.col == 2 && !this.state.sortType ? "#FFFFFF" : "rgba(255, 255, 255, 0.2)"}}></i>
                    </div>
                  </div>
                </th>
              </tr>
            </thead>
            <tbody>
              {this.state.dataSort && this.state.dataSort.length > 0 && this.state.dataSort.map((item, index) => (
                  <tr key={index}>
                    <td><a href={`/ty-gia-ngoai-te-${item.slug}`}>{item.bankName}</a></td>
                    <td>{item.bankCode || "-"}</td>
                    <td>
                      <div className="box-status-currentcy">
                        <span className="status-currentcy-text">{item.cash}</span>
                        {item.flagCash ? <img className="status-currentcy-image" src={`/images/${this.detectImage(item.flagCash)}`} alt="status-currentcy"/> : null}
                      </div>
                    </td>
                    <td>
                      <div className="box-status-currentcy">
                        <span className="status-currentcy-text">{item.transfer}</span>
                        {item.flagTrans ? <img className="status-currentcy-image" src={`/images/${this.detectImage(item.flagTrans)}`} alt="status-currentcy"/> : null}
                      </div>
                    </td>
                    <td>
                      <div className="box-status-currentcy">
                        <span className="status-currentcy-text">{item.sellRate}</span>
                        {item.flagSell ? <img className="status-currentcy-image" src={`/images/${this.detectImage(item.flagSell)}`} alt="status-currentcy"/> : null}
                      </div>
                    </td>
                  </tr>
                ))
              }
            </tbody>
          </Table>
        </div>
      </div>
    );
  }
}

export default TableDataCurrencyByMoney;