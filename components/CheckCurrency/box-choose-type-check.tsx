import React, { Component } from 'react';
import { ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';

interface IRecipeProps {
  listBankOps?: any;
  listMoneyOps?: any;
  typeCheck?: string;
  nameBankCheck?: string;
  nameMoneyCheck?: string;
}

interface IRecipeState {
  type?: string;
  dropdownOpen?: boolean;
  nameBank?: string;
  idBank?: number;
  nameMoney?: string;
  idMoney?: number;
  searchName?: string;
  arrResult?: any;
}

class BoxChooseTypeCheck extends Component<IRecipeProps,IRecipeState> {

  constructor(props) {
    super(props);
    this.state = {
      type: this.props.typeCheck ? this.props.typeCheck : "bank",
      dropdownOpen: false,
      nameBank: this.props.nameBankCheck ? this.props.nameBankCheck : "",
      idBank: null,
      nameMoney: this.props.nameMoneyCheck ? this.props.nameMoneyCheck : "",
      idMoney: null,
      searchName: "",
      arrResult: []
    }
  }
  

  onRadioChange = (e) => {
    this.setState({
      type: e.target.value,
    });
  }

  searchBank = (value) => {
    let arrResult = []
    this.props.listBankOps.map((item) => {
      if(item.searchName) {
        let arrSearch = item.searchName
        if (arrSearch.toLowerCase().search(value.toLowerCase()) !== -1) {
          arrResult.push(item)
        }
      }
    })
    this.setState({ arrResult: arrResult})
  }

  render() {
    
    return (
      <div className="block-choose-type-check">
        <p className="bread-scrum">
          <a href="/">Trang chủ</a>
          <i className="fa fa-angle-right bread-scrum-divider" aria-hidden="true"></i>
          <a href={`/tag/${"ngan-hang"}`}>{"Ngân hàng"}</a>
          <i className="fa fa-angle-right bread-scrum-divider post" aria-hidden="true"></i>
          {this.props.typeCheck && <a className="tag-link" href={"/ty-gia-ngoai-te-hoi-doai-ngan-hang-hom-nay"}>{"Tra cứu tỷ giá ngoại tệ"}</a>}
          {this.props.typeCheck && <i className="fa fa-angle-right bread-scrum-divider post" aria-hidden="true"></i>}
          {this.props.typeCheck ? <span>{this.props.typeCheck == "bank" ? `Tỷ giá ngoại tệ ${this.props.nameBankCheck}` : `So sánh tỷ giá ngoại tệ ${this.props.nameMoneyCheck}`}</span> : <span>{"Tra cứu tỷ giá ngoại tệ"}</span>}
        </p>
        <div className="contain-box-choose-type">
          {this.props.typeCheck ? <p className="box-choose-type-main-title">TỶ GIÁ NGOẠI TỆ HÔM NAY</p> : <h1 className="box-choose-type-main-title">TỶ GIÁ NGOẠI TỆ HÔM NAY</h1>}
          <div className="check-radio-value row">
            <div className="col-12 col-lg-6">
              <div className="check-radio-value-item">
                <input value="bank" type="radio" id="bank" name="bank" checked={this.state.type == "bank"}
                  onChange={this.onRadioChange}/>
                <label htmlFor="bank" className="label-radio-custom">Ngân hàng</label>
              </div>
              <p className="check-radio-value-item-description">(Tra cứu tỷ giá theo ngân hàng)</p>
            </div>
            <div className="col-12 col-lg-6">
              <div className="check-radio-value-item">
                <input value="money" type="radio" id="money" name="money" checked={this.state.type == "money"}
                  onChange={this.onRadioChange}/>
                <label htmlFor="money" className="label-radio-custom">Ngoại tệ</label>
              </div>
              <p className="check-radio-value-item-description">(Tra cứu giá mua - bán cao nhất theo ngoại tệ)</p>
            </div>
          </div>
          {this.state.type == "bank" ? <div className="box-choose-type">
            <p className="choose-bank-label">Ngân hàng *</p>
            <ButtonDropdown className="custom-select-box" isOpen={this.state.dropdownOpen} toggle={() => this.setState({dropdownOpen: !this.state.dropdownOpen})}>
              <DropdownToggle className="custom-select">
                {this.state.nameBank ? this.state.nameBank : "Chọn ngân hàng"}
              </DropdownToggle>
              <DropdownMenu className="custom-select-menu-box customScroll">  
                <div className="box-search-item">
                  <img className="search-image" src="/images/search-icon.svg" alt="search-icon"/>
                  <input type="text" placeholder="Nhập tên, mã ngân hàng" className="custom-search-box" onChange={(e) => this.setState({searchName: e.target.value}, () => this.searchBank(this.state.searchName))}/>
                </div>
                {this.props.listBankOps ?
                  this.state.arrResult.length > 0 ? this.state.arrResult.map((item, index) => (<DropdownItem onClick={() => this.setState({nameBank: item.fullName, idBank: item.id })} key={index} className="menu-box-item"><a href={`/ty-gia-ngoai-te-${item.slug}`} className="item-tag-link">{item.fullName}</a></DropdownItem>)) : this.props.listBankOps.map((item, index) => (<DropdownItem onClick={() => this.setState({nameBank: item.fullName, idBank: item.id })} key={index} className="menu-box-item"><a href={`/ty-gia-ngoai-te-${item.slug}`} className="item-tag-link">{item.fullName}</a></DropdownItem>))
                : 
                  <DropdownItem className="menu-box-item">Oops! Data not found.</DropdownItem>
                }
              </DropdownMenu>
              <img className="custom-select-image" src="/images/icon-custom-select.svg" alt="icon-custom-select"/>
            </ButtonDropdown>
          </div> : <div className="box-choose-type">
            <p className="choose-bank-label">Tiền tệ *</p>
            <ButtonDropdown className="custom-select-box" isOpen={this.state.dropdownOpen} toggle={() => this.setState({dropdownOpen: !this.state.dropdownOpen})}>
              <DropdownToggle className="custom-select">
                {this.state.nameMoney ? this.state.nameMoney : "Chọn tiền tệ"}
              </DropdownToggle>
              <DropdownMenu className="custom-select-menu-box customScroll">
                {this.props.listMoneyOps ?
                  this.props.listMoneyOps.map((item, index) => (<DropdownItem onClick={() => this.setState({nameMoney: `${item.moneyCode} - ${item.moneyName}`, idMoney: item.id })} key={index} className="menu-box-item"><a href={`/ty-gia-${item.fullSlug}`} className="item-tag-link">{`${item.moneyCode} - ${item.moneyName}`}</a></DropdownItem>))
                : 
                  <DropdownItem className="menu-box-item">Oops! Data not found.</DropdownItem>
                }
              </DropdownMenu>
              <img className="custom-select-image" src="/images/icon-custom-select.svg" alt="icon-custom-select"/>
            </ButtonDropdown>
          </div>}
        </div>
      </div>
    );
  }
}

export default BoxChooseTypeCheck;