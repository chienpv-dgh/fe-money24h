import React, { Component } from 'react';
import { Table } from 'reactstrap';

interface IRecipeProps {
  nameBank?: string;
  dataTableCurrencyByBank?: any;
}

interface IRecipeState {}

class TableDataCurrencyByBank extends Component<IRecipeProps,IRecipeState> {
  
  checkRenderTimeDate = (date) => {
    let receiveDate = new Date(date)
    let year = receiveDate.getFullYear()
    let month = receiveDate.getMonth() + 1
    let day = receiveDate.getDate()
    let result = `${day}/${month}/${year}`

    return result
  }

  detectImage = (code) => {
    let image = ""
    switch (code) {
      case 1:
        image = "icon-increase-curentcy.svg"
        break;
      case -1:
        image = "icon-decrease-currentcy.svg"
        break;
      default:
        break;
    }
    return image
  }
  
  render() {

    return (
      <div className="table-data-currency-by-bank">
        <h1 className="table-data-title-by-bank">{`TỈ GIÁ NGOẠI TỆ ${this.props.nameBank}`}</h1>
        <div className="description-table">
          <p>{`(Cập nhật mới nhất ngày ${this.checkRenderTimeDate(new Date())})`}</p>
        </div>
        <div className="custom-table-currentcy">
          <Table responsive>
            <thead>
              <tr>
                <th rowSpan={2}>Ngoại tệ</th>
                <th rowSpan={2}>Mã ngoại tệ</th>
                <th colSpan={2}>Tỷ giá mua</th>
                <th rowSpan={2}>Tỷ giá bán</th>
              </tr>
              <tr>
                <th>Tiền mặt</th>
                <th>Chuyển khoản</th>
              </tr>
            </thead>
            <tbody>
              {this.props.dataTableCurrencyByBank ?
                this.props.dataTableCurrencyByBank.results && this.props.dataTableCurrencyByBank.results.length > 0 && this.props.dataTableCurrencyByBank.results.map((item, index) => (
                  <tr key={index}>
                    <td><a href={`/ty-gia-${item.fullSlug}`}>{item.moneyName}</a></td>
                    <td>{item.moneyCode}</td>
                    <td>
                      <div className="box-status-currentcy">
                        <span className="status-currentcy-text">{item.cash}</span>
                        {item.flagCash ? <img className="status-currentcy-image" src={`/images/${this.detectImage(item.flagCash)}`} alt="status-currentcy"/> : null}
                      </div>
                    </td>
                    <td>
                      <div className="box-status-currentcy">
                        <span className="status-currentcy-text">{item.transfer}</span>
                        {item.flagTrans ? <img className="status-currentcy-image" src={`/images/${this.detectImage(item.flagTrans)}`} alt="status-currentcy"/> : null}
                      </div>
                    </td>
                    <td>
                      <div className="box-status-currentcy">
                        <span className="status-currentcy-text">{item.sellRate}</span>
                        {item.flagSell ? <img className="status-currentcy-image" src={`/images/${this.detectImage(item.flagSell)}`} alt="status-currentcy"/> : null}
                      </div>
                    </td>
                  </tr>
                ))
                :
                null
              }
            </tbody>
          </Table>
        </div>
      </div>
    );
  }
}

export default TableDataCurrencyByBank;