import React, { Component } from 'react';
import dynamic from "next/dynamic";
import BoxChooseTypeCheck from './box-choose-type-check';
import TableDataCurrencyByMoney from './table-data-currency-by-money';
import { ContentPageSaving } from '../SavingInterest';
import ArticleInterested from '../widgets/ArticleInterested/article-interested';

const GlobalToolBox = dynamic(() => import("../widgets/GlobalToolBox"));

interface IRecipeProps {
  lastestPost?: any;
  listBankOps?: any;
  listMoneyOps?: any;
  postOfPage?: any;
  dataTableCurrencyByMoney?: any;
  currentCurrency?: any;
  detalNewsPost?: any;
  dataCrypto ?:any
}
interface IRecipeState {}

class CheckCurrencyByMoney extends Component<IRecipeProps,IRecipeState> {
  render() {
    return (
      <div className="check-currency-page-container">
        <BoxChooseTypeCheck listBankOps={this.props.listBankOps} listMoneyOps={this.props.listMoneyOps} typeCheck={"money"} nameMoneyCheck={`${this.props.currentCurrency[0].moneyCode} - ${this.props.currentCurrency[0].moneyName}`}/>
        <TableDataCurrencyByMoney nameBank={`${this.props.currentCurrency[0].moneyCode} - ${this.props.currentCurrency[0].moneyName}`} dataTableCurrencyByMoney={this.props.dataTableCurrencyByMoney} />
        <GlobalToolBox />
        <div className="divider-saving-interest-page"></div>
        {this.props.detalNewsPost.detail_post && <ContentPageSaving showAuthor={false} AllPosts={this.props.lastestPost} postOfPage={this.props.detalNewsPost.detail_post} dataCrypto={this.props.dataCrypto}/>}
        <div className="block-interested-article">
          <p className="interested-article-box-highlight">CÓ THỂ BẠN QUAN TÂM</p>
          <div className="divider-box-highlight"></div>
          <ArticleInterested AllPosts={this.props.lastestPost}/>
        </div>
      </div>
    );
  }
}

export default CheckCurrencyByMoney;