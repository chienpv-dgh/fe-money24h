import React, { useEffect, useState } from 'react';
import { Table, Spinner } from 'reactstrap';
export interface CurrencyTableProps {
  currencyDataEng: {
    status: {
      timestamp: string;
      total_count: number;
    };
    data: {
      circulating_supply: number;
      cmc_rank: number;
      id: number;
      last_updated: string;
      max_supply: number;
      total_supply: number;
      name: string;
      num_market_pairs: number;
      slug: string;
      symbol: string;
      quote: {
        fully_diluted_market_cap: number;
        last_updated: string;
        market_cap: number;
        market_cap_dominance: number;
        percent_change_1h: number;
        percent_change_7d: number;
        percent_change_24h: number;
        percent_change_30d: number;
        percent_change_60d: number;
        percent_change_90d: number;
        price: number;
        volume_24h: number;
        volume_change_24h: number;
      }[];
    }[];
  };
  currencyDataVi: {
    status: {
      timestamp: string;
      total_count: number;
    };
    data: {
      circulating_supply: number;
      cmc_rank: number;
      id: number;
      last_updated: string;
      max_supply: number;
      total_supply: number;
      name: string;
      num_market_pairs: number;
      slug: string;
      symbol: string;
      quote: {
        fully_diluted_market_cap: number;
        last_updated: string;
        market_cap: number;
        market_cap_dominance: number;
        percent_change_1h: number;
        percent_change_7d: number;
        percent_change_24h: number;
        percent_change_30d: number;
        percent_change_60d: number;
        percent_change_90d: number;
        price: number;
        volume_24h: number;
        volume_change_24h: number;
      }[];
    }[];
  }
}

export interface DataCurrencyProps {
  status: {
    timestamp: string;
    total_count: number;
  };
  data: {
    circulating_supply: number;
    cmc_rank: number;
    id: number;
    last_updated: string;
    max_supply: number;
    total_supply: number;
    name: string;
    num_market_pairs: number;
    slug: string;
    symbol: string;
    quote: {
      fully_diluted_market_cap: number;
      last_updated: string;
      market_cap: number;
      market_cap_dominance: number;
      percent_change_1h: number;
      percent_change_7d: number;
      percent_change_24h: number;
      percent_change_30d: number;
      percent_change_60d: number;
      percent_change_90d: number;
      price: number;
      volume_24h: number;
    }[];
  }[];
}

export default function CryptoCurrencyTableHomePage({ 
  currencyDataEng,
  currencyDataVi
}: CurrencyTableProps) {

  const [dataCurrencyDefault, setDataCurrencyDefault] = useState<DataCurrencyProps>(currencyDataEng)
  const [currentLang, setCurrentLang] = useState('USD')

  const renderColorPercent = (percent) => {
    if(Number(percent)) {
      if (Number(percent) > 0) {
        return "text-increase"
      } else {
        return "text-decrease"
      }
    } else {
      return ""
    }
  }

  const formatNumberWithComma = (x) => {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }

  useEffect(() => {
    
    switch (currentLang) {
      case 'USD':
        setDataCurrencyDefault(currencyDataEng)
        break;
      case 'VND':
        setDataCurrencyDefault(currencyDataVi)
        break;
      default:
        setDataCurrencyDefault(currencyDataEng)
        break;
    }
  }, [currencyDataEng, currencyDataVi, currentLang])

  const changeUnitLang = (unit) => {
    setCurrentLang(unit)
  }

  const getCurrentTime = () => {
    let currentDay = new Date()

    let hour = currentDay.getHours() < 10 ? `0${currentDay.getHours()}` : currentDay.getHours()
    let minute = currentDay.getMinutes() < 10 ? `0${currentDay.getMinutes()}` : currentDay.getMinutes()
    let second = currentDay.getSeconds() < 10 ? `0${currentDay.getSeconds()}` : currentDay.getSeconds()

    let day = currentDay.getDate() < 10 ? `0${currentDay.getDate()}` : currentDay.getDate()
    let month = currentDay.getMonth() + 1 < 10 ? `0${currentDay.getMonth() + 1}` : currentDay.getMonth() + 1
    let year = currentDay.getFullYear()

    return `${hour}:${minute}:${second} ${day}/${month}/${year}`
  }

  const renderUnitCurrency = (unitLang) => {
    if(unitLang == 'USD' ) {
      return '$'
    } else {
      return 'VNĐ'
    }
  }

  const renderNameUnitCurrency = (unitLang) => {
    if(unitLang == 'USD' ) {
      return 'USD'
    } else {
      return 'VNĐ'
    }
  }
          
  return (
    <div className='table-currency-container'>
      <p className='currency-homepage-title'>BẢNG GIÁ TIỀN ẢO REAL TIME</p>
      <div className='box-update-time'>
        <small>{`Cập nhật lúc ${getCurrentTime()}`}</small>
        <div className='box-choose-unit'>
          <span className='unit-currency'>Đơn vị:</span>
          <div className='unit-container'>
            <span className={`unit-item ${currentLang == 'USD' ? 'active' : ''}`} onClick={() => changeUnitLang('USD')}>
              <img width={16} height={11} src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAMAAABBPP0LAAAAt1BMVEWSmb66z+18msdig8La3u+tYX9IaLc7W7BagbmcUW+kqMr/q6n+//+hsNv/lIr/jIGMnNLJyOP9/fyQttT/wb3/////aWn+YWF5kNT0oqz0i4ueqtIZNJjhvt/8gn//WVr/6+rN1+o9RKZwgcMPJpX/VFT9UEn+RUX8Ozv2Ly+FGzdYZrfU1e/8LS/lQkG/mbVUX60AE231hHtcdMb0mp3qYFTFwNu3w9prcqSURGNDaaIUMX5FNW5wYt7AAAAAjklEQVR4AR3HNUJEMQCGwf+L8RR36ajR+1+CEuvRdd8kK9MNAiRQNgJmVDAt1yM6kSzYVJUsPNssAk5N7ZFKjVNFAY4co6TAOI+kyQm+LFUEBEKKzuWUNB7rSH/rSnvOulOGk+QlXTBqMIrfYX4tSe2nP3iRa/KNK7uTmWJ5a9+erZ3d+18od4ytiZdvZyuKWy8o3UpTVAAAAABJRU5ErkJggg==" alt="USD" />
              USD
            </span>
            <span className={`unit-item ${currentLang == 'VND' ? 'active' : ''}`} onClick={() => changeUnitLang('VND')}>
              <img width={16} height={11} src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAMAAABBPP0LAAAATlBMVEX+AAD2AADvAQH/eXn+cXL9amr8YmL9Wlr8UlL7TkvoAAD8d0f6Pz/3ODf2Ly/0KSf6R0f6wTv60T31IBz6+jr4+Cv3QybzEhL4bizhAADgATv8AAAAW0lEQVR4AQXBgU3DQBRAMb+7jwKVUPefkQEQTYJqByBENpKUGoZslXoN5LPONH8G9WWZ7pGlOn6XZmaGRce1J/seei4dl+7dPWDqkk7+58e3+igdlySPcYbwBG+lPhCjrtt9EgAAAABJRU5ErkJggg==" alt="VND" />
              VND
            </span>
          </div>
        </div>
      </div>
      <Table responsive className='currency-table-data'>
        <thead>
          <tr>
            <th>#</th>
            <th>Tiền ảo</th>
            <th>{`Giá (${renderNameUnitCurrency(currentLang)})`}</th>
            <th>%(24h)</th>
            <th>%(7d)</th>
            <th>Vốn hóa</th>
            <th>Giao dịch (24h)</th>
          </tr>
        </thead>
        <tbody>
          {
            dataCurrencyDefault ? dataCurrencyDefault.data && dataCurrencyDefault.data.length > 0 && dataCurrencyDefault.data.map((currencyItem, index) => (
              <tr key={index}>
                <td>{index + 1}</td>
                <td>
                  <img src={`https://coinmarketcap.vn/assets/images/coins/16x16/${currencyItem.id}.png`} alt={currencyItem.name} className='icon-currency'/>
                  <a href={`/tien-ao/${currencyItem.symbol}`} title={`Giá ${currencyItem.name}`} className='currency-link'>
                    <span>{currencyItem.name}</span>
                    {" "}
                    <small>{currencyItem.symbol}</small>
                  </a>
                </td>
                <td>{`${formatNumberWithComma(Math.round(currencyItem.quote[0].price * 100) / 100)}`}</td>
                <td>
                  <span className={`text-percent ${renderColorPercent(Math.round(currencyItem.quote[0].percent_change_24h * 100) / 100)}`}>{`${Math.round(currencyItem.quote[0].percent_change_24h * 100) / 100}%`}</span>
                </td>
                <td>
                  <span className={`text-percent ${renderColorPercent(Math.round(currencyItem.quote[0].percent_change_7d * 100) / 100)}`}>{`${Math.round(currencyItem.quote[0].percent_change_7d * 100) / 100}%`}</span>
                </td>
                <td>{`${formatNumberWithComma(Math.round(currencyItem.quote[0].market_cap * 100) / 100)} ${renderUnitCurrency(currentLang)}`}</td>
                <td>{`${formatNumberWithComma(Math.round(currencyItem.quote[0].volume_24h * 100) / 100)} ${renderUnitCurrency(currentLang)}`}</td>
              </tr>
            ))
            :
            <tr>
              <td className='empty-data' colSpan={7}>
                <div className="spinner">
                  <Spinner color="primary" />
                </div>
              </td>
            </tr>
          }
        </tbody>
      </Table>
      <div className='see-more-crypto-currency-container'>
        <a href="/tien-ao" className='see-more-crypto-currency'>Xem toàn bộ tiền ảo</a>
      </div>
    </div>
  )
}