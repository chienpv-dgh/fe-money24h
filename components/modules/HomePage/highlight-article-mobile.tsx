import React from 'react';
import Image from 'next/image';
import SwiperCore, { Navigation, Pagination } from 'swiper/core';
import { Swiper, SwiperSlide } from 'swiper/react';

SwiperCore.use([Navigation, Pagination]);

export interface HighlightArticleMobileProps {
  dataNewest?: {
    author: {
      id: string;
      name: string;
      slug: string;
    };
    category?: {
      name: string;
      slug: string;
    }[];
    excerpt?: string;
    feature_image?: string;
    publish_date?: string;
    slug: string;
    tag: {
      name: string;
      slug: string;
    }[];
    title?: string;
  }[];
}

export default function HighlightArticleMobileModule({ dataNewest }: HighlightArticleMobileProps) {

  return (
    <div className="article-swiper-mobile">
      <Swiper
        className="article-swiper-mobile-contain"
        slidesPerView={1}
        allowTouchMove={true}
        pagination={{ clickable: true }}
        lazy={true}
      >
        {
          dataNewest.slice(0, 3).map((item, index) => (
            <SwiperSlide key={index} className='article-mobile-swiper-item'>
              <div className="block-article-full-item">
                <a href={`/blog/${item.slug}`}>
                  <Image
                    priority={index == 0 ? true : false}
                    loading={index == 0 ? "eager" : "lazy"}
                    className="article-full-item-image-small"
                    alt={item.title}
                    src={item.feature_image ? item.feature_image : "/images/finance-banner.png"}
                    layout="responsive"
                    width={374}
                    height={240}
                  />
                  <div className="background-front-article"></div>
                </a>
                <div className="article-full-item-detail">
                  {item.category && item.category.length > 0 && item.category[0].slug && <a href={`/tag/${item.category[0].slug}`}><p className="article-full-detail-category">{item.category[0].name}</p></a>}
                  <a href={`/blog/${item.slug}`}>
                    <p className="article-full-detail-description" dangerouslySetInnerHTML={{ __html: item.title }}></p>
                    <p className="article-full-detail-time-create">{item.publish_date}</p>
                  </a>
                </div>
              </div>
            </SwiperSlide>
          ))
        }
      </Swiper>
    </div>
  )
}