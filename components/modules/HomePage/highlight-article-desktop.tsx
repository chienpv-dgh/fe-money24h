import React from 'react';
import Image from 'next/image';

export interface HighlightArticleDesktopProps {
  dataNewest?: {
    author: {
      id: string;
      name: string;
      slug: string;
    };
    category?: {
      name: string;
      slug: string;
    }[];
    excerpt?: string;
    feature_image?: string;
    publish_date?: string;
    slug: string;
    tag: {
      name: string;
      slug: string;
    }[];
    title?: string;
  }[];
}

export default function HighlightArticleDesktopModule({ dataNewest }: HighlightArticleDesktopProps) {

  return (
    <>
      <div className="col-12 col-lg-8 article-full">
        {
          dataNewest && <div className="block-article-full-item">
            <a href={`/blog/${dataNewest[0].slug}`} className="image-optimize-tag-link">
              <Image
                priority={true}
                className="image-optimize"
                alt={dataNewest[0].title}
                src={dataNewest[0].feature_image ? dataNewest[0].feature_image : "/images/finance-banner.png"}
                layout="responsive"
                width={824}
                height={472}
              />
              <div className="background-front-article"></div>
            </a>
            <div className="article-full-item-detail">
              {dataNewest[0].category && dataNewest[0].category.length > 0 && dataNewest[0].category[0].slug && <a href={`/tag/${dataNewest[0].category[0].slug}`}><p className="article-full-detail-category">{dataNewest[0].category[0].name}</p></a>}
              <a href={`/blog/${dataNewest[0].slug}`}>
                <p className="article-full-detail-description" dangerouslySetInnerHTML={{ __html: dataNewest[0].title }}></p>
                <p className="article-full-detail-time-create">{dataNewest[0].publish_date}</p>
              </a>
            </div>
          </div>
        }
      </div>

      <div className="col-12 col-lg-4 row article-half">
        {
          dataNewest && dataNewest.slice(1, 3).map((item, index) => (
            <div className="col-6 col-lg-12 block-article-half-item" key={index}>
              <a href={`/blog/${item.slug}`}>
                <Image
                  priority={true}
                  className="article-half-item-image"
                  alt={item.title}
                  src={item.feature_image ? item.feature_image : "/images/finance-banner.png"}
                  layout="responsive"
                  width={390}
                  height={228}
                />
                <div className="background-front-article"></div>
              </a>
              <div className="article-half-item-detail">
                {item.category && item.category.length > 0 && item.category[0].slug && <a href={`/tag/${item.category[0].slug}`}><p className="article-half-detail-category">{item.category[0].name}</p></a>}
                <a href={`/blog/${item.slug}`}>
                  <p className="article-half-detail-description" dangerouslySetInnerHTML={{ __html: item.title }}></p>
                  <p className="article-half-detail-time-create">{item.publish_date}</p>
                </a>
              </div>
            </div>
          ))
        }
      </div>
    </>
  )
}