import React from 'react';
import dynamic from "next/dynamic";
import LazyLoad from 'react-lazyload';
import Image from 'next/image';

// import formatDistanceToNowStrict from 'date-fns/formatDistanceToNowStrict';

const CryptoCurrencyTableHomePage = dynamic(() => import("./table-currency-home-page"));
const HighlightArticleDesktopModule = dynamic(() => import("./highlight-article-desktop"));
const HighlightArticleMobileModule = dynamic(() => import("./highlight-article-mobile"));
const VietlotData = dynamic(() => import("../../widgets/DataVietlot"));
const GlobalToolBox = dynamic(() => import("../../widgets/GlobalToolBox"));
const WidgetDataByBank = dynamic(() => import("../../widgets/DataByBank"));
const ExchangeRate = dynamic(() => import("../../widgets/DataExchange"));
const WidgetBorowData = dynamic(() => import("../../widgets/DataBorrow"));
const WidgetsDataCrypto = dynamic( ()=>import("../../widgets/DataCryptoCurrency"));

export interface quoteObject{
  fully_diluted_market_cap: number;
  last_updated: string;
  market_cap: number;
  market_cap_dominance: number;
  percent_change_1h: number;
  percent_change_7d: number;
  percent_change_24h: number;
  percent_change_30d: number;
  percent_change_60d: number;
  percent_change_90d: number;
  price: number;
  volume_24h: number;
  volume_change_24h: number;
}
export interface HomePageModuleProps {
  dataInterest: {
    offline: {
      bankName: string;
      months1?: string;
      months3?: string;
      months6?: string;
      months9?: string;
      months12?: string;
      slug: string;
      type: number;
    }[];
    online: {
      bankName: string;
      months1?: string;
      months3?: string;
      months6?: string;
      months9?: string;
      months12?: string;
      slug: string;
      type: number;
    }[];
  };
  tableVietlot: {
    description: string;
    name: string;
    period: string;
    reward: number;
  }[];
  dataBorrow: {
    bankName: string;
    detail: string;
    idPost: string;
    months: string;
    results: {
      result?: string;
    };
    share?: string;
    slug: string;
  }[];
  dataExchange: {
    bankName: string;
    idPost: string;
    results: {
      cash: string;
      flagCash?: number;
      flagSell?: number;
      flagTrans?: number;
      fullSlug: string;
      moneyCode: string;
      moneyName: string;
      sellRate: string;
      share?: string;
      transfer: string;
    }[];
    share?: string;
    slug: string;
  };
  dataCategory?: {
    cat_name: string;
    cat_slug: string;
    list_posts: {
      author: {
        id: string;
        name: string;
        slug: string;
      };
      category?: {
        name: string;
        slug: string;
      }[];
      excerpt?: string;
      feature_image?: string;
      publish_date?: string;
      slug: string;
      tag: {
        name: string;
        slug: string;
      }[];
      title?: string;
    }[];
  }[];
  dataNewest?: {
    author: {
      id: string;
      name: string;
      slug: string;
    };
    category?: {
      name: string;
      slug: string;
    }[];
    excerpt?: string;
    feature_image?: string;
    publish_date?: string;
    slug: string;
    tag: {
      name: string;
      slug: string;
    }[];
    title?: string;
  }[];
  currencyDataEng: {
    status: {
      timestamp: string;
      total_count: number;
    };
    data: {
      circulating_supply: number;
      cmc_rank: number;
      id: number;
      last_updated: string;
      max_supply: number;
      total_supply: number;
      name: string;
      num_market_pairs: number;
      slug: string;
      symbol: string;
      quote: {
        fully_diluted_market_cap: number;
        last_updated: string;
        market_cap: number;
        market_cap_dominance: number;
        percent_change_1h: number;
        percent_change_7d: number;
        percent_change_24h: number;
        percent_change_30d: number;
        percent_change_60d: number;
        percent_change_90d: number;
        price: number;
        volume_24h: number;
        volume_change_24h: number;
      }[];
    }[];
  };
  currencyDataVi: {
    status: {
      timestamp: string;
      total_count: number;
    };
    data: {
      circulating_supply: number;
      cmc_rank: number;
      id: number;
      last_updated: string;
      max_supply: number;
      total_supply: number;
      name: string;
      num_market_pairs: number;
      slug: string;
      symbol: string;
      quote: {
        fully_diluted_market_cap: number;
        last_updated: string;
        market_cap: number;
        market_cap_dominance: number;
        percent_change_1h: number;
        percent_change_7d: number;
        percent_change_24h: number;
        percent_change_30d: number;
        percent_change_60d: number;
        percent_change_90d: number;
        price: number;
        volume_24h: number;
        volume_change_24h: number;
      }[];
    }[];
  };
  dataCrypto: {
    status: {
      timestamp: string;
      total_count: number;
    };
    data: {
      circulating_supply: number;
      cmc_rank: number;
      id: number;
      last_updated: string;
      max_supply: number;
      total_supply: number;
      name: string;
      num_market_pairs: number;
      slug: string;
      symbol: string;
      quote: quoteObject[];
    }[];
  };
}

export default function HomePageModule({
  dataInterest,
  tableVietlot,
  dataBorrow,
  dataExchange,
  dataCategory,
  dataNewest,
  currencyDataEng,
  currencyDataVi,
  dataCrypto
}: HomePageModuleProps) {

  // const checkRenderTimeDateTextMobile = (date) => {
  //   let getFromNow = formatDistanceToNowStrict(new Date(date))
  //   let timeVal = getFromNow.split(" ")[0]
  //   let prefix = getFromNow.split(" ")[1]
  //   let result = ""

  //   switch (prefix) {
  //     case "minute":
  //       result = `${timeVal}p trước`
  //       break;
  //     case "minutes":
  //       result = `${timeVal}p trước`
  //       break;
  //     case "hour":
  //       result = `${timeVal}h trước`
  //       break;
  //     case "hours":
  //       result = `${timeVal}h trước`
  //       break;
  //     case "day":
  //       result = `${timeVal} ngày trước`
  //       break;
  //     case "days":
  //       result = `${timeVal} ngày trước`
  //       break;
  //     case "month":
  //       result = `${timeVal} tháng trước`
  //       break;
  //     case "months":
  //       result = `${timeVal} tháng trước`
  //       break;
  //     default:
  //       result = "Tin mới nhất"
  //       break;
  //   }

  //   return result
  // }

  return (
    <>
      <div className="crypto-homepage">
        <CryptoCurrencyTableHomePage 
          currencyDataEng={currencyDataEng}
          currencyDataVi={currencyDataVi}
        />
      </div>

      <div className="row block-article-homepage">
        <HighlightArticleDesktopModule dataNewest={dataNewest}/>
      </div>

      {
        dataNewest && <HighlightArticleMobileModule dataNewest={dataNewest}/>
      }

      <div className="divider-homepage"></div>

      <GlobalToolBox />

      <div className="divider-homepage"></div>

      <div className="row block-body-homepage">
        <div className="col-12 col-lg-8 block-body-left">

          <div className="row">
            <div className="col-12 box-divider-custom">
              <div style={{display: "flex", justifyContent: "space-between", alignItems: "center"}}>
                <h2 className="title-block-update-interest">ĐÁNG CHÚ Ý</h2>
                <a href="/thong-tin-tai-chinh-moi-nhat">Xem thêm</a>
              </div>
              <div className="divide-title-block"></div>
            </div>
          </div>

          <div className="block-article-first row">
            {
              dataNewest && dataNewest.slice(3, 9).map((item, index) => (
                <div className="col-12 col-lg-4 article-first-item" key={index}>
                  <div className="article-first-item-box-image">
                    <a href={`/blog/${item.slug}`}>
                      <LazyLoad once={true} placeholder={"Loading..."}>
                        <Image
                          className="article-item-detail-image"
                          alt={item.title}
                          src={item.feature_image ? item.feature_image : "/images/finance-banner.png"}
                          layout="responsive"
                          width={260}
                          height={160}
                        />
                      </LazyLoad>
                    </a>
                  </div>
                  <div className="article-first-item-box-content">
                    <p className="article-item-detail-tag">
                      <span className="article-item-detail-tag-date">{item.publish_date}</span>
                      <span className="article-item-detail-tag-seprator"></span>
                      {item.category && item.category.length > 0 && item.category[0].slug && <a href={`/tag/${item.category[0].slug}`} className="article-item-detail-tag-name">{item.category[0].name}</a>}
                    </p>
                    <a href={`/blog/${item.slug}`}>
                      <p className="article-item-detail-text" dangerouslySetInnerHTML={{ __html: item.title }}></p>
                      <p className="article-item-detail-excerpt" dangerouslySetInnerHTML={{ __html: item.excerpt }}></p>
                    </a>
                  </div>
                </div>
              ))
            }
          </div>

          {
            dataCategory && dataCategory.length > 0 && <div className="block-update-interest row">
              <div className="col-12 update-interest-contain" >
                <h1 className="title-block-update-interest" dangerouslySetInnerHTML={{ __html: dataCategory[0].cat_name }}></h1>
                <div className="divide-title-block"></div>
                <div className="update-interest-data">
                  {
                    dataCategory[0].list_posts && dataCategory[0].list_posts.length > 0 && dataCategory[0].list_posts.map((item, index) =>
                      <div className="update-interest-data-item" key={index}>
                        <a href={`/blog/${item.slug}`} className="image-tag-link">
                          <Image
                            className="update-interest-data-item-image"
                            alt={item.title}
                            src={item.feature_image ? item.feature_image : "/images/finance-banner.png"}
                            layout="responsive"
                            width={330}
                            height={198}
                          />
                        </a>
                        <div className="update-interest-data-item-text">
                          <p className="article-item-detail-tag">
                            <span className="article-item-detail-tag-date">{item.publish_date}</span>
                            <span className="article-item-detail-tag-seprator"></span>
                            {item.category && item.category.length > 0 && item.category[0].slug && <a href={`/tag/${item.category[0].slug}`} className="article-item-detail-tag-name">{item.category[0].name}</a>}
                          </p>
                          <a href={`/blog/${item.slug}`}>
                            <p className="update-interest-data-item-text-title" dangerouslySetInnerHTML={{ __html: item.title }}></p>
                            <p className="update-interest-data-item-text-descripton" dangerouslySetInnerHTML={{ __html: item.excerpt }}></p>
                          </a>
                        </div>
                      </div>
                    )
                  }
                </div>
              </div>
            </div>
          }

          <div className="row">
            <div className="col-12 divider-homepage-left">
              <div className="divider-homepage"></div>
            </div>
          </div>

          {
            dataCategory && dataCategory.length > 2 &&
            <>
              <div className="row">
                <div className="col-12 box-divider-custom">
                  <h2 className="title-block-update-interest" dangerouslySetInnerHTML={{ __html: dataCategory[1].cat_name }}></h2>
                  <div className="divide-title-block"></div>
                </div>
              </div>

              <div className="block-article-second row">
                {
                  dataCategory[1].list_posts && dataCategory[1].list_posts.length > 0 && dataCategory[1].list_posts.map((item, index) => (
                    <div className="col-12 col-lg-4 article-second-item" key={index}>
                      <div className="article-second-item-box-image">
                        <a href={`/blog/${item.slug}`}>
                          <LazyLoad once={true} placeholder={"Loading..."}>
                            <img className="article-item-detail-image" src={item.feature_image ? item.feature_image : "/images/finance-banner.png"} width="100%" height="100%" alt="image-article" />
                          </LazyLoad>
                        </a>
                      </div>
                      <div className="article-second-item-box-content">
                        <p className="article-item-detail-tag">
                          <span className="article-item-detail-tag-date">{item.publish_date}</span>
                          <span className="article-item-detail-tag-seprator"></span>
                          {item.category && item.category.length > 0 && item.category[0].slug && <a href={`/tag/${item.category[0].slug}`} className="article-item-detail-tag-name">{item.category[0].name}</a>}
                        </p>
                        <a href={`/blog/${item.slug}`}>
                          <p className="article-item-detail-text" dangerouslySetInnerHTML={{ __html: item.title }}></p>
                          <p className="article-item-detail-excerpt" dangerouslySetInnerHTML={{ __html: item.excerpt }}></p>
                        </a>
                      </div>
                    </div>
                  ))
                }
              </div>
            </>
          }

          <div className="row">
            <div className="col-12 divider-homepage-left">
              <div className="divider-homepage"></div>
            </div>
          </div>

          <div className="block-article-multi-category row">
            {
              dataCategory && dataCategory.slice(2, 20).map((item, index) => (
                <div className="col-12 col-lg-6 block-article-multi-category-item" key={index}>
                  <p className="highlight-category-name"><a href={`/tag/${item.cat_slug}`}>{item.cat_name}</a></p>
                  <div className="divide-highlight-category"></div>
                  <div className="article-item-by-category">
                    {
                      <div className="article-item-top">
                        <a href={`/blog/${item.list_posts[0].slug}`}>
                          <LazyLoad once={true} placeholder={"Loading..."}>
                            <img className="article-item-top-image" src={item.list_posts[0].feature_image ? item.list_posts[0].feature_image : "/images/finance-banner.png"} alt={item.list_posts[0].title} width="100%" height="100%" />
                          </LazyLoad>
                        </a>
                        <div className="article-item-detail-tag">
                          <span className="article-item-detail-tag-date">{item.list_posts[0].publish_date}</span>
                          <span className="article-item-detail-tag-seprator"></span>
                          {item.cat_slug && <a href={`/tag/${item.cat_slug}`} className="article-item-detail-tag-name">{item.cat_name}</a>}
                        </div>
                        <a href={`/blog/${item.list_posts[0].slug}`}>
                          <p className="article-item-top-text" dangerouslySetInnerHTML={{ __html: item.list_posts[0].title }}></p>
                        </a>
                      </div>
                    }
                    <div className="article-item-normal">
                      <ul className="article-item-normal-list">
                        {
                          item.list_posts.map((subItem, indx) => (
                            <li key={indx} className="article-item-normal-list-item">
                              <div className="article-list-item-block">
                                <a href={`/blog/${subItem.slug}`}>
                                  <LazyLoad once={true} placeholder={"Loading..."}>
                                    <img className="article-item-image" src={subItem.feature_image ? subItem.feature_image : "/images/finance-banner.png"} alt={subItem.title} width="100%" height="100%" />
                                  </LazyLoad>
                                </a>
                                <div className="box-item-date-tag">
                                  <div className="article-item-detail-tag">
                                    <span className="article-item-detail-tag-date">{subItem.publish_date}</span>
                                    <span className="article-item-detail-tag-seprator"></span>
                                    {item.cat_slug && <a href={`/tag/${item.cat_slug}`} className="article-item-detail-tag-name">{item.cat_name}</a>}
                                  </div>
                                  <a href={`/blog/${subItem.slug}`}><p className="article-item-text" dangerouslySetInnerHTML={{ __html: subItem.title }}></p></a>
                                </div>
                              </div>
                            </li>
                          ))
                        }
                      </ul>
                    </div>
                  </div>
                </div>
              ))
            }
          </div>
        </div>

        <div className="col-12 col-lg-4 block-body-right">
          <WidgetsDataCrypto dataCrypto = {dataCrypto} />
          <WidgetDataByBank dataInterest={dataInterest} />
          <WidgetBorowData dataBorrow={dataBorrow} />
          <ExchangeRate dataExchange={dataExchange} />
          <VietlotData tableVietlot={tableVietlot} />
        </div>
      </div>
    </>
  )
}