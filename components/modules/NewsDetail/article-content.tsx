import React, { useEffect } from 'react';
import * as tocbot from 'tocbot';

export interface ArticleContentProps {
  detailPost: any;
}

export default function ArticleContentComponent({
  detailPost
}: ArticleContentProps) {

  const clickToShare = (type, url, title?) => {
    switch (type) {
      case 1:
        window.open(`https://www.facebook.com/sharer/sharer.php?u=${url}&amp;src=sdkpreparse`, '_blank');
        break;
      case 2:
        window.open(`https://twitter.com/share?text=${title};url=${url}`, '_blank');
        break;
      case 3:
        window.open(`https://www.reddit.com/submit?url=${url}&title=${title}`, '_blank');
        break;
      case 4:
        window.open(`https://www.linkedin.com/sharing/share-offsite/?url=${url}`, '_blank');
        break;
      case 5:
        window.open(`https://pinterest.com/pin/create/button/?url=${url}`, '_blank');
        break;
      case 6:
        window.open(`whatsapp://send?text=${url}`, '_blank');
        break;
      default:
        break;
    }
  }

  useEffect(() => {
    tocbot.init({
      tocSelector: '.toc',
      contentSelector: '.render-html-page',
      hasInnerContainers: true,
    });
    tocbot.refresh();
  }, [])

  return (
    <div className="box-article-html">
      <div className="box-share-artilce">
        <div className="box-share-artilce-stiky">
          <div className="box-share-artilce-item">
            <span style={{ cursor: "pointer" }} onClick={() => clickToShare(1, `https://money24h.vn/blog/${detailPost && detailPost.detail_post.slug}`)}>
              <img src="/images/icon-share-facebook.svg" alt="icon-share-facebook" width="36px" height="36px" />
            </span>
          </div>
          <div className="box-share-artilce-item">
            <span style={{ cursor: "pointer" }} onClick={() => clickToShare(2, `https://money24h.vn/blog/${detailPost && detailPost.detail_post.slug}`, detailPost && detailPost.detail_post.title)}>
              <img src="/images/icon-share-twitter.svg" alt="icon-share-twitter" width="36px" height="36px" />
            </span>
          </div>
          <div className="box-share-artilce-item">
            <span style={{ cursor: "pointer" }} onClick={() => clickToShare(3, `https://money24h.vn/blog/${detailPost && detailPost.detail_post.slug}`, detailPost && detailPost.detail_post.title)}>
              <img src="/images/icon-share-reddit.svg" alt="icon-share-reddit" width="36px" height="36px" />
            </span>
          </div>
          <div className="box-share-artilce-item">
            <span style={{ cursor: "pointer" }} onClick={() => clickToShare(4, `https://money24h.vn/blog/${detailPost && detailPost.detail_post.slug}`)}>
              <img src="/images/icon-share-linkedin.svg" alt="icon-share-linkedin" width="36px" height="36px" />
            </span>
          </div>
          <div className="box-share-artilce-item">
            <span style={{ cursor: "pointer" }} onClick={() => clickToShare(5, `https://money24h.vn/blog/${detailPost && detailPost.detail_post.slug}`)}>
              <img src="/images/icon-share-pinterest.svg" alt="icon-share-pinterest" width="36px" height="36px" />
            </span>
          </div>
        </div>
      </div>
      <aside className="toc"></aside>
      <div className="render-html-page" dangerouslySetInnerHTML={{ __html: detailPost && detailPost.detail_post.content }}></div>
      <div className="box-share-mobile">
        <div className="share-mobile-block">
          <div className="share-mobile-block-item">
            <span onClick={() => clickToShare(1, `https://money24h.vn/blog/${detailPost && detailPost.detail_post.slug}`)}>
              <div className="box-mobile-share-contain facebook">
                <img className="image-share-mobile" src="/images/icon-share-facebook-mobile.svg" width="15px" height="15px" alt="icon-share-facebook-mobile" />
                <span className="text-share-mobile">Share</span>
              </div>
            </span>
          </div>
          <div className="share-mobile-block-item">
            <span onClick={() => clickToShare(2, `https://money24h.vn/blog/${detailPost && detailPost.detail_post.slug}`, detailPost && detailPost.detail_post.title)}>
              <div className="box-mobile-share-contain tweet">
                <img className="image-share-mobile" src="/images/icon-share-twitter-mobile.svg" width="15px" height="15px" alt="icon-share-twitter-mobile" />
                <span className="text-share-mobile">Tweet</span>
              </div>
            </span>
          </div>
          <div className="share-mobile-block-item">
            <span onClick={() => clickToShare(6, `https://money24h.vn/blog/${detailPost && detailPost.detail_post.slug}`)}>
              <div className="box-mobile-share-contain send">
                <img className="image-share-mobile" src="/images/icon-share-whatapp-mobile.svg" width="15px" height="15px" alt="icon-share-whatapp-mobile" />
                <span className="text-share-mobile">Send</span>
              </div>
            </span>
          </div>
        </div>
      </div>
    </div>
  )
}