import React from 'react';

export interface LatestPostComponentProps {
  detailPost: any;
}

export default function LatestPostComponent({
  detailPost
}: LatestPostComponentProps) {
  return (
    <div className="box-news-lastest-article">
      {detailPost && detailPost.newest_post && detailPost.newest_post.length > 0 &&
        <>
          <a href={`/blog/${detailPost.newest_post[0].slug}`}>
            <div className="hot-item-lastest">
              <img className="hot-item-image" src={detailPost.newest_post[0].feature_image ? detailPost.newest_post[0].feature_image : "/images/image-post-default.png"} alt="image-lastest-article" width="100%" height="100%" />
              <p className="hot-item-title">{detailPost.newest_post[0].title}</p>
            </div>
          </a>
          <div className="divider-article"></div>
        </>
      }
      <div className="news-item-normal">
        {detailPost && detailPost.newest_post && detailPost.newest_post.length > 0 && detailPost.newest_post.slice(1, 5).map((item, index) => (
          <React.Fragment key={index}>
            <a href={`/blog/${item.slug}`}>
              <div className="news-item-normal-detail">
                <img className="item-detail-image" src={item.feature_image ? item.feature_image : "/images/image-post-default.png"} alt="image-lastest-article" width="100%" height="100%" />
                <p className="item-detail-title">{item.title}</p>
              </div>
            </a>
            <div className="divider-article"></div>
          </React.Fragment>
        ))}
      </div>
    </div>
  )
}