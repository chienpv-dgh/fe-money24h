import React from 'react';

export interface InfoAnotherBankProps {
  dataBank: any;
}

const InfoAnotherBank = ({
  dataBank
}: InfoAnotherBankProps) => {
  
  return (
    <div className="">
      <h3 className="info-another-bank-highlight">Xem thêm ngân hàng khác</h3>
      <div className="divide-box-right"></div>
      <div className="box-option-item">
        <div className="option-block cutom-scroll-box-option">
          {dataBank && dataBank.map((item, index) => (
            <a key={index} href={`/blog/${item.postSlug}`}>{item.bankName}</a>
          ))}
        </div>
      </div>
    </div>
  )
}

export default InfoAnotherBank