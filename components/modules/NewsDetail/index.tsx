import React from 'react';
import dynamic from "next/dynamic";
import InfoAnotherBank from './info-another-bank';
import ToolCalcByBank from './tool-calc-by-bank';
import InfoAuthorComponent from './info-author';
import LatestPostComponent from './lastest-post';
import ArticleContentComponent from './article-content';

const WidgetDataByBank = dynamic(() => import("../../widgets/DataByBank"));
const VietlotData = dynamic(() => import("../../widgets/DataVietlot"));
const ArticleInterested = dynamic(() => import('../../widgets/ArticleInterested/article-interested'));
const WidgetBorrowData = dynamic(() => import('../../widgets/DataBorrow'));
const ExchangeRate = dynamic(() => import('../../widgets/DataExchange'));
const WidgetsDataCrypto = dynamic ( ()=> import("../../widgets/DataCryptoCurrency") )

export interface NewsDetailModuleProps {
  dataBank: any;
  isBank: boolean;
  slugOfPost: string;
  dataInterest: {
    offline: {
      bankName: string;
      months1?: string;
      months3?: string;
      months6?: string;
      months9?: string;
      months12?: string;
      slug: string;
      type: number;
    }[];
    online: {
      bankName: string;
      months1?: string;
      months3?: string;
      months6?: string;
      months9?: string;
      months12?: string;
      slug: string;
      type: number;
    }[];
  };
  tableVietlot: {
    description: string;
    name: string;
    period: string;
    reward: number;
  }[];
  dataBorrow: {
    bankName: string;
    detail: string;
    idPost: string;
    months: string;
    results: {
      result?: string;
    };
    share?: string;
    slug: string;
  }[];
  dataExchange: {
    bankName: string;
    idPost: string;
    results: {
      cash: string;
      flagCash?: number;
      flagSell?: number;
      flagTrans?: number;
      fullSlug: string;
      moneyCode: string;
      moneyName: string;
      sellRate: string;
      share?: string;
      transfer: string;
    }[];
    share?: string;
    slug: string;
  };
  detailPost: any;
  dataCrypto: any;
}

export default function NewsDetail({ 
  dataBank, 
  isBank, 
  slugOfPost, 
  dataInterest, 
  tableVietlot, 
  dataExchange, 
  dataBorrow, 
  detailPost,
  dataCrypto
}: NewsDetailModuleProps) {

  return (
    <div className="container-block-new-detail">
      {detailPost && detailPost.detail_post && <p className="bread-scrum-post-page">
        <a href="/">Trang chủ</a>
        <i className="fa fa-angle-right bread-scrum-divider" aria-hidden="true"></i>
        <a href={`/tag/${detailPost && detailPost.detail_post.category[0].slug}`}>{detailPost && detailPost.detail_post.category[0].name}</a>
        <i className="fa fa-angle-right bread-scrum-divider post" aria-hidden="true"></i>
        <span dangerouslySetInnerHTML={{ __html: detailPost && detailPost.detail_post.title }}></span>
      </p>}
      {/* <ins className="adsbygoogle"
        style={{ display: "block", textAlign: "center" }}
        data-ad-layout="in-article"
        data-ad-format="fluid"
        data-ad-client="ca-pub-5581868270899806"
        data-ad-slot="1179910330">
      </ins> */}
      <div className="row">
        <div className="block-detail-left col-12 col-lg-8">
          <h1 className="title-blog-detail" dangerouslySetInnerHTML={{ __html: detailPost && detailPost.detail_post.title }}></h1>
          <InfoAuthorComponent detailPost={detailPost}/>
          {detailPost && detailPost.detail_post.feature_image && <img className="image-cover-article" src={detailPost.detail_post.feature_image ? detailPost.detail_post.feature_image : "/images/image-post-default.png"} width="100%" height="auto" alt="image-cover" />}
          <ArticleContentComponent detailPost={detailPost}/>
        </div>
        <div className="block-suggest-right col-12 col-lg-4">
          <div className="block-check-interest-rate">
            <WidgetsDataCrypto dataCrypto={dataCrypto}/>
            <WidgetDataByBank dataInterest={dataInterest} />
            <WidgetBorrowData dataBorrow={dataBorrow} />
            {dataExchange && <ExchangeRate dataExchange={dataExchange} />}
            <VietlotData tableVietlot={tableVietlot} />
          </div>
          {isBank && <div className="info-another-bank-block">
            <InfoAnotherBank dataBank={dataBank} />
          </div>}
          {isBank && <div className="info-another-bank-block"><ToolCalcByBank dataBank={dataBank} slugOfPost={slugOfPost} /></div>}
          <div className="block-lastest-news">
            <h3 className="lastest-news-box-highlight">Bài viết mới nhất</h3>
            <div className="divider-block-suggest-right"></div>
            <LatestPostComponent detailPost={detailPost} />
          </div>
        </div>
      </div>
      <div className="block-interested-article">
        <p className="interested-article-box-highlight">CÓ THỂ BẠN QUAN TÂM</p>
        <div className="divider-box-highlight"></div>
        {detailPost && detailPost.newest_post && detailPost.newest_post.length > 0 && <ArticleInterested AllPosts={detailPost.newest_post} />}
      </div>
    </div>
  );
}