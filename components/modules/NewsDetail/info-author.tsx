import React, { useEffect, useState } from 'react';
import 'rc-rate/assets/index.css';
import Rate from 'rc-rate';

export interface InfoAuthorProps {
  detailPost: any;
}

export default function InfoAuthorComponent({
  detailPost
}: InfoAuthorProps) {
  const [reviewed, setReviewed] = useState(false);
  const [rating, setRating] = useState(
    detailPost && detailPost.detail_post ? detailPost.detail_post.rating : 0
  );
  const [ratingCount, setRatingCount] = useState(
    detailPost && detailPost.detail_post ? detailPost.detail_post.count_rating : 0
  );

  const handleReview = async (value: number) => {

    let blogSlug = detailPost.detail_post.slug
    try {
      const item = window.localStorage.getItem('review-blog');
      if(item) {
        let historyReview = JSON.parse(item)
        let reviewCheck = historyReview.filter((item) => item == blogSlug)  
        if(reviewCheck && reviewCheck.length > 0) {
          return
        } else {
          historyReview.push(blogSlug)
          window.localStorage.setItem('review-blog', JSON.stringify(historyReview));
        }
      } else {
        let dataReview = []
        dataReview.push(blogSlug)
        window.localStorage.setItem('review-blog', JSON.stringify(dataReview));
      }
      setReviewed(true)
      let resAddRating = await fetch(`https://admin.money24h.vn/wp-json/totapi/posts/add_review_post_detail?key=4746a0cc735cb3ea921a01ffb1e5c257&slug=${encodeURIComponent(blogSlug)}&star=${value}&current_time=${new Date().getTime()}`)
      let dataRating = await resAddRating.json()
      if(dataRating.code == 'success') {
        setRating(dataRating.rating)
        setRatingCount(dataRating.count_rating)
      }
    } catch (error) {
      console.log(error);
      let dataReview = []
      dataReview.push(blogSlug)
      window.localStorage.setItem('review-blog', JSON.stringify(dataReview));
      setReviewed(true)
    }
  }

  useEffect(() => {

    let flagReview = false

    let blogSlug = detailPost.detail_post.slug
    try {
      const item = window.localStorage.getItem('review-blog');
      if(item) {
        let historyReview = JSON.parse(item)
        let reviewCheck = historyReview.filter((item) => item == blogSlug)  
        if(reviewCheck && reviewCheck.length > 0) {
          flagReview = true
        } else {
          flagReview = false
        }
      } else {
        flagReview = false
      }
      setReviewed(flagReview)
    } catch (error) {
      flagReview = false
      setReviewed(flagReview)
      console.log(error);
    }
  }, [detailPost.detail_post.slug])

  return (
    <div className="box-info-author">
      <div className="info-author-left">
        <a href={`/author/${detailPost && detailPost.detail_post.author.slug}`}>
          <img
            className="box-info-author-image"
            src={
              detailPost && detailPost.detail_post.author.profile_image
                ? detailPost.detail_post.author.profile_image
                : '/images/default-user-avatar.svg'
            }
            width="100%"
            height="100%"
            alt="author-image"
          />
        </a>
      </div>
      <div className="info-author-right">
        <p className="info-author-public">{`Published ${
          detailPost && detailPost.detail_post.publish_date
        }`}</p>
        <div className="info-author-name">
          <div>
            <span>By </span>
            <a
              href={`/author/${detailPost && detailPost.detail_post.author.slug}`}
              className="info-author-name-link"
            >
              <span>{detailPost && detailPost.detail_post.author.name}</span>
            </a>
          </div>
          <div>
            <Rate
              defaultValue={rating}
              onChange={(value) => handleReview(value)}
              className="rate-custom"
              allowHalf
              allowClear={false}
              disabled={reviewed}
            />
            <span className="detail-rating">{`${rating}/5 - (${ratingCount} bình chọn)`}</span>
          </div>
        </div>
      </div>
    </div>
  );
}
