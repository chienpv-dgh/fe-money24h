import React from 'react';

export interface ToolCalcByBankProps {
  dataBank: any;
  slugOfPost: string;
}

const ToolCalcByBank = ({
  dataBank, 
  slugOfPost
}: ToolCalcByBankProps) => {
  
  let bankCheck = dataBank.filter((item) => item.postSlug == slugOfPost)
  
  return (
    <div className="">
      <h3 className="info-another-bank-highlight">Công cụ tra cứu, tính toán</h3>
      <div className="divide-box-right"></div>
      <div className="box-option-item">
        <div className="option-block cutom-scroll-box-option">
          {bankCheck.length > 0 && <><a href={`/lai-suat-gui-tiet-kiem-${bankCheck[0].slug}`}>
            <span>{`Lãi suất tiết kiệm ${bankCheck[0].bankName}`}</span>
            <i className="fa fa-chevron-right" aria-hidden="true"></i>
          </a>
          <a href={`/lai-suat-vay-${bankCheck[0].slug}`}>
            <span>{`Lãi suất vay ${bankCheck[0].bankName}`}</span>
            <i className="fa fa-chevron-right" aria-hidden="true"></i>
          </a>
          <a href={`/ty-gia-ngoai-te-${bankCheck[0].slug}`}>
            <span>{`Tỷ giá ngoại tệ ${bankCheck[0].bankName}`}</span>
            <i className="fa fa-chevron-right" aria-hidden="true"></i>
          </a></>}
          <a href="/cong-cu-tinh-tien-lai-gui-ngan-hang">
            <span>{`Công cụ tính lãi suất tiết kiệm`}</span>
            <i className="fa fa-chevron-right" aria-hidden="true"></i>
          </a>
          <a href="/cong-cu-tinh-toan-khoan-vay-ngan-hang">
            <span>{`Công cụ tính lãi suất vay`}</span>
            <i className="fa fa-chevron-right" aria-hidden="true"></i>
          </a>
          <a href="/cong-cu-tinh-ty-gia-ngoai-te">
            <span>{`Công cụ tính tỷ giá ngoại tệ`}</span>
            <i className="fa fa-chevron-right" aria-hidden="true"></i>
          </a>
        </div>
      </div>
    </div>
  )
}

export default ToolCalcByBank