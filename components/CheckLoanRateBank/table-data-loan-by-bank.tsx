import React, { Component } from 'react';
import { Table } from 'reactstrap';
import { ModalShowData } from '.';

interface IRecipeProps {
  dataTableLoanRateBank?: any;
  loanRateAllBank?: any;
  loanRateByBank?: any;
}
interface IRecipeState {
  openModal?: boolean;
  data?: string;
  col?: number;
  sortType?: boolean;
  dataSort?: any;
}

class TableDataLoanByBank extends Component<IRecipeProps,IRecipeState> {

  constructor(props) {
    super(props);
    this.state = {
      openModal: false,
      data: "",
      col: 1,
      sortType: false,
      dataSort: null
    }
  }
  
  sortDataTable = async (type, col) => {
    let data = [...this.props.dataTableLoanRateBank]
    for(let i = 0; i < data.length; i++) {
      
      if(data[i].results) {
        if(data[i].results.result) {
          data[i].results = data[i].results.result
        } else {
          data[i].results = data[i].results
        }
      } else {
        data[i].results = 0
      }
    }
    
    if(type == true) {
      let tempSort = [...data].sort((a, b) => Number(b.results) - Number(a.results))
      await this.setState({dataSort: tempSort})
    } else {
      let tempSort = [...data].sort((a, b) => Number(a.results) - Number(b.results))
      let tempArr = []
      let tempUnUse = []
      for(let i = 0; i < tempSort.length; i++) {
        if(tempSort[i].results > 0 && tempSort[i].results !== 0) {
          tempArr.push(tempSort[i])
        } else {
          tempUnUse.push(tempSort[i])
        }
      }
      let totalArr = tempArr.concat(tempUnUse)
      await this.setState({dataSort: totalArr})
    }
  }

  checkRenderTimeDate = (date) => {
    let receiveDate = new Date(date)
    let year = receiveDate.getFullYear()
    let month = receiveDate.getMonth() + 1
    let day = receiveDate.getDate()
    let result = `${day}/${month}/${year}`

    return result
  }

  componentDidMount() {
    this.sortDataTable(false, 1)
  }

  render() {
    
    return (
      <div className="table-data-by-bank">
        {this.props.loanRateByBank.length > 0 && <h1 className="table-data-title-by-bank">{`LÃI SUẤT VAY ${this.props.loanRateByBank[0].bankName}`}</h1>}
        <div className="description-table">
          <p>{`(Cập nhật mới nhất ngày ${this.checkRenderTimeDate(new Date())})`}</p>
          <p style={{color: "#F60707"}}>Lãi suất cuối kỳ: %/năm</p>
        </div>
        <div className="custom-table-interest-by-bank">
          <Table responsive>
            <thead>
              <tr>
                <th>Ngân hàng</th>
                <th onClick={() => this.setState({sortType: !this.state.sortType}, () => this.sortDataTable(this.state.sortType, 1))} style={{cursor: "pointer"}}>
                  <div className="sort-thead" style={{display: this.props.loanRateByBank.length == 0 ? "flex" : "block"}}>
                    <span>Lãi suất</span>
                    {this.props.loanRateByBank.length == 0 && <div className="sort-box-icon">
                      <i className="fa fa-caret-up" aria-hidden="true" style={{color: this.state.col == 1 && this.state.sortType ? "#FFFFFF" : "rgba(255, 255, 255, 0.2)"}}></i>
                      <i className="fa fa-caret-down" aria-hidden="true" style={{color: this.state.col == 1 && !this.state.sortType ? "#FFFFFF" : "rgba(255, 255, 255, 0.2)"}}></i>
                    </div>}
                  </div>
                </th>
                <th>Thời gian ưu đãi</th>
                <th style={{width: "40%"}}>Ghi chú</th>
              </tr>
            </thead>
            <tbody>
              {this.state.dataSort && this.state.dataSort.map((item, index) =>
                <tr key={index}>
                  <td>{item.bankName ? <div><a href={`/lai-suat-vay-${item.slug}`}>{item.bankName}</a></div>  : "-"}</td>
                  <td>{item.results ? `${item.results}%` : "-"}</td>
                  <td>{item.months || "-"}</td>
                  <td>{item.detail ? <>
                    {this.props.loanRateByBank.length == 0 ? <>
                      <div className="hidden-box-note" dangerouslySetInnerHTML={{ __html: item.detail }} style={{textAlign: "left"}}></div>
                    <p className="box-note-showmore" onClick={() => this.setState({openModal: true, data: item.detail})}>See more...</p>
                    </> : <div dangerouslySetInnerHTML={{ __html: item.detail }} style={{textAlign: "left"}}></div>}
                    </> : "-"}</td>
                </tr>
              )}
            </tbody>
          </Table>
        </div>
        <ModalShowData isOpen={this.state.openModal} toggle={() => this.setState({ openModal: false })} data={this.state.data}/>
      </div>
    )
  }
}

export default TableDataLoanByBank;