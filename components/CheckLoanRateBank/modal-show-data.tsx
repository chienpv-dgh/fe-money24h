import React, { Component } from 'react';
import { Modal, ModalBody } from 'reactstrap';

interface IRecipeProps {
  isOpen?: boolean;
  toggle?: any;
  data?: string;
}

interface IRecipeState {}

class ModalShowData extends Component<IRecipeProps,IRecipeState> {
  render() {
    return (
      <Modal
        centered
        isOpen={this.props.isOpen}
        toggle={this.props.toggle}
        style={{ maxWidth: "800px" }}
      >
        <ModalBody>
          <div dangerouslySetInnerHTML={{ __html: this.props.data }}></div>
        </ModalBody>
      </Modal>
    );
  }
}

export default ModalShowData;