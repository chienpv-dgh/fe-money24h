import CheckLoanRateBank from "./check-loan-rate-bank-page";
import BoxChooseLoanBank from "./box-choose-loan-bank";
import TableDataLoanByBank from "./table-data-loan-by-bank";
import ModalShowData from "./modal-show-data";
export {
  CheckLoanRateBank,
  BoxChooseLoanBank,
  TableDataLoanByBank,
  ModalShowData
}