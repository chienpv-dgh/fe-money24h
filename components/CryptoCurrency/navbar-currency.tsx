import React, { useEffect, useState } from 'react';

export interface CryptoCurrencyNavbarProps {
  currencyDataEng: {
    status: {
      timestamp: string;
      total_count: number;
    };
    data: {
      circulating_supply: number;
      cmc_rank: number;
      id: number;
      last_updated: string;
      max_supply: number;
      total_supply: number;
      name: string;
      num_market_pairs: number;
      slug: string;
      symbol: string;
      quote: {
        fully_diluted_market_cap: number;
        last_updated: string;
        market_cap: number;
        market_cap_dominance: number;
        percent_change_1h: number;
        percent_change_7d: number;
        percent_change_24h: number;
        percent_change_30d: number;
        percent_change_60d: number;
        percent_change_90d: number;
        price: number;
        volume_24h: number;
        volume_change_24h: number;
      }[];
    }[];
  };
}

export default function CryptoCurrencyNavbar({
  currencyDataEng
 }: CryptoCurrencyNavbarProps) {

  return (
    <div className='navbar-currency-container'>
      <div className='navbar-container'>
        <ul className='navbar-list'>
          {currencyDataEng && currencyDataEng.data.length > 0 && currencyDataEng.data.slice(0, 20).map((navItem, index) => (
            <li className='navbar-item' key={index}>
              <a href={`/tien-ao/${navItem.symbol}`} title={`Giá tiền ${navItem.name}`}>
                <span>{navItem.symbol}</span>
              </a>
            </li>
          ))}
        </ul>
      </div>
    </div>
  )
}