import React, { useEffect, useState } from 'react';
import { Pagination, PaginationItem, PaginationLink } from 'reactstrap';

export interface CryptoCurrencyPaginationProps {
  currentPage: number;
  currencyDataEng: {
    status: {
      timestamp: string;
      total_count: number;
    };
    data: {
      circulating_supply: number;
      cmc_rank: number;
      id: number;
      last_updated: string;
      max_supply: number;
      total_supply: number;
      name: string;
      num_market_pairs: number;
      slug: string;
      symbol: string;
      quote: {
        fully_diluted_market_cap: number;
        last_updated: string;
        market_cap: number;
        market_cap_dominance: number;
        percent_change_1h: number;
        percent_change_7d: number;
        percent_change_24h: number;
        percent_change_30d: number;
        percent_change_60d: number;
        percent_change_90d: number;
        price: number;
        volume_24h: number;
        volume_change_24h: number;
      }[];
    }[];
  };
}

export default function CryptoCurrencyPagination({ 
  currentPage,
  currencyDataEng
}: CryptoCurrencyPaginationProps) {

  const [pageItem, setPageItem] = useState([])
  
  let maxPages = Math.ceil(currencyDataEng.status.total_count / 100);

  useEffect(() => {
    let items = [];
    let leftSide = Number(currentPage) - 2;
    if(leftSide <= 0 ) {
      leftSide = 1
    }

    let rightSide = Number(currentPage) + 2;
    if(rightSide > maxPages) {
      rightSide = maxPages
    }

    for (let number = leftSide ; number <= rightSide; number++) {
      items.push(
        <PaginationItem key={number} active={number == currentPage ? true : false}>
          <PaginationLink href={`/tien-ao?page=${number}`}>
            {number}
          </PaginationLink>
        </PaginationItem>,
      );
    }

    setPageItem(items)
  }, [currentPage, maxPages])

  const checkPagination = (type: string) => {
    let status = true

    switch (type) {
      case "first":
        if(currentPage > 1) {
          status = false
        } else {
          status = true
        }
        break;
      case "last":
        if(currentPage >= maxPages) {
          status = true
        } else {
          status = false
        }
        break;
      default:
        break;
    }

    return status
  }

  return (
    <div className='currency-pagination'>
      <p className='total-page'>{`Trang ${currentPage}/${maxPages}`}</p>
      <Pagination className='pagination-list'>
        <PaginationItem disabled={checkPagination("first")}>
          <PaginationLink
            first
            href="/tien-ao?page=1"
          />
        </PaginationItem>
        <PaginationItem disabled={checkPagination("first")}>
          <PaginationLink
            href={`/tien-ao?page=${Number(currentPage) - 1}`}
            previous
          />
        </PaginationItem>
        {pageItem}
        <PaginationItem disabled={checkPagination("last")}>
          <PaginationLink
            href={`/tien-ao?page=${Number(currentPage) + 1}`}
            next
          />
        </PaginationItem>
        <PaginationItem disabled={checkPagination("last")}>
          <PaginationLink
            href={`/tien-ao?page=${maxPages}`}
            last
          />
        </PaginationItem>
      </Pagination>
    </div>
  )
}