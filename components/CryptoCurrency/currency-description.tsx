import React, { useEffect, useState } from 'react';

export interface CurrencyDescriptionModuleProps {
  dataMakerCap: {
    btc_dominance: number;
    eth_dominance: number;
    quote: {
      total_market_cap: number;
      total_volume_24h: number;
    }[];
  };
  currencyDataEng: {
    status: {
      timestamp: string;
      total_count: number;
    };
    data: {
      circulating_supply: number;
      cmc_rank: number;
      id: number;
      last_updated: string;
      max_supply: number;
      total_supply: number;
      name: string;
      num_market_pairs: number;
      slug: string;
      symbol: string;
      quote: {
        fully_diluted_market_cap: number;
        last_updated: string;
        market_cap: number;
        market_cap_dominance: number;
        percent_change_1h: number;
        percent_change_7d: number;
        percent_change_24h: number;
        percent_change_30d: number;
        percent_change_60d: number;
        percent_change_90d: number;
        price: number;
        volume_24h: number;
        volume_change_24h: number;
      }[];
    }[];
  };
}

export default function CryptoCurrencyDescription({ 
  dataMakerCap,
  currencyDataEng
}: CurrencyDescriptionModuleProps) {

  const formatNumberWithComma = (x) => {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }

  return (
    <div className='currency-description'>
      <h1 className='main-title'>
        Bảng giá tiền ảo, coin, tiền diện tử hôm nay
      </h1>
      <p className='text-currency'>Bảng giá cập nhật liền tục <strong>{currencyDataEng.status.total_count}</strong> đồng Coin và Token phổ biến hiện nay trên thị trường tiền số.</p>
      <p className='text-currency'>Vốn hóa thị trường lúc này là <strong>{`$${formatNumberWithComma(Math.round((dataMakerCap.quote[0].total_market_cap * 100) / 100))}`}</strong> trong đó giá trị <strong>{`BTC chiếm ${Math.round((dataMakerCap.btc_dominance * 100)) / 100}%`}</strong>, <strong>{`ETH chiếm ${Math.round((dataMakerCap.eth_dominance * 100)) / 100}%`}</strong>.</p>
      <p className='text-currency'>Khối lượng giao dịch trong 24 giờ qua là <strong>{`$${formatNumberWithComma(Math.round((dataMakerCap.quote[0].total_volume_24h * 100) / 100))}`}</strong>.</p>
      <p className='text-currency'>Bảng tỷ giá được sắp xếp theo giá trị <strong>Vốn hóa thị trường</strong> từ cao đến thấp.</p>
      <p className='text-currency'>Bảng giá này được đồng bộ trực tiếp từ hệ thống bảng giá tiền điện tử <a target='_blank' href='https://coinmarketcap.com/' rel='noreferrer'>CoinMarketCap</a> lớn nhất thế giới.</p>
    </div>
  )
}