import React, { useEffect, useState } from 'react';
import LazyLoad from 'react-lazyload';
import dynamic from "next/dynamic";
// import CryptoCurrencyNavbar from './navbar-currency';
// import CryptoCurrencyDescription from './currency-description';
// import CryptoCurrencyTable from './table-currency';
// import CryptoCurrencyPagination from './currency-pagination';

const CryptoCurrencyNavbar = dynamic(() => import("./navbar-currency"));
const CryptoCurrencyDescription = dynamic(() => import("./currency-description"));
const CryptoCurrencyTable = dynamic(() => import("./table-currency"));
const CryptoCurrencyPagination = dynamic(() => import("./currency-pagination"));

const VietlotData = dynamic(() => import("../widgets/DataVietlot"));
const WidgetDataByBank = dynamic(() => import("../widgets/DataByBank"));
const ExchangeRate = dynamic(() => import("../widgets/DataExchange"));
const WidgetBorrowData = dynamic(() => import("../widgets/DataBorrow"));
const WidgetsDataCrypto = dynamic( ()=> import("../widgets/DataCryptoCurrency") );

export interface quoteObject{
  fully_diluted_market_cap: number;
  last_updated: string;
  market_cap: number;
  market_cap_dominance: number;
  percent_change_1h: number;
  percent_change_7d: number;
  percent_change_24h: number;
  percent_change_30d: number;
  percent_change_60d: number;
  percent_change_90d: number;
  price: number;
  volume_24h: number;
  volume_change_24h: number;
}
export interface CryptoCurrencyModuleProps {
  dataInterest: {
    offline: {
      bankName: string;
      months1?: string;
      months3?: string;
      months6?: string;
      months9?: string;
      months12?: string;
      slug: string;
      type: number;
    }[];
    online: {
      bankName: string;
      months1?: string;
      months3?: string;
      months6?: string;
      months9?: string;
      months12?: string;
      slug: string;
      type: number;
    }[];
  };
  dataBorrow: {
    bankName: string;
    detail: string;
    idPost: string;
    months: string;
    results: {
      result?: string;
    };
    share?: string;
    slug: string;
  }[];
  dataExchange: {
    bankName: string;
    idPost: string;
    results: {
      cash: string;
      flagCash?: number;
      flagSell?: number;
      flagTrans?: number;
      fullSlug: string;
      moneyCode: string;
      moneyName: string;
      sellRate: string;
      share?: string;
      transfer: string;
    }[];
    share?: string;
    slug: string;
  };
  tableVietlot: {
    description: string;
    name: string;
    period: string;
    reward: number;
  }[];
  currentPage: number;
  currencyDataEng: {
    status: {
      timestamp: string;
      total_count: number;
    };
    data: {
      circulating_supply: number;
      cmc_rank: number;
      id: number;
      last_updated: string;
      max_supply: number;
      total_supply: number;
      name: string;
      num_market_pairs: number;
      slug: string;
      symbol: string;
      quote: {
        fully_diluted_market_cap: number;
        last_updated: string;
        market_cap: number;
        market_cap_dominance: number;
        percent_change_1h: number;
        percent_change_7d: number;
        percent_change_24h: number;
        percent_change_30d: number;
        percent_change_60d: number;
        percent_change_90d: number;
        price: number;
        volume_24h: number;
        volume_change_24h: number;
      }[];
    }[];
  };
  currencyDataVi: {
    status: {
      timestamp: string;
      total_count: number;
    };
    data: {
      circulating_supply: number;
      cmc_rank: number;
      id: number;
      last_updated: string;
      max_supply: number;
      total_supply: number;
      name: string;
      num_market_pairs: number;
      slug: string;
      symbol: string;
      quote: {
        fully_diluted_market_cap: number;
        last_updated: string;
        market_cap: number;
        market_cap_dominance: number;
        percent_change_1h: number;
        percent_change_7d: number;
        percent_change_24h: number;
        percent_change_30d: number;
        percent_change_60d: number;
        percent_change_90d: number;
        price: number;
        volume_24h: number;
        volume_change_24h: number;
      }[];
    }[];
  };
  dataCrypto: {
    status: {
        timestamp: string;
        total_count: number;
    };
    data: {
      circulating_supply: number;
      cmc_rank: number;
      id: number;
      last_updated: string;
      max_supply: number;
      total_supply: number;
      name: string;
      num_market_pairs: number;
      slug: string;
      symbol: string;
      quote: quoteObject[];
    }[];
  };
  dataMakerCap: {
    btc_dominance: number;
    eth_dominance: number;
    quote: {
      total_market_cap: number;
      total_volume_24h: number;
    }[];
  }
}

export default function CryptoCurrencyPageComponent({ 
  dataInterest,
  dataBorrow,
  dataExchange,
  tableVietlot,
  currentPage,
  currencyDataEng,
  currencyDataVi,
  dataCrypto,
  dataMakerCap
}: CryptoCurrencyModuleProps) {

  return (
    <div className='currency-page-container'>

      <p className="bread-scrum-post-page">
        <a href="/">Trang chủ</a>
        <i className="fa fa-angle-right bread-scrum-divider" aria-hidden="true"></i>
        <span>Tiền ảo</span>
      </p>

      <CryptoCurrencyNavbar currencyDataEng={currencyDataEng}/>

      <div className='currency-body'>
        <div className='row'>
          <div className='col-12 col-lg-8 currency-body-left'>
            <CryptoCurrencyDescription dataMakerCap={dataMakerCap} currencyDataEng={currencyDataEng}/>
            <CryptoCurrencyTable 
              currencyDataEng={currencyDataEng}
              currencyDataVi={currencyDataVi}
            />
            <CryptoCurrencyPagination currentPage={currentPage} currencyDataEng = {currencyDataEng}/>
          </div>
          <div className='col-12 col-lg-4 currency-body-right'>
            <WidgetsDataCrypto dataCrypto={dataCrypto} />
            <WidgetDataByBank dataInterest={dataInterest} />
            <WidgetBorrowData dataBorrow={dataBorrow} />
            {dataExchange && <ExchangeRate dataExchange={dataExchange} />}
            
            <VietlotData tableVietlot={tableVietlot} />

            {/* <LazyLoad offset={100} placeholder="Loading..."><WidgetDataByBank dataInterest={dataInterest} /></LazyLoad>
            <LazyLoad offset={100} placeholder="Loading..."><WidgetBorrowData dataBorrow={dataBorrow} /></LazyLoad>
            {dataExchange && <LazyLoad offset={100} placeholder="Loading..."><ExchangeRate dataExchange={dataExchange} /></LazyLoad>}
            <LazyLoad offset={100} placeholder="Loading..."><VietlotData tableVietlot={tableVietlot} /></LazyLoad> */}
          </div>
        </div>
      </div>

    </div>
  )
}