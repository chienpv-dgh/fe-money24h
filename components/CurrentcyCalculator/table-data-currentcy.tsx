import React, { Component } from 'react';
import { Table } from 'reactstrap';

interface IRecipeProps {
  dataTableCurrentcy?: {
    bankName: string;
    results: {
      moneyName: string;
      moneyCode: string;
      cash: string;
      transfer: string;
      sellRate: string;
      flagCash: number;
      flagTrans: number;
      flagSell: number;
      fullSlug: string;
    }[];
  }
}
interface IRecipeState {}
class TableDataCurrentcy extends Component<IRecipeProps,IRecipeState> {

  checkRenderTimeDate = (date) => {
    let receiveDate = new Date(date)
    let year = receiveDate.getFullYear()
    let month = receiveDate.getMonth() + 1
    let day = receiveDate.getDate()
    let result = `${day}/${month}/${year}`

    return result
  }

  detectImage = (code) => {
    let image = ""
    switch (code) {
      case 1:
        image = "icon-increase-curentcy.svg"
        break;
      case -1:
        image = "icon-decrease-currentcy.svg"
        break;
      default:
        break;
    }
    return image
  }

  render() {
    
    return (
      <div className="table-data-currentcy">
        <p className="table-data-currentcy-title">
          {`TỶ GIÁ NGOẠI TỆ ${this.props.dataTableCurrentcy.bankName || ""}`}
        </p>
        <p className="table-data-currentcy-time-update">{`(Cập nhật mới nhất ngày ${this.checkRenderTimeDate(new Date())})`}</p>
        <div className="custom-table-currentcy">
          <Table responsive>
            <thead>
              <tr>
                <th rowSpan={2}>Ngoại tệ</th>
                <th rowSpan={2}>Mã ngoại tệ</th>
                <th colSpan={2}>Tỷ giá mua</th>
                <th rowSpan={2}>Tỷ giá bán</th>
              </tr>
              <tr>
                <th>Tiền mặt</th>
                <th>Chuyển khoản</th>
              </tr>
            </thead>
            <tbody>
              {this.props.dataTableCurrentcy ?
                this.props.dataTableCurrentcy.results && this.props.dataTableCurrentcy.results.length > 0 && this.props.dataTableCurrentcy.results.map((item, index) => (
                  <tr key={index}>
                    <td><a href={`/ty-gia-${item.fullSlug}`}>{item.moneyName}</a></td>
                    <td>{item.moneyCode}</td>
                    <td>
                      <div className="box-status-currentcy">
                        <span className="status-currentcy-text">{item.cash}</span>
                        {item.flagCash ? <img className="status-currentcy-image" src={`/images/${this.detectImage(item.flagCash)}`} alt="status-currentcy"/> : null}
                      </div>
                    </td>
                    <td>
                      <div className="box-status-currentcy">
                        <span className="status-currentcy-text">{item.transfer}</span>
                        {item.flagTrans ? <img className="status-currentcy-image" src={`/images/${this.detectImage(item.flagTrans)}`} alt="status-currentcy"/> : null}
                      </div>
                    </td>
                    <td>
                      <div className="box-status-currentcy">
                        <span className="status-currentcy-text">{item.sellRate}</span>
                        {item.flagSell ? <img className="status-currentcy-image" src={`/images/${this.detectImage(item.flagSell)}`} alt="status-currentcy"/> : null}
                      </div>
                    </td>
                  </tr>
                ))
                :
                null
              }
            </tbody>
          </Table>
        </div>
      </div>
    );
  }
}

export default TableDataCurrentcy;