import React, { Component } from 'react';
import NumberFormat from 'react-number-format';
import { ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import { fetchAPI } from '../../pages/api';

interface IRecipeProps {
  listBankOps?: {
    id: number;
    bankName: string;
  }[];
  getDataTable?: Function;
}
interface IRecipeState {
  type?: string;
  numberSaleCash?: number;
  baseCash?: number;
  resultValue?: number;
  dropdownOpen?: boolean;
  dataMoneyCash?: {
    bankName: string;
    results: {
      moneyName: string;
      moneyCode: string;
      cash: string;
      transfer: string;
      sellRate: string;
      flagCash: number;
      flagTrans: number;
      flagSell: number;
    }[];
  };
  dropdownOpenMoney?: boolean;
  dataMoneyDetail?: number;
  nameBank?: string;
  nameMoney?: string;
  moneyChoose?: {
    moneyName: string;
    moneyCode: string;
    cash: string;
    transfer: string;
    sellRate: string;
  };
}

class ToolCalculatorCurrentcy extends Component<IRecipeProps,IRecipeState> {
  constructor(props) {
    super(props);
    this.state = {
      type: 'buyCash',
      numberSaleCash: null,
      baseCash: null,
      resultValue: null,
      dropdownOpen: false,
      dataMoneyCash: null,
      dropdownOpenMoney: false,
      dataMoneyDetail: null,
      nameBank: "",
      nameMoney: "",
      moneyChoose: null,
    };
  }

  onRadioChange = (e) => {
    this.setState({
      type: e.target.value
    }, () => this.calcChangeCash());
  }

  calcChangeCash = async () => {
    await this.getDetailMoney()
    if(this.state.numberSaleCash) {
      let numberSale = this.state.numberSaleCash
      let baseCash = this.state.baseCash

      let result = numberSale * baseCash

      await this.setState({
        resultValue: Math.round(result)
      })

      this.props.getDataTable(this.state.dataMoneyCash[0])
    } else { return }
  }

  numberWithCommas = (number) => {
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  }

  getMoneyByBank = async (id) => {
    let res = await fetchAPI(`/money24h/exchange-rate-by-bankid?BankId=${id}`)
    if(res.success && res.data) {
      await this.setState({dataMoneyCash: res.data})
    } else {
      await this.setState({dataMoneyCash: null})
    }
  }

  getDetailMoney = async (data?) => {
    let type = this.state.type
    let money = ""
    if(this.state.moneyChoose == null){
      if(data) {
        this.setState({moneyChoose: data})
        switch (type) {
          case "buyCash":
            money = data.sellRate
            break;
          case "transferSale":
            money = data.transfer
            break;
          case "cashSale":
            money = data.cash
            break;
          default:
            money = ""
            break;
        }
      }
    } else {
      if(data) {
        this.setState({moneyChoose: data})
        switch (type) {
          case "buyCash":
            money = data.sellRate
            break;
          case "transferSale":
            money = data.transfer
            break;
          case "cashSale":
            money = data.cash
            break;
          default:
            money = ""
          break;
        }
      } else {
        let data = this.state.moneyChoose
        switch (type) {
          case "buyCash":
            money = data.sellRate
            break;
          case "transferSale":
            money = data.transfer
            break;
          case "cashSale":
            money = data.cash
            break;
          default:
            money = ""
          break;
        }
      }
    }
    let formatMoney = money.replace(/\./g,'').replace(/\,/g,'.')
    
    await this.setState({baseCash: Number(formatMoney)})
  }
  
  render() {
    
    return (
      <div className="block-tool-calc-loan">
        <div className="contain-calc-tool">
          <p className="calc-tool-main-title">CÔNG CỤ TÍNH TOÁN TỶ GIÁ NGOẠI TỆ, <br /> TỶ GIÁ HỐI ĐOÁI</p>
          <div className="check-radio-value row">
              <div className="check-radio-value-item col-12 col-lg-4">
                <input value="buyCash" type="radio" id="buyCash" name="buyCash" checked={this.state.type == "buyCash"}
                  onChange={this.onRadioChange}/>
                <label htmlFor="buyCash" className="label-radio-custom">Mua tiền mặt</label>
              </div>
              <div className="check-radio-value-item col-12 col-lg-4">
                <input value="transferSale" type="radio" id="transferSale" name="transferSale" checked={this.state.type == "transferSale"}
                  onChange={this.onRadioChange}/>
                <label htmlFor="transferSale" className="label-radio-custom">Bán chuyển khoản</label>
              </div>
              <div className="check-radio-value-item col-12 col-lg-4">
                <input value="cashSale" type="radio" id="cashSale" name="cashSale" checked={this.state.type == "cashSale"}
                  onChange={this.onRadioChange}/>
                <label htmlFor="cashSale" className="label-radio-custom">Bán tiền mặt</label>
              </div>
          </div>
          <div className="block-calc-content row">
            <div className="block-calc-content-left col-12 col-lg-5">
              <div className="calc-content-left-item">
                <p className="content-left-title">Ngân hàng *</p>
                <ButtonDropdown className="custom-select-box" isOpen={this.state.dropdownOpen} toggle={() => this.setState({dropdownOpen: !this.state.dropdownOpen})}>
                  <DropdownToggle className="custom-select">
                    {this.state.nameBank || "Chọn ngân hàng"}
                  </DropdownToggle>
                  <DropdownMenu className="custom-select-menu-box customScroll">
                    {this.props.listBankOps ?
                      this.props.listBankOps.map((item, index) => (<DropdownItem onClick={() => this.setState({nameBank: item.bankName, numberSaleCash: null, baseCash: null, resultValue: null, dataMoneyCash: null, moneyChoose: null, nameMoney: ""}, () => this.getMoneyByBank(item.id))} key={index} className="menu-box-item">{item.bankName}</DropdownItem>))
                    : 
                      <DropdownItem className="menu-box-item">Oops! Data not found.</DropdownItem>
                    }
                  </DropdownMenu>
                  <img className="custom-select-image" src="/images/icon-custom-select.svg" alt="icon-custom-select"/>
                </ButtonDropdown>
              </div>
              <div className="calc-content-left-item">
                <p className="content-left-title">Ngoại tệ muốn mua/bán *</p>
                <ButtonDropdown disabled={this.state.dataMoneyCash ? false : true} className="custom-select-box" isOpen={this.state.dropdownOpenMoney} toggle={() => this.setState({dropdownOpenMoney: !this.state.dropdownOpenMoney})}>
                  <DropdownToggle className="custom-select">
                    { this.state.nameMoney || "Chọn ngoại tệ"}
                  </DropdownToggle>
                  <DropdownMenu className="custom-select-menu-box customScroll">
                    {this.state.dataMoneyCash && this.state.dataMoneyCash.results ? 
                      this.state.dataMoneyCash.results.map((item, index) => (<DropdownItem onClick={() => this.setState({nameMoney: `${item.moneyCode} - ${item.moneyName}`}, () => this.getDetailMoney(item))} key={index} className="menu-box-item">{`${item.moneyCode} - ${item.moneyName}`}</DropdownItem>))
                    : 
                      <DropdownItem className="menu-box-item">Oops! Data not found.</DropdownItem>
                    }
                  </DropdownMenu>
                  <img className="custom-select-image" src="/images/icon-custom-select.svg" alt="icon-custom-select"/>
                </ButtonDropdown>
              </div>
              <div className="calc-content-left-item">
                <p className="content-left-title">Tỷ giá hiện hành *</p>
                <NumberFormat className="custom-input-calc-left" disabled value={this.state.baseCash ? this.state.baseCash : 0} thousandSeparator={"."} decimalSeparator={","}
                />
              </div>
              <div className="calc-content-left-item">
                <p className="content-left-title">Số lượng ngoại tệ muốn mua/bán *</p>
                <NumberFormat className="custom-input-calc-left" value={this.state.numberSaleCash ? this.state.numberSaleCash : 0} thousandSeparator={"."} decimalSeparator={","}
                  onValueChange={(values) => {
                    const {formattedValue, value} = values;
                    this.setState({numberSaleCash: Number(value)})
                  }}
                />
              </div>
              <p className="description-text">(*): Thông tin bắt buộc</p>
              <p className="view-result" onClick={() => this.calcChangeCash()}>XEM KẾT QUẢ</p>
            </div>
            <div className="block-calc-content-right col-12 col-lg-5">
              <div className="calc-tool-box">
                <div>
                  <p className="calc-tool-box-tile">Tổng số tiền mua/bán ngoại tệ</p>
                  <div className="calc-tool-box-value">
                    <span>{this.numberWithCommas(this.state.resultValue ? this.state.resultValue : 0)}</span>
                    <img src="/images/icon-unit-blue.svg" alt="icon-unit-blue"/>
                  </div>
                </div>
              </div>
              <div className="view-current-interest-rate">
                <div className="border-box">
                  <p className="border-box-title">XEM LÃI SUẤT HIỆN TẠI</p>
                  <img className="border-box-image" src="/images/icon-arow-blue.svg" alt="icon-arow-blue"/>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ToolCalculatorCurrentcy;