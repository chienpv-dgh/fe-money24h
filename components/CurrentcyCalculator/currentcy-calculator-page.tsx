import React, { Component } from 'react';
import dynamic from "next/dynamic";
import { TableDataCurrentcy, ToolCalculatorCurrentcy } from '.';
import { ContentPageSaving } from '../SavingInterest';

const ArticleInterested = dynamic(() => import('../widgets/ArticleInterested/article-interested'));
const GlobalToolBox = dynamic(() => import("../widgets/GlobalToolBox"));

export interface quoteObject{
  fully_diluted_market_cap: number;
  last_updated: string;
  market_cap: number;
  market_cap_dominance: number;
  percent_change_1h: number;
  percent_change_7d: number;
  percent_change_24h: number;
  percent_change_30d: number;
  percent_change_60d: number;
  percent_change_90d: number;
  price: number;
  volume_24h: number;
  volume_change_24h: number;
}
interface IRecipeProps {
  AllPosts?: any;
  listBankOps?: any;
  postOfPage?: any;
  detalNewsPost?: any;
  dataCrypto: {
    status: {
        timestamp: string;
        total_count: number;
    };
    data: {
      circulating_supply: number;
      cmc_rank: number;
      id: number;
      last_updated: string;
      max_supply: number;
      total_supply: number;
      name: string;
      num_market_pairs: number;
      slug: string;
      symbol: string;
      quote: quoteObject[];
    }[];
  };
}
interface IRecipeState {
  dataTableCurrentcy?: {
    bankName: string;
    results: {
      moneyName: string;
      moneyCode: string;
      cash: string;
      transfer: string;
      sellRate: string;
      flagCash: number;
      flagTrans: number;
      flagSell: number;
      fullSlug: string;
    }[];
  }
}

class CurrentcyCalculatorPage extends Component<IRecipeProps,IRecipeState> {
  constructor(props) {
    super(props);
    this.state = {
      dataTableCurrentcy: null,
    }
  }
  
  getDataTable = (data) => {
    if(data) {
      this.setState({dataTableCurrentcy: data})
    }
  }

  render() {
    
    return (
      <div className="currentcy-calculator-page-container">
        <ToolCalculatorCurrentcy getDataTable={(data) => this.getDataTable(data)} listBankOps={this.props.listBankOps}/>
        {this.state.dataTableCurrentcy && <TableDataCurrentcy dataTableCurrentcy={this.state.dataTableCurrentcy}/>}
        <GlobalToolBox />
        <div className="divider-saving-interest-page"></div>
        {this.props.detalNewsPost.detail_post && <ContentPageSaving showAuthor={false} AllPosts={this.props.AllPosts} postOfPage={this.props.detalNewsPost.detail_post} dataCrypto={this.props.dataCrypto}/>}
        <div className="block-interested-article">
          <p className="interested-article-box-highlight">CÓ THỂ BẠN QUAN TÂM</p>
          <div className="divider-box-highlight"></div>
          <ArticleInterested AllPosts={this.props.AllPosts}/>
        </div>
      </div>
    );
  }
}

export default CurrentcyCalculatorPage;