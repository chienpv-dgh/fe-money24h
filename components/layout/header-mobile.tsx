import React from 'react';
import { useEffect } from "react";
import Image from 'next/image';

export interface HeaderMobileProps {
  dataHeader: string;
  listBank: {
    data: {
      bankName: string;
      idPost: string;
      slug: string;
    }[];
  };
  dataBorrow: {
    bankName: string;
    detail: string;
    idPost: string;
    months: string;
    results: {
      result?: string;
    };
    share?: string;
    slug: string;
  }[];
  listCurrency: {
    data: {
      data: {
        name: string;
        symbol: string;
        slug: string;
        id: number;
      }[];
    }
  };
}

export default function HeaderMobile({ 
  listBank, 
  dataBorrow, 
  listCurrency 
}: HeaderMobileProps) {

  useEffect(() => {
    const script = document.createElement('script');
      script.src = "/library/menu/handle.js";
      script.async = true;

      document.body.appendChild(script);

    return () => {
      document.body.removeChild(script);
    }
  }, []);

  return (
    <div className="mobile-header">
      <div style={{}} className="block-header-mobile">
        <div>
          <a href={"/"}>
            <Image
              alt="logo-header"
              src="/logos/header-logo.png"
              layout="fixed"
              width={168}
              height={40}
              priority={true}
            />
          </a>
        </div>
      </div>
      <div id="mobileMenuToggle">
        <input
          type="checkbox"
          onChange={() => {
            if (document.body.style.position == "fixed") {
              document.body.style.position = "";
            } else {
              document.body.style.position = "fixed";
            }
          }}
        />
        <span className="first-burger-piece"></span>
        <span className="second-burger-piece"></span>
        <span className="third-burger-piece"></span>

        <div className="mobileMenu" id="menu">
          <div className="menu-section">

            <div className="mobile-memu-item-contain">
              <p className="memu-item-contain-link active-item-parent">
                <a className="item-link-text" href="/lai-suat-gui-tiet-kiem-ngan-hang">Lãi suất tiết kiệm</a>
              </p>
              <div className="sub-item-contain active-sub-item">
                <div className="sub-item-box row" style={{margin: 0}}>
                  {listBank?.data?.length > 0 && listBank.data.map((item, index) => (
                    <div className="col-6 child-item-custom" key={index}>
                      <a href={`/lai-suat-gui-tiet-kiem-${item.slug}`} className="sub-item-contain-link">
                        <span className={`icon-list-bank ${item.slug}`}></span>
                        <span>{item.bankName.replace(/Ngân hàng/g, '')}</span>
                      </a>
                    </div>
                  ))}
                </div>
              </div>
            </div>

            <div className="mobile-memu-item-contain">
              <p className="memu-item-contain-link active-item-parent">
              <a className="item-link-text" href="/lai-suat-vay-ngan-hang">Lãi suất vay</a>
              </p>
              <div className="sub-item-contain active-sub-item">
                <div className="sub-item-box row" style={{margin: 0}}>
                  {dataBorrow?.length > 0 && dataBorrow.map((item, index) => (
                    <div className="col-6 child-item-custom" key={index}>
                      <a href={`/lai-suat-vay-ngan-hang-${item.slug}`} className="sub-item-contain-link">
                        <span className={`icon-list-bank ${item.slug}`}></span>
                        <span>{item.bankName.replace(/Ngân hàng/g, '')}</span>
                      </a>
                    </div>
                  ))}
                </div>
              </div>
            </div>

            <div className="mobile-memu-item-contain">
              <p className="memu-item-contain-link active-item-parent">
                <a className="item-link-text" href="/tien-ao">Tiền ảo</a>
              </p>
              <div className="sub-item-contain active-sub-item">
                <div className="sub-item-box row" style={{margin: 0}}>
                  {listCurrency?.data?.data.length > 0 && listCurrency.data.data.map((item, index) => (
                    <div className="col-6 child-item-custom" key={index}>
                      <a href={`/tien-ao/${item.symbol}`} className="sub-item-contain-link">
                        <span className={`icon-list-bank`} style={{ backgroundImage: `url(https://coinmarketcap.vn/assets/images/coins/16x16/${item.id}.png)`, backgroundSize: 'unset' }}></span>
                        <span>{item.name}</span>
                      </a>
                    </div>
                  ))}
                </div>
              </div>
            </div>

            <div className="mobile-memu-item-contain">
              <p className="memu-item-contain-link active-item-parent">
                <span className="item-link-text">Công cụ</span>
              </p>
              <div className="sub-item-contain active-sub-item">
                <div className="sub-item-box">
                  <a href="/cong-cu-tinh-tien-lai-gui-ngan-hang" className="sub-item-contain-link">Tính lãi tiền gửi ngân hàng</a>
                  <a href="/cong-cu-tinh-toan-khoan-vay-ngan-hang" className="sub-item-contain-link">Tính toán khoản vay ngân hàng</a>
                  <a href="/cong-cu-tinh-ty-gia-ngoai-te" className="sub-item-contain-link">Tính tỷ giá ngoại tệ</a>
                  <a href="/lai-suat-vay-ngan-hang" className="sub-item-contain-link">Tra cứu lãi suất vay</a>
                  <a href="/lai-suat-gui-tiet-kiem-ngan-hang" className="sub-item-contain-link">Tra cứu lãi suất tiết kiệm</a>
                  <a href="/ty-gia-ngoai-te-hoi-doai-ngan-hang-hom-nay" className="sub-item-contain-link">Tra cứu tỷ giá ngoại tệ ngân hàng</a>
                  <a href="/tag/thong-tin-ngan-hang" className="sub-item-contain-link">Thông tin ngân hàng</a>
                </div>
              </div>
            </div>

            <div className="mobile-memu-item-contain">
              <p className="memu-item-contain-link active-item-parent">
                <a className="item-link-text" href="/tag/bao-hiem">BẢO HIỂM</a>
              </p>
              <div className="sub-item-contain active-sub-item">
                <div className="sub-item-box">
                  <p className="sub-item-contain-link">
                    <span>Chuyên mục</span>
                    <span className="material-icons">expand_more</span>
                  </p>
                  <div className="item-child-box">
                    <p className="item-child-box-item">
                      <a href="/tag/bao-hiem-xa-hoi">Bảo hiểm xã hội</a>
                    </p>
                    <p className="item-child-box-item">
                      <a href="/tag/bao-hiem-nhan-tho">Bảo hiểm nhân thọ</a>
                    </p>
                    <p className="item-child-box-item">
                      <a href="/tag/bao-hiem-suc-khoe/">Bảo hiểm sức khỏe</a>
                    </p>
                    <p className="item-child-box-item">
                      <a href="/tag/bao-hiem-thai-san/">Bảo hiểm thai sản</a>
                    </p>
                    <p className="item-child-box-item">
                      <a href="/tag/bao-hiem-y-te/">Bảo hiểm y tế</a>
                    </p>
                    <p className="item-child-box-item">
                      <a href="/blog/bao-hiem-sinh-ky-la-gi/">Bảo hiểm sinh kỳ</a>
                    </p>
                    <p className="item-child-box-item">
                      <a href="/tag/bao-hiem-that-nghiep/">Bảo hiểm thất nghiệp</a>
                    </p>
                    <p className="item-child-box-item">
                      <a href="/tag/bao-hiem-xe-may-o-to/">Bảo hiểm xe máy, ô tô</a>
                    </p>
                    <p className="item-child-box-item">
                      <a href="/tag/bao-hiem-du-lich/">Bảo hiểm du lịch</a>
                    </p>
                    <p className="item-child-box-item">
                      <a href="/tag/bao-hiem-huu-tri/">Bảo hiểm hưu trí</a>
                    </p>
                  </div>
                </div>
                <div className="sub-item-box">
                  <p className="sub-item-contain-link">
                    <span>Bài viết nổi bật</span>
                    <span className="material-icons">expand_more</span>
                  </p>
                  <div className="item-child-box">
                    <p className="item-child-box-item">
                      <a href="/blog/bao-hiem-xa-hoi">Bảo hiểm xã hội là gì</a>
                    </p>
                    <p className="item-child-box-item">
                      <a href="/blog/bao-hiem-nhan-tho">Bảo hiểm nhân thọ là gì</a>
                    </p>
                    <p className="item-child-box-item">
                      <a href="/khong-nho-so-so-bao-hiem-xa-hoi-can-phai-lam-gi">Không nhớ số sổ bảo hiểm xã hội</a>
                    </p>
                    <p className="item-child-box-item">
                      <a href="/blog/30-cau-noi-hay-ve-bao-hiem-nhan-tho-de-khoi-goi-nhu-cau-bao-hiem">Những câu nói hay về bảo hiểm nhân thọ</a>
                    </p>
                    <p className="item-child-box-item">
                      <a href="/blog/bao-hiem-sinh-ky-la-gi">Bảo hiểm sinh kỳ là gì</a>
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}