import React from 'react';
import dynamic from "next/dynamic";

const HeaderLayout = dynamic(() => import("./header"));
const FooterLayout = dynamic(() => import("./footer"));

export interface LayoutProps {
  webSetting: {
    header: {
      menu: string;
    }
  };
  listBank: {
    data: {
      bankName: string;
      idPost: string;
      slug: string;
    }[];
  };
  dataBorrow: {
    bankName: string;
    detail: string;
    idPost: string;
    months: string;
    results: {
      result?: string;
    };
    share?: string;
    slug: string;
  }[];
  listCurrency: {
    data: {
      data: {
        name: string;
        symbol: string;
        slug: string;
        id: number;
      }[];
    }
  };
  children: React.ReactNode;
}

export default function Layout({
  webSetting,
  listBank,
  dataBorrow,
  listCurrency,
  children
}: LayoutProps) {
  return (
    <div className='layout'>
      <HeaderLayout dataHeader={webSetting} listBank={listBank} dataBorrow={dataBorrow} listCurrency={listCurrency}/>
      {/* <ins className="adsbygoogle"
        style={{ }}
        data-ad-client="ca-pub-5581868270899806"
        data-ad-slot="9437674305"
        data-ad-format="horizontal"
        data-full-width-responsive="true"></ins> */}
      {children}
      <FooterLayout />
    </div>
  );
}