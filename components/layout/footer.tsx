import React from 'react';
import Image from 'next/image';
import LazyLoad from 'react-lazyload';

const stringFooter = `<div class="company-infomation-block">
  <div class="company-contact-block">
    <p class="company-contact-title">Công cụ tra cứu</p>
    <ul class="company-contact-menu">
      <li>
        <a href="https://money24h.vn/cong-cu-tinh-tien-lai-gui-ngan-hang">Công cụ tính lãi tiền gửi ngân hàng</a>
      </li>
      <li>
        <a href="https://money24h.vn/cong-cu-tinh-toan-khoan-vay-ngan-hang">Công cụ tính toán khoản vay ngân hàng</a>
      </li>
      <li>
        <a href="https://money24h.vn/cong-cu-tinh-ty-gia-ngoai-te">Công cụ tính tỷ giá ngoại tệ</a>
      </li>
      <li>
        <a href="https://money24h.vn/lai-suat-vay-ngan-hang">Tra cứu lãi suất vay ngân hàng</a>
      </li>
      <li>
        <a href="https://money24h.vn/lai-suat-gui-tiet-kiem-ngan-hang">Tra cứu lãi suất gửi tiết kiệm ngân hàng</a>
      </li>
      <li>
        <a href="https://money24h.vn/ty-gia-ngoai-te-hoi-doai-ngan-hang-hom-nay">Tra cứu tỷ giá ngoại tệ</a>
      </li>
    </ul>
  </div>
  </div>
  <div class="nav-links-block">
  <div>
    <p class="company-contact-title">Hướng dẫn, mẹo</p>
    <ul class="company-contact-menu">
      <li>
        <a href="https://money24h.vn/blog/10-cach-lam-giau-nhanh-nhat-cuc-it-rui-ro-ma-ban-can-biet/">Cách làm giàu</a>
      </li>
      <li>
        <a href="https://money24h.vn/blog/cach-tinh-lai-suat-vay-ngan-hang-nhanh-chong-don-gian-2/">Cách tính lãi suất ngân hàng</a>
      </li>
      <li>
        <a href="https://money24h.vn/huong-dan-cach-lam-the-ngan-hang-chi-tiet-nhanh-chong/">Cách làm thẻ ngân hàng</a>
      </li>
      <li>
        <a href="https://money24h.vn/huong-dan-chuyen-tien-khac-ngan-hang-trong-5-phut/">Cách chuyển khoản</a>
      </li>
      <li>
        <a href="https://money24h.vn/6-cach-kiem-tra-tien-trong-the-atm-trong-30-giay/">Cách kiểm tra tiền trong thẻ ATM</a>
      </li>
      <li>
        <a href="https://money24h.vn/blog/cac-cach-thanh-toan-tien-dien-nhanh-chong/">Thanh toán tiền điện</a>
      </li>
      <li>
        <a href="https://money24h.vn/3-cach-chuyen-tien-dien-thoai-nhanh-chong/">Nạp tiền điện thoại</a>
      </li>
    </ul>
  </div>
  </div>
  <div class="customers-support-block" style="align-items: flex-end">
  <div>
    <p class="company-contact-title">Về chúng tôi</p>
    <ul class="company-contact-menu">
      <li>
        <a href="#">Giới thiệu</a>
      </li>
      <li>
        <a href="#">Sơ đồ Website</a>
      </li>
      <li>
        <a href="#">Chính sách bảo mật</a>
      </li>
    </ul>
  </div>
</div>`;

export default function FooterLayout() {
  return (
    <>
      <footer className="footer-contain">
        <LazyLoad className="footer-under-central-contain">
          <div className="image-logo-footer">
            <div style={{ maxWidth: '248px' }}>
              <a href="/">
                <Image
                  className="logo-footer-img"
                  alt="logo-footer-image"
                  src="/logos/logo-footer.png"
                  loading="lazy"
                  layout="intrinsic"
                  width={248}
                  height={56}
                />
              </a>
            </div>
            <div className="icon-about-us">
              <div className="lazyload-wrapper">
                <a href="https://twitter.com/Money24H1" target="_blank" rel="noreferrer">
                  <Image
                    alt="logo-footer-image"
                    src="/images/twitter-footer.png"
                    loading="lazy"
                    layout="intrinsic"
                    width={32}
                    height={32}
                  />
                </a>
              </div>
              <div>
                <a
                  href="https://www.facebook.com/money24H.vn/"
                  target="_blank"
                  rel="noreferrer"
                >
                  <Image
                    alt="logo-footer-image"
                    src="/logos/facebook-logo.png"
                    loading="lazy"
                    layout="intrinsic"
                    width={32}
                    height={32}
                  />
                </a>
              </div>
              <div>
                <a
                  href="https://www.youtube.com/channel/UCSpa8iwEcpH0LdR0LKZISXA"
                  target="_blank"
                  rel="noreferrer"
                >
                  <Image
                    alt="logo-footer-image"
                    src="/logos/youtube-logo.png"
                    loading="lazy"
                    layout="intrinsic"
                    width={32}
                    height={32}
                  />
                </a>
              </div>
            </div>
          </div>
          <div
            className="box-footer-cms"
            dangerouslySetInnerHTML={{ __html: stringFooter }}
          ></div>
        </LazyLoad>
      </footer>
      <div className="copyright">
        <span>
          Copyright © 2021, Money24H. All Rights Reserved Power by{' '}
          <a href="https://www.toponseek.com/">TopOnSeek</a>
        </span>
      </div>
    </>
  );
}