import React from 'react';

export interface HeaderDesktopProps {
  dataHeader: string;
  listBank: {
    data: {
      bankName: string;
      idPost: string;
      slug: string;
    }[];
  };
  dataBorrow: {
    bankName: string;
    detail: string;
    idPost: string;
    months: string;
    results: {
      result?: string;
    };
    share?: string;
    slug: string;
  }[];
  listCurrency: {
    data: {
      data: {
        name: string;
        symbol: string;
        slug: string;
        id: number;
      }[];
    }
  };
}

export default function HeaderDesktop ({ 
  dataHeader, 
  listBank, 
  dataBorrow, 
  listCurrency 
}: HeaderDesktopProps) {

  const generateInterestRate = (dataInterestRate) => {
    let prefix = Math.ceil(dataInterestRate.length / 14)

    const renderListData = () => {
      let HTML_List = []
      for(let i = 0; i < prefix; i++) {
        HTML_List.push(
          <li className="menu-parent-item" key={i}>
            <ul className="menu-parent-item-container">
              {dataInterestRate.slice(i * 14, ((i + 1) * 14)).map((item, index) => (
                <li key={index}>
                  <a href={`/lai-suat-gui-tiet-kiem-${item.slug}`}>
                    <span className={`icon-list-bank ${item.slug}`}></span>
                    <span>{item.bankName.replace(/Ngân hàng/g, '')}</span>
                  </a>
                </li>
              ))}
            </ul>
          </li>
        )
      }
      return HTML_List
    }

    return (
      <ul className="list-menu-parent-container">
        {renderListData()}
      </ul>
    )
  }


  const generateLoanRate = (dataInterestRate) => {
    let prefix = Math.ceil(dataInterestRate.length / 14)

    const renderListData = () => {
      let HTML_List = []
      for(let i = 0; i < prefix; i++) {
        HTML_List.push(
          <li className="menu-parent-item" key={i}>
            <ul className="menu-parent-item-container">
              {dataInterestRate.slice(i * 14, ((i + 1) * 14)).map((item, index) => (
                <li key={index}>
                  <a href={`/lai-suat-vay-ngan-hang-${item.slug}`}>
                    <span className={`icon-list-bank ${item.slug}`}></span>
                    <span>{item.bankName.replace(/Ngân hàng/g, '')}</span>
                  </a>
                </li>
              ))}
            </ul>
          </li>
        )
      }
      return HTML_List
    }

    return (
      <ul className="list-menu-parent-container">
        {renderListData()}
      </ul>
    )
  }

  const generateCoinList = (dataCoin) => {
    if(dataCoin && dataCoin.data) {
      let prefix = Math.ceil(dataCoin.data.length / 14)

      const renderListData = () => {
        let HTML_List = []
        for(let i = 0; i < prefix; i++) {
          HTML_List.push(
            <li className="menu-parent-item" key={i}>
              <ul className="menu-parent-item-container">
                {dataCoin.data.slice(i * 14, ((i + 1) * 14)).map((item, index) => (
                  <li key={index}>
                    <a href={`/tien-ao/${item.symbol}`}>
                      <span className={`icon-list-bank`} style={{ backgroundImage: `url(https://coinmarketcap.vn/assets/images/coins/16x16/${item.id}.png)`, backgroundSize: 'unset' }}></span>
                      <span>{item.name}</span>
                    </a>
                  </li>
                ))}
              </ul>
            </li>
          )
        }
        return HTML_List
      }

      return (
        <ul className="list-menu-parent-container">
          {renderListData()}
        </ul>
      )
    }
  } 

  const renderCustomMenu = () => {
    return stringHeader
  }

  // generateCoinList(listCurrency.data)

  const stringHeader = `<div class="dropdown-nav-toggle">
  <a href="/tag/bao-hiem" class="menu-link-item">
    <span class="item-link-text">Bảo hiểm</span>
    <span class="caret-custom"></span>
  </a>
</div>
<div class="dropdown-nav-menu bottom navmenu-link-item-dropdown">
  <div class="box-nav-menu-link">
    <div class="box-menu-title">
      <p class="nav-menu-title">Chuyên mục</p>
    </div>
    <div class="dropdown-nav-item">
      <a href="/tag/bao-hiem-xa-hoi" class="navmenu-link-item-dropdown-item">
        <span class="dropdown-item-text">Bảo hiểm xã hội</span>
      </a>
    </div>
    <div class="dropdown-nav-item">
      <a href="/tag/bao-hiem-nhan-tho" class="navmenu-link-item-dropdown-item">
        <span class="dropdown-item-text">Bảo hiểm nhân thọ</span>
      </a>
    </div>
    <div class="dropdown-nav-item">
      <a href="/tag/bao-hiem-suc-khoe/" class="navmenu-link-item-dropdown-item">
        <span class="dropdown-item-text">Bảo hiểm sức khỏe</span>
      </a>
    </div>
    <div class="dropdown-nav-item">
      <a href="/tag/bao-hiem-thai-san/" class="navmenu-link-item-dropdown-item">
        <span class="dropdown-item-text">Bảo hiểm thai sản</span>
      </a>
    </div>
    <div class="dropdown-nav-item">
      <a href="/tag/bao-hiem-y-te/" class="navmenu-link-item-dropdown-item">
        <span class="dropdown-item-text">Bảo hiểm y tế</span>
      </a>
    </div>
    <div class="dropdown-nav-item">
      <a href="/blog/bao-hiem-sinh-ky-la-gi/" class="navmenu-link-item-dropdown-item">
        <span class="dropdown-item-text">Bảo hiểm sinh kỳ</span>
      </a>
    </div>
    <div class="dropdown-nav-item">
      <a href="/tag/bao-hiem-that-nghiep/" class="navmenu-link-item-dropdown-item">
        <span class="dropdown-item-text">Bảo hiểm thất nghiệp</span>
      </a>
    </div>
    <div class="dropdown-nav-item">
      <a href="/tag/bao-hiem-xe-may-o-to/" class="navmenu-link-item-dropdown-item">
        <span class="dropdown-item-text">Bảo hiểm xe máy, ô tô</span>
      </a>
    </div>
    <div class="dropdown-nav-item">
      <a href="/tag/bao-hiem-du-lich/" class="navmenu-link-item-dropdown-item">
        <span class="dropdown-item-text">Bảo hiểm du lịch</span>
      </a>
    </div>
    <div class="dropdown-nav-item">
      <a href="/tag/bao-hiem-huu-tri/" class="navmenu-link-item-dropdown-item">
        <span class="dropdown-item-text">Bảo hiểm hưu trí</span>
      </a>
    </div>
  </div>
  <div class="box-nav-menu-link" style='min-width: 285px'>
    <div class="box-menu-title">
      <p class="nav-menu-title">Bài viết nổi bật</p>
    </div>
    <div class="dropdown-nav-item">
      <a href="/blog/bao-hiem-xa-hoi" class="navmenu-link-item-dropdown-item">
        <span class="dropdown-item-text">Bảo hiểm xã hội là gì</span>
      </a>
    </div>
    <div class="dropdown-nav-item">
      <a href="/blog/bao-hiem-nhan-tho" class="navmenu-link-item-dropdown-item">
        <span class="dropdown-item-text">Bảo hiểm nhân thọ là gì</span>
      </a>
    </div>
    <div class="dropdown-nav-item">
      <a href="/blog/30-cau-noi-hay-ve-bao-hiem-nhan-tho-de-khoi-goi-nhu-cau-bao-hiem" class="navmenu-link-item-dropdown-item">
        <span class="dropdown-item-text">Những câu nói hay về bảo hiểm nhân thọ</span>
      </a>
    </div>
    <div class="dropdown-nav-item">
      <a href="/blog/khong-nho-so-so-bao-hiem-xa-hoi-can-phai-lam-gi" class="navmenu-link-item-dropdown-item">
        <span class="dropdown-item-text">Không nhớ số sổ bảo hiểm xã hội</span>
      </a>
    </div>
    <div class="dropdown-nav-item">
      <a href="/blog/bao-hiem-sinh-ky-la-gi" class="navmenu-link-item-dropdown-item">
        <span class="dropdown-item-text">Bảo hiểm sinh kỳ là gì</span>
      </a>
    </div>
  </div>
</div>`

  return (
    
    // <div className="header-desktop">
    //   <div className="menu-section" dangerouslySetInnerHTML={{ __html: dataHeader}}></div>
    // </div>
    <div className="header-desktop">
      <div className="menu-section">
        <div className="menu-link-contain">
          <div className="dropdown-nav">
            <div className="dropdown-nav-toggle">
              <a href="/lai-suat-gui-tiet-kiem-ngan-hang" className="menu-link-item">
                <span className="item-link-text">Lãi suất tiết kiệm</span>
                <span className="caret-custom"></span>
              </a>
            </div>
            <div className="dropdown-nav-menu bottom navmenu-link-item-dropdown">
              {listBank?.data?.length > 0 && generateInterestRate(listBank.data)}
            </div>
          </div>

          <div className="dropdown-nav">
            <div className="dropdown-nav-toggle">
              <a href="/lai-suat-vay-ngan-hang" className="menu-link-item">
                <span className="item-link-text">Lãi suất vay</span>
                <span className="caret-custom"></span>
              </a>
            </div>
            <div className="dropdown-nav-menu bottom navmenu-link-item-dropdown">
              {dataBorrow?.length > 0 && generateLoanRate(dataBorrow)}
            </div>
          </div>

          <div className="dropdown-nav">
            <div className="dropdown-nav-toggle">
              <a href="/tien-ao" className="menu-link-item">
                <span className="item-link-text">Tiền ảo</span>
                <span className="caret-custom"></span>
              </a>
            </div>
            <div className="dropdown-nav-menu bottom navmenu-link-item-dropdown">
              {listCurrency?.data?.data?.length > 0 && generateCoinList(listCurrency.data)}
            </div>
          </div>

          <div className="dropdown-nav">
            <div className="dropdown-nav-toggle">
              <p className="menu-link-item">
                <span className="item-link-text">Công cụ</span>
                <span className="caret-custom"></span>
              </p>
            </div>
            <div className="dropdown-nav-menu bottom navmenu-link-item-dropdown">
              <div className="box-nav-menu-link">
                <div className="dropdown-nav-item">
                  <a href="/cong-cu-tinh-tien-lai-gui-ngan-hang" className="navmenu-link-item-dropdown-item">
                    <span className="dropdown-item-text">Tính lãi tiền gửi ngân hàng</span>
                  </a>
                </div>
                <div className="dropdown-nav-item">
                  <a href="/cong-cu-tinh-toan-khoan-vay-ngan-hang" className="navmenu-link-item-dropdown-item">
                    <span className="dropdown-item-text">Tính toán khoản vay ngân hàng</span>
                  </a>
                </div>
                <div className="dropdown-nav-item">
                  <a href="/cong-cu-tinh-ty-gia-ngoai-te" className="navmenu-link-item-dropdown-item">
                    <span className="dropdown-item-text">Tính tỷ giá ngoại tệ</span>
                  </a>
                </div>
                <div className="dropdown-nav-item">
                  <a href="/lai-suat-vay-ngan-hang" className="navmenu-link-item-dropdown-item">
                    <span className="dropdown-item-text">Tra cứu lãi suất vay</span>
                  </a>
                </div>
                <div className="dropdown-nav-item">
                  <a href="/lai-suat-gui-tiet-kiem-ngan-hang" className="navmenu-link-item-dropdown-item">
                    <span className="dropdown-item-text">Tra cứu lãi suất tiết kiệm</span>
                  </a>
                </div>
                <div className="dropdown-nav-item">
                  <a href="/ty-gia-ngoai-te-hoi-doai-ngan-hang-hom-nay" className="navmenu-link-item-dropdown-item">
                    <span className="dropdown-item-text">Tra cứu tỷ giá ngoại tệ ngân hàng</span>
                  </a>
                </div>
                <div className="dropdown-nav-item">
                  <a href="/tag/thong-tin-ngan-hang" className="navmenu-link-item-dropdown-item">
                    <span className="dropdown-item-text">Thông tin ngân hàng</span>
                  </a>
                </div>
              </div>
            </div>
          </div>

          <div className="dropdown-nav" dangerouslySetInnerHTML={{ __html: renderCustomMenu()}}>
          </div>
        </div>
      </div>
    </div>
  )
}