import React from 'react';
import Image from 'next/image';
import dynamic from "next/dynamic";

const HeaderDesktop = dynamic(() => import("./header-desktop"));
const HeaderMobile = dynamic(() => import("./header-mobile"));

export interface HeaderLayoutProps {
  dataHeader: {
    header: {
      menu: string;
    }
  };
  listBank: {
    data: {
      bankName: string;
      idPost: string;
      slug: string;
    }[];
  };
  dataBorrow: {
    bankName: string;
    detail: string;
    idPost: string;
    months: string;
    results: {
      result?: string;
    };
    share?: string;
    slug: string;
  }[];
  listCurrency: {
    data: {
      data: {
        name: string;
        symbol: string;
        slug: string;
        id: number;
      }[];
    }
  };
}

export default function HeaderLayout({
  dataHeader,
  listBank,
  dataBorrow,
  listCurrency
}: HeaderLayoutProps) {

  return (
    <>
      <div className="block-header-logo-theme">
        <div style={{ marginRight: "24px" }}>
          <a href={"/"}>
            <Image
              alt="logo-header"
              src="/logos/header-logo.png"
              layout="fixed"
              width={150}
              height={35}
              priority={true}
            />
          </a>
        </div>
        <div className="header-contain"
          style={{ margin: 0, border: "none" }}
         >
          <HeaderDesktop dataHeader={dataHeader.header.menu} listBank={listBank} dataBorrow={dataBorrow} listCurrency={listCurrency}/>
        </div>
      </div>
      <div className="header-contain" style={{ height: "auto", borderTop: "none" }}>
        <HeaderMobile dataHeader={dataHeader.header.menu} listBank={listBank} dataBorrow={dataBorrow} listCurrency={listCurrency}/>
      </div>
    </>
  )
}