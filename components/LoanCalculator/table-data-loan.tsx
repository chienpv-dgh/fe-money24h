import React, { Component } from 'react';
import { Table } from 'reactstrap';
import { addDays } from "date-fns";

interface IRecipeProps {
  dataTableLoan?: {
    loanRemain: number;
    loanOrigin: number;
    interest: number;
    totalMoney: number;
    dateLoan: Date;
  }[];
  totalOriginal?: number;
  totalInterest?: number;
  totalMoney?: number;
}
interface IRecipeState {}


class TableDataLoan extends Component<IRecipeProps,IRecipeState> {

  numberWithCommas = (number) => {
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  }

  converDate = (date, index) => {
    
    let count = Number(30 * index)

    let getDateCustom = new Date(addDays(new Date(date), count))

    let getDate = new Date(getDateCustom).getDate()
    let getMonth = new Date(getDateCustom).getMonth() + 1
    let getFullYear = new Date(getDateCustom).getFullYear()

    return `${getDate < 10 ? `0${getDate}` : getDate}-${getMonth < 10 ? `0${getMonth}` : getMonth}-${getFullYear}`
  }

  render() {
    return (
      <div className="table-data-loan">
        <div className="custom-table-loan">
          <Table responsive>
            <thead>
              <tr>
                <th>STT</th>
                <th>Kỳ trả nợ</th>
                <th>Số gốc còn lại</th>
                <th>Gốc</th>
                <th>Lãi</th>
                <th>Tổng tiền</th>
              </tr>
            </thead>
            <tbody>
              {this.props.dataTableLoan ?
                this.props.dataTableLoan.map((item, index) => (
                  <tr key={index}>
                    <td>{index}</td>
                    <td>{this.converDate(item.dateLoan, index)}</td>
                    <td>{item.loanRemain !== null ? this.numberWithCommas(item.loanRemain) : "-"}</td>
                    <td>{item.loanOrigin !== null ? this.numberWithCommas(item.loanOrigin) : "-"}</td>
                    <td>{item.interest !== null ? this.numberWithCommas(item.interest) : "-"}</td>
                    <td>{item.totalMoney !== null ? this.numberWithCommas(item.totalMoney) : "-"}</td>
                  </tr>
                ))
                :
                null
              }
              <tr>
                <td colSpan={3}>TỔNG CỘNG</td>
                <td>{this.numberWithCommas(this.props.totalOriginal)}</td>
                <td>{this.numberWithCommas(this.props.totalInterest)}</td>
                <td>{this.numberWithCommas(this.props.totalMoney)}</td>
              </tr>
            </tbody>
          </Table>
        </div>
      </div>
    );
  }
}

export default TableDataLoan;