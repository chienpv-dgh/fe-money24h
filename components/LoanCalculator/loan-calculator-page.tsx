import React, { Component } from 'react';
import dynamic from "next/dynamic";
import { TableDataLoan, ToolCalculatorLoan } from '.';
import { ContentPageSaving } from '../SavingInterest';

const ArticleInterested = dynamic(() => import('../widgets/ArticleInterested/article-interested'));
const GlobalToolBox = dynamic(() => import("../widgets/GlobalToolBox"));
export interface quoteObject{
  fully_diluted_market_cap: number;
  last_updated: string;
  market_cap: number;
  market_cap_dominance: number;
  percent_change_1h: number;
  percent_change_7d: number;
  percent_change_24h: number;
  percent_change_30d: number;
  percent_change_60d: number;
  percent_change_90d: number;
  price: number;
  volume_24h: number;
  volume_change_24h: number;
}
interface IRecipeProps {
  AllPosts?: any;
  postOfPage?: any;
  dataCrypto: {
    status: {
        timestamp: string;
        total_count: number;
    };
    data: {
      circulating_supply: number;
      cmc_rank: number;
      id: number;
      last_updated: string;
      max_supply: number;
      total_supply: number;
      name: string;
      num_market_pairs: number;
      slug: string;
      symbol: string;
      quote: quoteObject[];
    }[];
  };
}
interface IRecipeState {
  dataTableLoan?: {
    loanRemain: number;
    loanOrigin: number;
    interest: number;
    totalMoney: number;
    dateLoan: Date;
  }[];
  totalOriginal?: number;
  totalInterest?: number;
  totalMoney?: number;
}

class LoanCalculatorPage extends Component<IRecipeProps,IRecipeState> {
  constructor(props) {
    super(props);
    this.state = {
      dataTableLoan: null,
      totalOriginal: null,
      totalInterest: null,
      totalMoney: null,
    }
  }
  
  getDataTableFromChild = (data, totalOriginal, totalInterest, totalMoney) => {
    if(data) {
      this.setState({dataTableLoan: data, totalOriginal: totalOriginal, totalInterest: totalInterest, totalMoney: totalMoney})
    }
  }

  render() {
   
    return (
      <div className="loan-calculator-page-container">
        <ToolCalculatorLoan sendDataToMap={(data, totalOriginal, totalInterest, totalMoney) => this.getDataTableFromChild(data, totalOriginal, totalInterest, totalMoney)}/>
        {this.state.dataTableLoan && <TableDataLoan 
          dataTableLoan={this.state.dataTableLoan} 
          totalOriginal={this.state.totalOriginal}
          totalInterest={this.state.totalInterest}
          totalMoney={this.state.totalMoney}
        />}
        <GlobalToolBox />
        <div className="divider-saving-interest-page"></div>
        <ContentPageSaving showAuthor={false} AllPosts={this.props.AllPosts} postOfPage={this.props.postOfPage} dataCrypto={this.props.dataCrypto}/>
        <div className="block-interested-article">
          <p className="interested-article-box-highlight">CÓ THỂ BẠN QUAN TÂM</p>
          <div className="divider-box-highlight"></div>
          <ArticleInterested AllPosts={this.props.AllPosts}/>
        </div>
      </div>
    );
  }
}

export default LoanCalculatorPage;