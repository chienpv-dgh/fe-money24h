import LoanCalculatorPage from "./loan-calculator-page";
import ToolCalculatorLoan from "./tool-calculator-loan";
import TableDataLoan from "./table-data-loan";

export {
  LoanCalculatorPage,
  ToolCalculatorLoan,
  TableDataLoan
}