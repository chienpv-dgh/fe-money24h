import React, { Component } from 'react';
import NumberFormat from 'react-number-format';
import DatePicker from "react-datepicker";
import { addDays } from "date-fns";
import "react-datepicker/dist/react-datepicker.css";

interface IRecipeProps {
  sendDataToMap?: Function;
}
interface IRecipeState {
  type?: string;
  loan?: number;
  startDate?: Date;
  loanInterest?: number;
  loanTerm?: number;
  dataTableLoan?: {
    loanRemain: number;
    loanOrigin: number;
    interest: number;
    totalMoney: number;
  }[];
  totalMoney?: number;
  totalInterest?: number;
  totalLoanOrigin?: number;
}

class ToolCalculatorLoan extends Component<IRecipeProps,IRecipeState> {
  constructor(props) {
    super(props);
    this.state = {
      type: 'original',
      loan: null,
      startDate: new Date(),
      loanInterest: null,
      loanTerm: null,
      dataTableLoan: null,
      totalMoney: null,
      totalInterest: null,
      totalLoanOrigin: null
    };
  }
  
  onRadioChange = (e) => {
    this.setState({
      type: e.target.value,
    });
  }

  numberWithCommas = (number) => {
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  }

  calcLoan = () => {
    let loan = this.state.loan
    let loanInterest = this.state.loanInterest
    let loanTerm = this.state.loanTerm

    let errorLoan = ""
    let errorLoanInterest = ""
    let errorLoanTerm = ""

    if(loan) {
      errorLoan = ""
    } else {
      errorLoan = "Error"
    }

    if(loanInterest) {
      errorLoanInterest = ""
    } else {
      errorLoanInterest = "Error"
    }

    if(loanTerm) {
      errorLoanTerm = ""
    } else {
      errorLoanTerm = "Error"
    }

    if(errorLoan || errorLoanInterest || errorLoanTerm) {
      return
    } else {

      if(this.state.type == "original") {
        
        let principalMonthly = Math.round(this.state.loan / this.state.loanTerm)
        
        let count = this.state.loanTerm + 1
       
        let arr = []
        let totalMoney = 0
        let totalInterest = 0
        let totalLoanOrigin = 0
    
        for(let i = 0; i < count; i++) {
          if (i == 0) {
            let data = {
              loanRemain: null,
              loanOrigin: null,
              interest: null,
              totalMoney: null,
              dateLoan: this.state.startDate,
            }
            arr.push(data)
          } else {
            let data = { 
              loanRemain: Math.round(this.state.loan - (principalMonthly * i)),
              loanOrigin: Math.round(principalMonthly),
              interest: Math.round((this.state.loan - (principalMonthly * (i - 1))) * ( (this.state.loanInterest / 100) / 12)),
              totalMoney: Math.round(principalMonthly + ((this.state.loan - (principalMonthly * (i - 1))) * ( (this.state.loanInterest / 100) / 12))),
              dateLoan: this.state.startDate
            }
            totalMoney = totalMoney + data.totalMoney
            totalInterest = totalInterest + data.interest
            totalLoanOrigin = totalLoanOrigin + data.loanOrigin
            arr.push(data)
          }
        }
        
        this.setState({
          dataTableLoan: arr,
          totalMoney: totalMoney,
          totalInterest: totalInterest,
          totalLoanOrigin: totalLoanOrigin
        })
      } else {
       
        let principalMonthly = Math.round(this.state.loan / this.state.loanTerm)
        
        let count = this.state.loanTerm + 1
        
        let arr = []
        let totalMoney = 0
        let totalInterest = 0
        let totalLoanOrigin = 0

        for(let i = 0; i < count; i++) {
          if (i == 0) {
            let data = {
              loanRemain: null,
              loanOrigin: null,
              interest: null,
              totalMoney: null,
              dateLoan: this.state.startDate,
            }
            arr.push(data)
          } else {
            let data = { 
              loanRemain: Math.round(this.state.loan - (principalMonthly * i)),
              loanOrigin: Math.round(principalMonthly),
              interest: Math.round(this.state.loan * ( (this.state.loanInterest / 100) / 12)),
              totalMoney: Math.round(principalMonthly + (this.state.loan * ( (this.state.loanInterest / 100) / 12))),
              dateLoan: this.state.startDate,
            }
            totalMoney = totalMoney + data.totalMoney
            totalInterest = totalInterest + data.interest
            totalLoanOrigin = totalLoanOrigin + data.loanOrigin
            arr.push(data)
          }
        }
        
        this.setState({
          dataTableLoan: arr,
          totalMoney: totalMoney,
          totalInterest: totalInterest,
          totalLoanOrigin: totalLoanOrigin
        })
      }
    }
  }

  getData = async () => {
    await this.calcLoan()
    if(this.state.dataTableLoan && this.state.totalLoanOrigin && this.state.totalInterest && this.state.totalMoney){
      this.props.sendDataToMap(this.state.dataTableLoan, this.state.totalLoanOrigin, this.state.totalInterest, this.state.totalMoney)
    } else {
      return
    }
  }

  render() {
    return (
      <div className="block-tool-calc-loan">
        <div className="contain-calc-tool">
          <h1 className="calc-tool-main-title">CÔNG CỤ TÍNH TOÁN KHOẢN VAY NGÂN HÀNG</h1>
          <div className="check-radio-value row">
              <div className="check-radio-value-item col-12 col-lg-6">
                <input value="original" type="radio" id="original" name="original" checked={this.state.type == "original"}
                  onChange={this.onRadioChange}/>
                <label htmlFor="original" className="label-radio-custom">Trả trên dư nợ giảm dần</label>
              </div>
              <div className="check-radio-value-item col-12 col-lg-6">
                <input value="decrease" type="radio" id="decrease" name="decrease" checked={this.state.type == "decrease"}
                  onChange={this.onRadioChange}/>
                <label htmlFor="decrease" className="label-radio-custom">Trả trên dư nợ ban đầu</label>
              </div>
          </div>
          <div className="block-calc-content row">
            <div className="block-calc-content-left col-12 col-lg-5">
              <div className="calc-content-left-item">
                <p className="content-left-title">Số tiền vay *</p>
                <NumberFormat className="custom-input-calc-left" value={this.state.loan} thousandSeparator={"."} decimalSeparator={","}
                  onValueChange={(values) => {
                    const {formattedValue, value} = values;
                    this.setState({loan: Number(value)})
                  }}
                />
                <span className="icon-end-input"><img src="/images/icon-viet-nam-dong.svg" alt=""/></span>
              </div>
              <div className="calc-content-left-item">
                <p className="content-left-title">Lãi suất vay *</p>
                <NumberFormat className="custom-input-calc-left" value={this.state.loanInterest} thousandSeparator={"."} decimalSeparator={","}
                  onValueChange={(values) => {
                    const {formattedValue, value} = values;
                    this.setState({loanInterest: Number(value)})
                  }}
                />
                <span className="icon-end-input">%/Năm</span>
              </div>
              <div className="calc-content-left-item">
                <p className="content-left-title">Thời hạn vay *</p>
                <NumberFormat className="custom-input-calc-left" value={this.state.loanTerm} thousandSeparator={"."} decimalSeparator={","}
                  onValueChange={(values) => {
                    const {formattedValue, value} = values;
                    this.setState({loanTerm: Number(value)})
                  }}
                />
                <span className="icon-end-input">Tháng</span>
              </div>
              <div className="calc-content-left-item">
                <p className="content-left-title">Ngày giải ngân *</p>
                <DatePicker dateFormat="dd/MM/yyyy" minDate={new Date(addDays(new Date(), 1))} selected={this.state.startDate} onChange={date => this.setState({startDate: date})} className="custom-input-calc-left"/>
                <span className="icon-end-input"><img src="/images/calendar-icon.svg" alt="calendar-icon"/></span>
              </div>
              <p className="description-text">(*): Thông tin bắt buộc</p>
              <p className="view-result" onClick={() => this.getData()}>XEM KẾT QUẢ</p>
            </div>
            <div className="block-calc-content-right col-12 col-lg-5">
              <div className="calc-tool-box">
                <p className="calc-tool-box-tile">Số tiền lãi phải trả</p>
                <div className="calc-tool-box-value">
                  <span>{this.numberWithCommas(this.state.totalInterest ? this.state.totalInterest : 0)}</span>
                  <img src="/images/icon-unit-blue.svg" alt="icon-unit-blue"/>
                </div>
                <p className="calc-tool-box-tile">Tổng số gốc và lãi phải trả</p>
                <div className="calc-tool-box-value">
                  <span>{this.numberWithCommas(this.state.totalMoney ? this.state.totalMoney : 0)}</span>
                  <img src="/images/icon-unit-blue.svg" alt="icon-unit-blue"/>
                </div>
              </div>
              <div className="view-current-interest-rate">
                <div className="border-box">
                  <p className="border-box-title">XEM LÃI SUẤT HIỆN TẠI</p>
                  <img className="border-box-image" src="/images/icon-arow-blue.svg" alt="icon-arow-blue"/>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ToolCalculatorLoan;