import React from 'react';
import { Spinner } from 'reactstrap';
import dynamic from "next/dynamic";
import LazyLoad from 'react-lazyload';

// const StockHome = dynamic(() => import("../modules/HomePage/stock-home"), { ssr: false });
const VietlotData = dynamic(() => import("../widgets/DataVietlot"));
const WidgetDataByBank = dynamic(() => import("../widgets/DataByBank"));

interface IRecipeProps {
  listBankOps?: any;
  tableVietlot?: any;
  dataInterest?: any;
  tagPage?: string;
  dataPost?: any;
}

interface IRecipeState {
  listBankOps?: any;
  allPosts?: any;
  page?: number;
  loading?: boolean;
  size?: number;
}
export default class InfoFinanceComponent extends React.Component<IRecipeProps, IRecipeState> {

  constructor(props) {
    super(props);
    this.state = {
      listBankOps: null,
      allPosts: this.props.dataPost ? this.props.dataPost : null,
      page: 1,
      size: 20,
      loading: false,
    }
  }

  checkRenderTimeDateText = (date) => {
    let receiveDate = new Date(date)
    let year = receiveDate.getFullYear()
    let month = receiveDate.getMonth() + 1
    let day = receiveDate.getDate()
    let result = `${day} THÁNG ${month} ${year}`

    return result
  }

  fetchMoreData = () => {
    let page = this.state.page
    let size = this.state.size
    let newPage = page + 1;
    let newSize = size + 20;
    this.setState({
      page: newPage,
      size: newSize
    });
    this.getDataPost(newPage, newSize)
  }

  getDataPost = async (size, offset) => {
    this.setState({ loading: true })
    let res = await fetch(`https://admin.money24h.vn/wp-json/totapi/posts/get_newest_post?key=4746a0cc735cb3ea921a01ffb1e5c257&posts_per_page=20&page=${1}&offset=${offset}`)
    let dataRes = await res.json()
    if (dataRes) {
      let data = [...this.state.allPosts].concat(dataRes.posts)
      await this.setState({ allPosts: data, loading: false })
    }
  }

  async componentDidMount() {
  }
  
  render() {
    // console.log("====.", this.state.allPosts)

    return (
      <div className="tag-page-container">
        <p className="bread-scrum-post-page">
          <a href="/">Trang chủ</a>
          <i className="fa fa-angle-right bread-scrum-divider" aria-hidden="true"></i>
          <span>{"Thông tin tài chính mới nhất"}</span>
        </p>
        <div className="row">
          <div className="block-detail-left col-12 col-lg-8">
            {this.props.dataPost && this.props.dataPost.length > 0 ? <div className="block-post-by-tag row">
              <div className="block-post-by-tag-item col-12">
                <h1 className="title-detail-tag">Thông tin tài chính mới nhất</h1>
                
                <div className="divide-title-block"></div>
                <div className="block-post-data-item">
                  {this.state.allPosts && this.state.allPosts.length > 0 && this.state.allPosts.map((item, index) =>
                    <a key={index} href={`/blog/${item.slug}`}>
                      <div className="block-post-data-item-detail">
                        <LazyLoad once={true} placeholder={"Loading..."} className="block-post-data-item-contain-image">
                          <img className="block-post-data-item-image" src={item.feature_image} alt="image-post-by-tag" width="100%" height="100%" style={{ objectFit: "cover" }} />
                        </LazyLoad>
                        <div className="block-post-data-item-content">
                          <p className="block-post-data-item-content-title" dangerouslySetInnerHTML={{__html: item.title}}></p>
                          <p className="block-post-data-item-content-date">{item.publish_date}</p>
                          <p className="block-post-data-item-content-excerpt" dangerouslySetInnerHTML={{__html: item.excerpt}}></p>
                        </div>
                      </div>
                    </a>
                  )}
                </div>
                {this.props.dataPost.length >= 20 ? this.state.loading ? <div className="spinner">
                  <Spinner color="primary" />
                </div> : <div className="box-view-more">
                  <p className="button-view-more" onClick={() => this.fetchMoreData()}>
                    View more
                  </p>
                </div> : null}
              </div>
            </div> : null}
          </div>
          <div className="block-body-right col-12 col-lg-4">
            <WidgetDataByBank dataInterest={this.props.dataInterest} />
            {/* <StockHome /> */}
            <VietlotData tableVietlot={this.props.tableVietlot} />
          </div>
        </div>
      </div>
    );
  }
}