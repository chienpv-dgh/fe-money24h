import React, { useEffect, useState } from 'react';
import { Spinner } from 'reactstrap';

export interface BorrowDataWidgetProps {
  dataBorrow: {
    bankName: string;
    detail: string;
    idPost: string;
    months: string;
    results: {
      result?: string;
    };
    share?: string;
    slug: string;
  }[];
}

export interface BorrowDataProps {
  bankName: string;
  detail: string;
  idPost: string;
  months: string;
  results: {
    result?: string;
  };
  share?: string;
  slug: string;
}

export default function WidgetBorrowData({ dataBorrow }: BorrowDataWidgetProps) {
  const [sortDataborrow, setSortDataborrow] = useState<BorrowDataProps[]>([]);

  useEffect(() => {
    const handleSortBorrow = async () => {
      let data = [...dataBorrow];
      let itemSort = [];
      itemSort = [...data].sort((b, a) => Number(b.results.result) - Number(a.results.result));
      setSortDataborrow(itemSort);
    };
    if (dataBorrow) {
      handleSortBorrow();
    }
  }, [dataBorrow]);

  return (
    <div className="interest-bank-block borow-rate">
      <h3 className="interest-bank-highlight">Lãi suất vay</h3>
      <div className="divide-box-right"></div>
      <div className="interest-box-data">
        <div className="box-description-interrest">
          <span className="smail-text">(Từ thấp đến cao)</span>
        </div>
      </div>
      <div className="box-table-data-borrow-bank">
        <div className="box-table">
          <table className="row-month">
            <tbody>
              <tr>
                <td>Ngân hàng</td>
                <td>
                  <div>
                    <span>Lãi suất</span>
                  </div>
                </td>
                <td>Thời gian ưu đãi</td>
              </tr>
            </tbody>
          </table>
        </div>
        <div className="data-interest-by-bank customScroll">
          <table className="data-interest-table">
            <tbody>
              {dataBorrow ? (
                sortDataborrow &&
                sortDataborrow.map((item, index) => (
                  <tr key={index}>
                    <td className="col-bank">
                      <a href={item ? `/lai-suat-vay-${item.slug}` : `#`}>
                        {item.bankName.replace('Ngân hàng ', '')}
                      </a>
                    </td>
                    <td style={{ color: index == 0 ? 'red' : '' }}>{item.results.result}</td>
                    <td>{item.months}</td>
                  </tr>
                ))
              ) : (
                <tr>
                  <td colSpan={4}>
                    <div className="spinner-interest">
                      <Spinner color="primary" />
                    </div>
                  </td>
                </tr>
              )}
            </tbody>
          </table>
        </div>
      </div>
      <div className="view-all-interrest">
        <span>(Đơn vị: %)</span>
        <a href="/lai-suat-vay-ngan-hang">Xem toàn bộ</a>
      </div>
    </div>
  );
}
