import React, { Component } from 'react';

interface IRecipeProps {
  postOfPage?: any;
  stockCode?: string;
  showAuthor: boolean;
}
interface IRecipeState {}

class CurrencyDetailPost extends Component<IRecipeProps,IRecipeState> {
  
  formatDateTime = (data) => {

    let date = new Date(data)

    let getDate = date.getDate()
    let getMonth = date.getMonth() + 1
    let getFullyear = date.getFullYear()
    
    return `${getDate} tháng ${getMonth} ${getFullyear}`
  }

  componentDidMount() {
    const script = document.createElement('script');
    
    script.src = "/library/toc/handle.js";
    script.async = true;

    document.body.appendChild(script);
    
    return () => {
      document.body.removeChild(script);
    }
  }

  clickToShare = (type, url, title?) => {
    switch (type) {
      case 1:
        window.open(`https://www.facebook.com/sharer/sharer.php?u=${url}&amp;src=sdkpreparse`, '_blank');
        break;
      case 2:
        window.open(`https://twitter.com/share?text=${title};url=${url}`, '_blank');
        break;
      case 3:
        window.open(`https://www.reddit.com/submit?url=${url}&title=${title}`, '_blank');
        break;
      case 4:
        window.open(`https://www.linkedin.com/sharing/share-offsite/?url=${url}`, '_blank');
        break;
      case 5:
        window.open(`https://pinterest.com/pin/create/button/?url=${url}`, '_blank');
        break;
      case 6:
        window.open(`whatsapp://send?text=${url}`, '_blank');
        break;
      default:
        break;
    }
  }
  
  render() {

    // console.log("=======>", this.props.postOfPage)
    
    return (
      <div className="block-content-page">
        <div className="row">
          <div className="content-page-detail-left col-12">
            {this.props.postOfPage ?
              <>
              {this.props.postOfPage && <p className="content-page-detail-left-title" dangerouslySetInnerHTML={{ __html: this.props.postOfPage.title }}></p>}
              {this.props.postOfPage.author && this.props.showAuthor && <div className="box-info-author">
                <div className="info-author-left">
                  <a href={`/author/${this.props.postOfPage.author.slug}`}>
                    <img className="box-info-author-image" src={this.props.postOfPage.author && this.props.postOfPage.author.profile_image ? this.props.postOfPage.author.profile_image : "/images/default-user-avatar.svg"} width="100%" height="100%" alt="author-image"/>
                  </a>
                </div>
                <div className="info-author-right">
                  <p className="info-author-public">{this.props.postOfPage.publish_date}</p>
                  <p className="info-author-name">
                    <span>By {" "}</span>
                    <a href={`/author/${this.props.postOfPage.author.slug}`} className="info-author-name-link">
                      <span>{this.props.postOfPage.author.name}</span>
                    </a>
                  </p>
                </div>
              </div>}
              {this.props.postOfPage.feature_image && <img className="image-cover-article" src={this.props.postOfPage.feature_image ? this.props.postOfPage.feature_image : "/images/image-post-default.png"} width="100%" height="auto" alt="image-cover"/>}
              {this.props.postOfPage && <div className="box-article-html">
              <div className="box-share-artilce">
                <div className="box-share-artilce-stiky">
                  <div className="box-share-artilce-item">
                    <span style={{ cursor: "pointer" }} onClick={() => this.clickToShare(1, `https://money24h.vn/blog/${this.props.postOfPage.slug}`)}>
                      <img src="/images/icon-share-facebook.svg" alt="icon-share-facebook" width="36px" height="36px"/>
                    </span>
                  </div>
                  <div className="box-share-artilce-item">
                    <span style={{ cursor: "pointer" }} onClick={() => this.clickToShare(2, `https://money24h.vn/blog/${this.props.postOfPage.slug}`)}>
                      <img src="/images/icon-share-twitter.svg" alt="icon-share-twitter" width="36px" height="36px"/>
                    </span>
                  </div>
                  <div className="box-share-artilce-item">
                    <span style={{ cursor: "pointer" }} onClick={() => this.clickToShare(3, `https://money24h.vn/blog/${this.props.postOfPage.slug}`, this.props.postOfPage.title)}>
                      <img src="/images/icon-share-reddit.svg" alt="icon-share-reddit" width="36px" height="36px"/>
                    </span>
                  </div>
                  <div className="box-share-artilce-item">
                    <span style={{ cursor: "pointer" }} onClick={() => this.clickToShare(4, `https://money24h.vn/blog/${this.props.postOfPage.slug}`)}>
                      <img src="/images/icon-share-linkedin.svg" alt="icon-share-linkedin" width="36px" height="36px"/>
                    </span>
                  </div>
                  <div className="box-share-artilce-item">
                    <span style={{ cursor: "pointer" }} onClick={() => this.clickToShare(5, `https://money24h.vn/blog/${this.props.postOfPage.slug}`)}>
                      <img src="/images/icon-share-pinterest.svg" alt="icon-share-pinterest" width="36px" height="36px"/>
                    </span>
                  </div>
                </div>
              </div>
              <aside className="toc"></aside>
              <div className="render-html-page" dangerouslySetInnerHTML={{ __html: this.props.postOfPage.content }}></div>
              <div className="box-share-mobile">
                <div className="share-mobile-block">
                  <div className="share-mobile-block-item">
                    <span onClick={() => this.clickToShare(1, `https://money24h.vn/blog/${this.props.postOfPage.slug}`)}>
                      <div className="box-mobile-share-contain facebook">
                        <img className="image-share-mobile" src="/images/icon-share-facebook-mobile.svg" width="15px" height="15px" alt="icon-share-facebook-mobile"/>
                        <span className="text-share-mobile">Share</span>
                      </div>
                    </span>
                  </div>
                  <div className="share-mobile-block-item">
                    <span onClick={() => this.clickToShare(2, `https://money24h.vn/blog/${this.props.postOfPage.slug}`, this.props.postOfPage.title)}>
                      <div className="box-mobile-share-contain tweet">
                        <img className="image-share-mobile" src="/images/icon-share-twitter-mobile.svg" width="15px" height="15px" alt="icon-share-twitter-mobile"/>
                        <span className="text-share-mobile">Tweet</span>
                      </div>
                    </span>
                  </div>
                  <div className="share-mobile-block-item">
                    <span onClick={() => this.clickToShare(6, `https://money24h.vn/blog/${this.props.postOfPage.slug}`)}>
                      <div className="box-mobile-share-contain send">
                        <img className="image-share-mobile" src="/images/icon-share-whatapp-mobile.svg" width="15px" height="15px" alt="icon-share-whatapp-mobile"/>
                        <span className="text-share-mobile">Send</span>
                      </div>
                    </span>
                  </div>
                </div>
              </div>
            </div>}
              </> : <p>Đang cập nhật...</p>
            }
          </div>
        </div>
      </div>
    );
  }
}

export default CurrencyDetailPost;