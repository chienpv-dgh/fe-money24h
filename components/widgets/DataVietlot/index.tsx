import React from 'react';

const datalink = [
  {
    name: 'Power 6/55',
    link: 'https://xosovietlott.net/ket-qua-xo-so-power-6-55-vietlott-hom-nay/',
  },
  {
    name: 'Mega 6/45',
    link: 'https://xosovietlott.net/ket-qua-xo-so-mega-6-45-hom-nay/',
  },
  {
    name: 'Max 3D',
    link: 'https://xosovietlott.net/ket-qua-xo-so-max-3d-vietlott-hom-nay/',
  },
  {
    name: 'Max 4D',
    link: 'https://xosovietlott.net/ket-qua-xo-so-max-4d-vietlott-hom-nay/',
  },
];

export interface VietlotDataWidgetProps {
  tableVietlot: {
    description: string;
    name: string;
    period: string;
    reward: number;
  }[];
}

export interface DataLinkProps {
  name: string;
  link: string;
}

export default function VietlotData({ tableVietlot }: VietlotDataWidgetProps) {
  const numberWithCommas = (number: number) => {
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
  };

  const getlink = (data: DataLinkProps[], name: string) => {
    const link = data.filter((x) => x.name === name);
    return link[0].link;
  };

  return (
    <div className="vietlot-block">
      <h3 className="vietlot-box-highlight">
        <a href="https://xosovietlott.net/" target="_blank" rel="noreferrer">
          xosovietlott.net
        </a>
      </h3>
      <div className="divide-box-right"></div>
      <div className="data-table-vietlot">
        {tableVietlot ? (
          <table className="table table-bordered table-vietlot">
            <tbody>
              {tableVietlot.map((item, index) => (
                <tr key={index}>
                  <td>
                    <a
                      target="_blank"
                      href={
                        datalink.filter((x) => x.name === item.name).length > 0
                          ? `${getlink(datalink, item.name)}`
                          : ''
                      }
                      rel="noreferrer"
                    >
                      <p className="highlight-title">{item.name}</p>
                    </a>
                    <p className="title-normal">{item.description}</p>
                  </td>
                  <td>
                    <p className="number-normal">{`${numberWithCommas(item.reward)} VNĐ`}</p>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        ) : (
          <div className="data-not-found-home">
            <img
              src="/images/image-data-not-found.svg"
              width="100%"
              height="100%"
              alt="image-data-not-found"
            />
          </div>
        )}
      </div>
    </div>
  );
}
