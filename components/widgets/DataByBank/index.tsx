import React, { useEffect, useState } from 'react';
import { Spinner } from 'reactstrap';

export interface DataByBankWidgetProps {
  dataInterest: {
    offline: {
      bankName: string;
      months1?: string;
      months3?: string;
      months6?: string;
      months9?: string;
      months12?: string;
      slug: string;
      type: number;
    }[];
    online: {
      bankName: string;
      months1?: string;
      months3?: string;
      months6?: string;
      months9?: string;
      months12?: string;
      slug: string;
      type: number;
    }[];
  };
}

export interface DataBankOffProps {
  bankName: string;
      months1?: string;
      months3?: string;
      months6?: string;
      months9?: string;
      months12?: string;
      slug: string;
      type: number;
}

export interface DataBankOnProps {
  bankName: string;
      months1?: string;
      months3?: string;
      months6?: string;
      months9?: string;
      months12?: string;
      slug: string;
      type: number;
}

export default function WidgetDataByBank ({dataInterest}: DataByBankWidgetProps) {
  
  const [dataByBankOff, setDataByBankOff] = useState<DataBankOffProps[]>([])
  const [dataByBankOnl, setDataByBankOnl] = useState<DataBankOnProps[]>([])
  const [col, setCol] = useState(12)
  const [typeCheck, setTypeCheck] = useState("off")

  const handleSort = async (col: number) => {
    setCol(col)
    if (typeCheck == "off") {
      let data = [...dataInterest.offline]
      let tempSort = []
      switch (col) {
        case 1:
          tempSort = [...data].sort((a, b) => Number(b.months1) - Number(a.months1))
          await setDataByBankOff(tempSort)

          break;
        case 3:
          tempSort = [...data].sort((a, b) => Number(b.months3) - Number(a.months3))
          await setDataByBankOff(tempSort)
          break;
        case 6:
          tempSort = [...data].sort((a, b) => Number(b.months6) - Number(a.months6))
          await setDataByBankOff(tempSort)
          break;
        case 9:
          tempSort = [...data].sort((a, b) => Number(b.months9) - Number(a.months9))
          await setDataByBankOff(tempSort)
          break;
        case 12:
          tempSort = [...data].sort((a, b) => Number(b.months12) - Number(a.months12))
          await setDataByBankOff(tempSort)
          break;
      }
    } else {
      let data = [...dataInterest.online]
      let tempSort = []
      switch (col) {
        case 1:
          tempSort = [...data].sort((a, b) => Number(b.months1) - Number(a.months1))
          await setDataByBankOff(tempSort)
          break;
        case 3:
          tempSort = [...data].sort((a, b) => Number(b.months3) - Number(a.months3))
          await setDataByBankOff(tempSort)
          break;
        case 6:
          tempSort = [...data].sort((a, b) => Number(b.months6) - Number(a.months6))
          await setDataByBankOff(tempSort)
          break;
        case 9:
          tempSort = [...data].sort((a, b) => Number(b.months9) - Number(a.months9))
          await setDataByBankOff(tempSort)
          break;
        case 12:
          tempSort = [...data].sort((a, b) => Number(b.months12) - Number(a.months12))
          await setDataByBankOff(tempSort)
          break;
      }
    }
  }

  const changeTypeInterest = async (type: string) => {
    setTypeCheck(type)
  }

  useEffect(() => {
    const handleSortByType = async (col: number, ) => {
      setCol(col)
      if (typeCheck == "off") {
        let data = [...dataInterest.offline]
        let tempSort = []
        switch (col) {
          case 1:
            tempSort = [...data].sort((a, b) => Number(b.months1) - Number(a.months1))
            await setDataByBankOff(tempSort)
  
            break;
          case 3:
            tempSort = [...data].sort((a, b) => Number(b.months3) - Number(a.months3))
            await setDataByBankOff(tempSort)
            break;
          case 6:
            tempSort = [...data].sort((a, b) => Number(b.months6) - Number(a.months6))
            await setDataByBankOff(tempSort)
            break;
          case 9:
            tempSort = [...data].sort((a, b) => Number(b.months9) - Number(a.months9))
            await setDataByBankOff(tempSort)
            break;
          case 12:
            tempSort = [...data].sort((a, b) => Number(b.months12) - Number(a.months12))
            await setDataByBankOff(tempSort)
            break;
        }
      } else {
        let data = [...dataInterest.online]
        let tempSort = []
        switch (col) {
          case 1:
            tempSort = [...data].sort((a, b) => Number(b.months1) - Number(a.months1))
            await setDataByBankOff(tempSort)
            break;
          case 3:
            tempSort = [...data].sort((a, b) => Number(b.months3) - Number(a.months3))
            await setDataByBankOff(tempSort)
            break;
          case 6:
            tempSort = [...data].sort((a, b) => Number(b.months6) - Number(a.months6))
            await setDataByBankOff(tempSort)
            break;
          case 9:
            tempSort = [...data].sort((a, b) => Number(b.months9) - Number(a.months9))
            await setDataByBankOff(tempSort)
            break;
          case 12:
            tempSort = [...data].sort((a, b) => Number(b.months12) - Number(a.months12))
            await setDataByBankOff(tempSort)
            break;
        }
      }
    }
    handleSortByType(col)
  }, [col, dataInterest.offline, dataInterest.online, typeCheck])

  const highlightNumber = (data: DataBankOffProps[], item?: string, prefix?: string) => {
    if (data && item && prefix) {
      switch (prefix) {
        case "months12":
          let dataCompareMonths12 = data.map((item) => item.months12).sort((a, b) => Number(b) - Number(a))
          if (item == dataCompareMonths12[0]) {
            return true
          } else {
            return false
          }
        case "months9":
          let dataCompareMonths9 = data.map((item) => item.months9).sort((a, b) => Number(b) - Number(a))
          if (item == dataCompareMonths9[0]) {
            return true
          } else {
            return false
          }
        case "months6":
          let dataCompareMonths6 = data.map((item) => item.months6).sort((a, b) => Number(b) - Number(a))
          if (item == dataCompareMonths6[0]) {
            return true
          } else {
            return false
          }
        case "months3":
          let dataCompareMonths3 = data.map((item) => item.months3).sort((a, b) => Number(b) - Number(a))
          if (item == dataCompareMonths3[0]) {
            return true
          } else {
            return false
          }
        case "months1":
          let dataCompareMonths1 = data.map((item) => item.months1).sort((a, b) => Number(b) - Number(a))
          if (item == dataCompareMonths1[0]) {
            return true
          } else {
            return false
          }
      }
    } else {
      return false
    }
  }

  useEffect(() => {
    if (dataInterest) {
      const handleSortReady = async (col: number) => {
        setCol(col)
        if (typeCheck == "off") {
          let data = [...dataInterest.offline]
          let tempSort = []
          switch (col) {
            case 1:
              tempSort = [...data].sort((a, b) => Number(b.months1) - Number(a.months1))
              await setDataByBankOff(tempSort)
    
              break;
            case 3:
              tempSort = [...data].sort((a, b) => Number(b.months3) - Number(a.months3))
              await setDataByBankOff(tempSort)
              break;
            case 6:
              tempSort = [...data].sort((a, b) => Number(b.months6) - Number(a.months6))
              await setDataByBankOff(tempSort)
              break;
            case 9:
              tempSort = [...data].sort((a, b) => Number(b.months9) - Number(a.months9))
              await setDataByBankOff(tempSort)
              break;
            case 12:
              tempSort = [...data].sort((a, b) => Number(b.months12) - Number(a.months12))
              await setDataByBankOff(tempSort)
              break;
          }
        } else {
          let data = [...dataInterest.online]
          let tempSort = []
          switch (col) {
            case 1:
              tempSort = [...data].sort((a, b) => Number(b.months1) - Number(a.months1))
              await setDataByBankOff(tempSort)
              break;
            case 3:
              tempSort = [...data].sort((a, b) => Number(b.months3) - Number(a.months3))
              await setDataByBankOff(tempSort)
              break;
            case 6:
              tempSort = [...data].sort((a, b) => Number(b.months6) - Number(a.months6))
              await setDataByBankOff(tempSort)
              break;
            case 9:
              tempSort = [...data].sort((a, b) => Number(b.months9) - Number(a.months9))
              await setDataByBankOff(tempSort)
              break;
            case 12:
              tempSort = [...data].sort((a, b) => Number(b.months12) - Number(a.months12))
              await setDataByBankOff(tempSort)
              break;
          }
        }
      }
      handleSortReady(12)
    }
  }, [dataInterest, typeCheck])

  return (
    <div className="interest-bank-block">
      <h3 className="interest-bank-highlight">Lãi suất tiết kiệm</h3>
      <div className="divide-box-right"></div>
      <div className="interest-box-data">
        <div className="box-description-interrest">
          <span className="smail-text">(Từ cao đến thấp)</span>
          <div className="choose-type-interest">
            <span onClick={() => changeTypeInterest("off")} className={`type-interest-item ${typeCheck == "off" ? "active" : ""}`}>Tại quầy</span>
            <span onClick={() => changeTypeInterest("on")} className={`type-interest-item ${typeCheck == "on" ? "active" : ""}`}>Online</span>
          </div>
        </div>
      </div>

      <div className="box-table-data-intterest-bank">
        <div className="box-table">
          <table className="row-month">
            <tbody>
              <tr>
                <td className="col-bank">Kỳ hạn (tháng)</td>
                <td onClick={() => handleSort(1)}>
                  <div className="box-sort-type">
                    <i className="fa fa-sort-amount-desc icon-sort" aria-hidden="true" style={{ color: col == 1 ? "#076FBA" : "#e4e4e4" }}></i>
                    <span style={{ color: col == 1 ? "#076FBA" : "#000" }}>1</span>
                  </div>
                </td>
                <td onClick={() => handleSort(3)}>
                  <div className="box-sort-type">
                    <i className="fa fa-sort-amount-desc icon-sort" aria-hidden="true" style={{ color: col == 3 ? "#076FBA" : "#e4e4e4" }}></i>
                    <span style={{ color: col == 3 ? "#076FBA" : "#000" }}>3</span>
                  </div>
                </td>
                <td onClick={() => handleSort(6)}>
                  <div className="box-sort-type">
                    <i className="fa fa-sort-amount-desc icon-sort" aria-hidden="true" style={{ color: col == 6 ? "#076FBA" : "#e4e4e4" }}></i>
                    <span style={{ color: col == 6 ? "#076FBA" : "#000" }}>6</span>
                  </div>
                </td>
                <td onClick={() => handleSort(9)}>
                  <div className="box-sort-type">
                    <i className="fa fa-sort-amount-desc icon-sort" aria-hidden="true" style={{ color: col == 9 ? "#076FBA" : "#e4e4e4" }}></i>
                    <span style={{ color: col == 9 ? "#076FBA" : "#000" }}>9</span>
                  </div>
                </td>
                <td onClick={() => handleSort(12)}>
                  <div className="box-sort-type">
                    <i className="fa fa-sort-amount-desc icon-sort" aria-hidden="true" style={{ color: col == 12 ? "#076FBA" : "#e4e4e4" }}></i>
                    <span style={{ color: col == 12 ? "#076FBA" : "#000" }}>12</span>
                  </div>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
        <div className="data-interest-by-bank customScroll">
          <table className="data-interest-table">
            <tbody>
              {dataInterest ?
                dataByBankOff && dataByBankOff.map((item, index) => (
                  <tr key={index}>
                    <td className="col-bank">
                      <a href={item ? `/lai-suat-gui-tiet-kiem-${item.slug}` : `#`}>{item.bankName.replace("Ngân hàng ", "")}</a>

                    </td>
                    <td style={{ color: `${highlightNumber(dataByBankOff, item.months1, "months1") ? "red" : ""}` }}>{item.months1}</td>
                    <td style={{ color: `${highlightNumber(dataByBankOff, item.months3, "months3") ? "red" : ""}` }}>{item.months3}</td>
                    <td style={{ color: `${highlightNumber(dataByBankOff, item.months6, "months6") ? "red" : ""}` }}>{item.months6}</td>
                    <td style={{ color: `${highlightNumber(dataByBankOff, item.months9, "months9") ? "red" : ""}` }}>{item.months9}</td>
                    <td style={{ color: `${highlightNumber(dataByBankOff, item.months12, "months12") ? "red" : ""}` }}>{item.months12}</td>
                  </tr>
                )) :
                <tr>
                  <td colSpan={6}>
                    <div className="spinner-interest">
                      <Spinner color="primary" />
                    </div>
                  </td>
                </tr>
              }
            </tbody>
          </table>
        </div>
      </div>

      <div className="view-all-interrest">
        <span>(Đơn vị: %/năm)</span>
        <a href="/lai-suat-gui-tiet-kiem-ngan-hang">Xem toàn bộ</a>
      </div>
    </div>
  );
}