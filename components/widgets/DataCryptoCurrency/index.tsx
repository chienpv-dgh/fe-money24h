import React, { useState, useEffect } from 'react';
import { Spinner } from 'reactstrap';
import { fetchAPI } from '../../../pages/api';

export interface quoteObject {
  fully_diluted_market_cap: number;
  last_updated: string;
  market_cap: number;
  market_cap_dominance: number;
  percent_change_1h: number;
  percent_change_7d: number;
  percent_change_24h: number;
  percent_change_30d: number;
  percent_change_60d: number;
  percent_change_90d: number;
  price: number;
  volume_24h: number;
  volume_change_24h: number;
}

export interface WidgetsDataCryptoProps {
  dataCrypto: {
    status: {
      timestamp: string;
      total_count: number;
    };
    data: {
      circulating_supply: number;
      cmc_rank: number;
      id: number;
      last_updated: string;
      max_supply: number;
      total_supply: number;
      name: string;
      num_market_pairs: number;
      slug: string;
      symbol: string;
      quote: quoteObject[];
    }[];
  };
}

let WidgetsDataCrypto = ({ dataCrypto }) => {
  const [isActice, setIsActive] = useState(true);
  const [isDesc, setIsDesc] = useState(true);
  const [isInit, setInit] = useState('VND');
  const [data, setData] = useState([...dataCrypto.data]);
  const [isLoading, setIsLoading] = useState(false);
  const convertObjecttoArray = (object: quoteObject) => {
    let data = Object.entries(object);
    let dataReturn: quoteObject = data[0][1];
    return dataReturn;
  };

  const handleChangeLang = async (unit) => {
    if (unit === isInit) return;
    setIsActive(!isActice);
    setInit(unit);
  };

  const handleSortChange = async () => {
    setIsDesc(!isDesc);
  };

  useEffect(() => {
    async function getData() {
      setIsLoading(true);
      let desc;
      if (isDesc) {
        desc = 'desc';
      } else {
        desc = 'asc';
      }
      let dataChange = await fetchAPI(
        `/coincotroller/sort-coin?Limit=10&Sort_dir=${desc}&Convert=${isInit.toLocaleLowerCase()}`
      );
      let dataCrypto = dataChange && dataChange.success ? dataChange.data : null;
      setData([...dataCrypto.data]);
      setIsLoading(false);
      return dataCrypto;
    }
    getData();
  }, [isDesc, isInit]);

  const formatNumberWithComma = (x) => {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  };

  const renderColorPercent = (percent) => {
    if (Number(percent)) {
      if (Number(percent) > 0) {
        return 'text-increase';
      } else {
        return 'text-decrease';
      }
    } else {
      return '';
    }
  };

  return (
    <div className="datacrypto">
      <h3 className="datacrypto__title">
        <a href="/tien-ao">Bảng giá tiền ảo (coin)</a>
      </h3>
      <div className="datacrypto__device"></div>
      <div className="datacrypto__lang">
        <p className="datacrypto__lang--text">Đơn vị</p>
        <div className="datacrypto__lang--choose">
          <span
            className={
              isActice ? 'datacrypto__lang--choose--btn active' : 'datacrypto__lang--choose--btn'
            }
            onClick={() => handleChangeLang('VND')}
          >
            VND
          </span>
          <span
            className={
              isActice ? 'datacrypto__lang--choose--btn ' : 'datacrypto__lang--choose--btn active'
            }
            onClick={() => handleChangeLang('USD')}
          >
            USD
          </span>
        </div>
      </div>
      <div className="datacrypto__table">
        <div className="datacrypto__table--top">
          <table className="datacrypto__table--main">
            <tbody>
              <tr>
                <td>Tiền Ảo</td>
                <td>Giá ({isInit}) </td>
                <td
                  className={`datacrypto__table--main--iconsort  ${isDesc ? '' : 'active'}`}
                  onClick={() => handleSortChange()}
                >
                  <span>
                    <i className={`fa fa-sort-amount-${isDesc ? 'desc' : 'asc'}`}></i>
                  </span>
                  <span>%(24H)</span>
                </td>
                <td>Vốn hóa</td>
              </tr>
            </tbody>
          </table>
        </div>
        <div className="datacrypto__table--bottom customScroll">
          {isLoading ? (
            <div className="loading_box">
              <Spinner animation="border" color="primary" />
            </div>
          ) : (
            <table className="datacrypto__table--bottom-main ">
              <tbody>
                {data &&
                  data.map((item, index) => {
                    let priceChange = convertObjecttoArray(item.quote);
                    return (
                      <tr key={index}>
                        <td>
                          <a
                            href={`/tien-ao/${item.symbol}`}
                            title={`Giá ${item.name}`}
                            className="datacrypto__table--bottom--link"
                          >
                            {item.symbol}
                          </a>
                        </td>
                        {isInit === 'VND' ? (
                          <td>
                            {priceChange
                              ? `${formatNumberWithComma(
                                  Math.round(priceChange.price * 100) / 100
                                )} VNĐ`
                              : '-'}
                          </td>
                        ) : (
                          <td>
                            {priceChange
                              ? `${formatNumberWithComma(
                                  Math.round(priceChange.price * 100) / 100
                                )} $ `
                              : '-'}
                          </td>
                        )}
                        <td
                          className={renderColorPercent(
                            Math.round(priceChange.percent_change_24h * 100) / 100
                          )}
                        >
                          {priceChange
                            ? `${formatNumberWithComma(
                                Math.round(priceChange.percent_change_24h * 100) / 100
                              )} %`
                            : '-'}
                        </td>
                        <td>{priceChange ? formatNumberWithComma(priceChange.market_cap) : '-'}</td>
                      </tr>
                    );
                  })}
              </tbody>
            </table>
          )}
        </div>
      </div>
    </div>
  );
};

export default WidgetsDataCrypto;
