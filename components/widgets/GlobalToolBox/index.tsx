import React from 'react';
import Image from 'next/image';

export default function GlobalToolBox() {
  const currentMonth = () => {
    const today = new Date();
    const month = today.getMonth();
    return ` tháng ${month + 1}`;
  };

  const arrTag = [
    {
      itemTitle: 'Tính lãi suất <br /> tiết kiệm',
      itemLink: '/cong-cu-tinh-tien-lai-gui-ngan-hang',
      itemImage: '/images/interest-calc-image.png',
    },
    {
      itemTitle: 'Tính lãi suất vay',
      itemLink: '/cong-cu-tinh-toan-khoan-vay-ngan-hang',
      itemImage: '/images/loan-calc-image.png',
    },
    {
      itemTitle: 'Tính tỷ giá <br /> ngoại tệ',
      itemLink: '/cong-cu-tinh-ty-gia-ngoai-te',
      itemImage: '/images/currency-calc-image.png',
    },
    {
      itemTitle: 'Lãi suất tiết kiệm' + currentMonth(),
      itemLink: '/lai-suat-gui-tiet-kiem-ngan-hang',
      itemImage: '/images/check-interest-rate-image.png',
    },
    {
      itemTitle: 'Lãi suất vay' + currentMonth(),
      itemLink: '/lai-suat-vay-ngan-hang',
      itemImage: '/images/check-loan-rate-image.png',
    },
    {
      itemTitle: 'Tra cứu tỷ giá <br /> ngoại tệ',
      itemLink: '/ty-gia-ngoai-te-hoi-doai-ngan-hang-hom-nay',
      itemImage: '/images/check-currency-rate.png',
    },
  ];

  return (
    <div className="block-view-more-tool">
      <p className="view-more-tool-title">CÁC CÔNG CỤ TÀI CHÍNH</p>
      <div className="view-more-tool-item row">
        {arrTag.map((item, index) => (
          <div className="view-more-tool-item-detail col-4 col-lg-2" key={index}>
            <a href={item.itemLink} className="box-link-icon">
              <div className="item-detail-box">
                <Image
                  className="image-optimize"
                  alt="item-detail-image"
                  src={item.itemImage}
                  layout="fixed"
                  width={80}
                  height={80}
                />
              </div>
              <p
                className="desciption-image"
                dangerouslySetInnerHTML={{ __html: `${item.itemTitle}` }}
              ></p>
            </a>
          </div>
        ))}
      </div>
    </div>
  );
}
