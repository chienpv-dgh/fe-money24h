import SwiperCore, { Navigation, Pagination } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';

SwiperCore.use([Navigation, Pagination]);

export interface ArticleInterestedProps {
  AllPosts: {
    author: {
      name: string;
      slug: string;
    };
    category: {
      name: string;
      slug: string;
    }[];
    excerpt: string;
    feature_image?: string;
    publish_date?: string;
    slug: string;
    title: string;
    tag?: {
      name: string;
      slug: string;
    }[];
  }[];
}

const ArticleInterested = ({ AllPosts }: ArticleInterestedProps) => {
  return (
    AllPosts && (
      <div className="box-list-article-interested">
        <Swiper
          className="article-interested-swiper-contain"
          slidesPerView={3}
          noSwiping={AllPosts.length > 4}
          spaceBetween={24}
          breakpoints={{
            320: {
              slidesPerView: 1,
              allowTouchMove: true,
              shortSwipes: true,
              spaceBetween: 16,
            },
            400: {
              slidesPerView: 1,
              allowTouchMove: true,
              shortSwipes: true,
              spaceBetween: 16,
            },
            768: {
              slidesPerView: 2,
              allowTouchMove: true,
              shortSwipes: true,
              spaceBetween: 24,
            },
            1024: {
              slidesPerView: 3,
              allowTouchMove: false,
              shortSwipes: true,
              spaceBetween: 24,
            },
          }}
        >
          {AllPosts.slice(0, 3).map((item, index) => (
            <SwiperSlide key={index} className="article-interested-swiper-item">
              <a href={`/blog/${item.slug}`} className="article-interested-swiper-item-tag-link">
                <img
                  src={item.feature_image}
                  className="article-interested-swiper-image"
                  width="100%"
                  height="100%"
                  alt="image-article-interested"
                />
                <p
                  className="article-interested-swiper-title"
                  dangerouslySetInnerHTML={{ __html: item.title }}
                ></p>
              </a>
            </SwiperSlide>
          ))}
        </Swiper>
      </div>
    )
  );
};

export default ArticleInterested;
