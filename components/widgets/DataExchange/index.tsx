import React, { Component } from 'react';
import { Spinner } from 'reactstrap';

interface IRecipeProps {
    dataExchange?: any
}
interface IRecipeState {
    dataExchangeSort: any
    typeCheck?: string
}

class ExchangeRate extends Component<IRecipeProps, IRecipeState> {
    constructor(props) {
        super(props);
        this.state = {
            dataExchangeSort: null,
            typeCheck: '',
        }
    }

    async componentDidMount() {
        if (this.props.dataExchange) {
            this.handleSort('sellRate');
        }
    }
    checkRenderTimeDate = (date) => {
        let receiveDate = new Date(date)
        let year = receiveDate.getFullYear()
        let month = receiveDate.getMonth() + 1
        let day = receiveDate.getDate()
        let result = `${day}/${month}/${year}`

        return result
    }
    handleSort = async (typeCheck) => {

        let data = [...this.props.dataExchange.results];
        let tempSort = [];
        switch (typeCheck) {
            case 'sellRate':
                tempSort = [...data].sort((a, b) => Number((b.sellRate).replace('.', '').replace(',', '.')) - Number(a.sellRate.replace('.', '').replace(',', '.')))
                await this.setState({
                    dataExchangeSort: tempSort,
                })
                break;
            case 'cash':
                tempSort = [...data].sort((a, b) => Number((b.cash).replace('.', '').replace(',', '.')) - Number(a.cash.replace('.', '').replace(',', '.')))
                await this.setState({
                    dataExchangeSort: tempSort,
                })
                break;
            case 'transfer':
                tempSort = [...data].sort((a, b) => Number((b.transfer).replace('.', '').replace(',', '.')) - Number(a.transfer.replace('.', '').replace(',', '.')))
                await this.setState({
                    dataExchangeSort: tempSort,
                })
                break;
            default:
                break;
        }
    }

    render() {
        return (
            <div className='exchange-box'>
                <h3 className='exchange-box-highlight'>Tỷ giá ngoại tệ</h3>
                <div className="divide-box-right"></div>
                <div className="exchange-box-data">
                    <div className="box-description-exchange">
                        <div className="smail-text">
                            <p style={{margin: 0}}>{`(Cập nhật mới nhất ngày ${this.checkRenderTimeDate(new Date())})`}</p>
                        </div>
                    </div>
                </div>

                <div className="box-table-data-exchange-rate">
                    <div className="box-table">
                        <table className="row-title">
                            <tbody>
                                <tr>
                                    <td className='col-currency' rowSpan={2}>Ngoại tệ</td>
                                    <td className='col-currency' rowSpan={2}>Mã<br></br> ngoại tệ</td>
                                    <td colSpan={2}>Tỷ giá mua</td>
                                    <td className='col-rate' rowSpan={2} >
                                        <div className='box-sort-type'>
                                            <span style={{ color: this.state.typeCheck === 'sellRate' ? "#076FBA" : "#000" }}>Tỷ giá bán</span>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td className='col-bank' >
                                        <div className='box-sort-type'>
                                            <span style={{ color: this.state.typeCheck === 'cash' ? "#076FBA" : "#000" }}>Tiền mặt</span>
                                        </div>
                                    </td>
                                    <td className='col-rate' >
                                        <div className='box-sort-type'>
                                            <span style={{ color: this.state.typeCheck === 'transfer' ? "#076FBA" : "#000" }}>Chuyển khoản</span>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div className="data-exchange-rate customScroll">
                        <table className="data-exchange-table">
                            <tbody>
                                {this.props.dataExchange.results && this.state.dataExchangeSort && this.state.dataExchangeSort &&
                                    <>
                                        {this.state.dataExchangeSort.filter((item) => item.moneyCode == "USD").map((item, index) => <tr key={index}>
                                            <td className='col-bank'>
                                                <a href={`/ty-gia-${item.fullSlug}`}>{item.moneyName} </a>
                                            </td>
                                            <td className='col-bank'>{item.moneyCode}</td>
                                            <td>{item.cash}</td>
                                            <td className='col-rate'>{item.transfer}</td>
                                            <td className='col-rate'>{item.sellRate}</td>
                                        </tr>)}
                                        {this.state.dataExchangeSort.filter((item) => item.moneyCode == "GBP").map((item, index) => <tr key={index}>
                                            <td className='col-bank'>
                                                <a href={`/ty-gia-${item.fullSlug}`}>{item.moneyName} </a>
                                            </td>
                                            <td className='col-bank'>{item.moneyCode}</td>
                                            <td>{item.cash}</td>
                                            <td className='col-rate'>{item.transfer}</td>
                                            <td className='col-rate'>{item.sellRate}</td>
                                        </tr>)}
                                    </>
                                }
                                {this.props.dataExchange.results ? this.state.dataExchangeSort && this.state.dataExchangeSort.map((item, index) => {
                                    if(item.moneyCode == "USD" || item.moneyCode == "GBP") {
                                        return
                                    } else {
                                        return (
                                            <tr key={index}>
                                                <td className='col-bank'>
                                                    <a href={`/ty-gia-${item.fullSlug}`}>{item.moneyName} </a>
                                                </td>
                                                <td className='col-bank'>{item.moneyCode}</td>
                                                <td>{item.cash}</td>
                                                <td className='col-rate'>{item.transfer}</td>
                                                <td className='col-rate'>{item.sellRate}</td>
                                            </tr>
                                        )   
                                    }
                                }) :
                                    <tr>
                                        <td colSpan={6}>
                                            <div className="spinner-interest">
                                                <Spinner color="primary" />
                                            </div>
                                        </td>
                                    </tr>
                                }
                            </tbody>
                        </table>
                    </div>
                </div>
                <div className="view-all-exchange">
                    <span>(Đơn vị: VNĐ)</span>
                    <span>Nguồn: <a href={`/ty-gia-ngoai-te-${this.props.dataExchange.slug}`}>Vietcombank</a></span>
                </div>
            </div >
        );
    }
}
export default ExchangeRate;

