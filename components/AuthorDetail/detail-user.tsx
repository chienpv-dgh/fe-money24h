export default function UserDetail({ detailAuthor, total }) {

  return (
    <div className="block-user-detail">
      <div className="author-inner">
        <div className="author-left">
          <div className="author-avatar">
            <img className="author-avatar-image" src={detailAuthor.avatar ? detailAuthor.avatar : "/images/default-user-avatar.svg"} alt="" />
          </div>
        </div>
        <div className="author-right">
          <p className="author-name">{detailAuthor.display_name ? detailAuthor.display_name : "Money24h"}</p>
          {detailAuthor.user_description && <p className="author-bio">{detailAuthor.user_description ? detailAuthor.user_description : ""}</p>}
          <div className="author-follow">
            <a className="button-follow facebook" href={detailAuthor.facebook ? detailAuthor.facebook : "/#"} target="_blank" rel="noreferrer">
              <i className="fa fa-facebook-square button-follow-icon"></i> Facebook
            </a>
            <a className="button-follow twitter" href={detailAuthor.facebook ? detailAuthor.twitter : "/#"} target="_blank" rel="noreferrer">
              <i className="fa fa-twitter button-follow-icon" aria-hidden="true"></i> Twitter
            </a>
            <a className="button-follow feed" href={detailAuthor.facebook ? detailAuthor.facebook : "/#"} target="_blank" rel="noreferrer">
              <i className="fa fa-rss button-follow-icon" aria-hidden="true"></i> {`${total} posts`}
            </a>
          </div>
        </div>
      </div>
    </div>
  )
}