import { useState } from 'react';
import dynamic from "next/dynamic";
import { Spinner } from 'reactstrap';
import { UserDetail } from '.';

// const StockHome = dynamic(() => import("../modules/HomePage/stock-home"));
const VietlotData = dynamic(() => import("../widgets/DataVietlot"));
const WidgetDataByBank = dynamic(() => import("../widgets/DataByBank"));

export default function AuthorDetail({  
  dataInterest, 
  tableVietlot, 
  dataPostAuthor,
  authorSlug 
}) {

  const [dataPost, setDataPost] = useState(dataPostAuthor.posts);
  const [page, setPage] = useState(1)
  const [size, setSize] = useState(20)
  const [loading, setLoading] = useState(false)

  const getDataPost = async (size, offset) => {
    setLoading(true)
    let res = await fetch(`https://admin.money24h.vn/wp-json/totapi/posts/get_post_by_author?key=4746a0cc735cb3ea921a01ffb1e5c257&author_slug=${authorSlug}&posts_per_page=20&page=${size}&offset=${offset}`)
    let dataRes = await res.json()
    if (dataRes) {
      let data = [...dataPost].concat(dataRes.posts)
      await setDataPost(data)
      await setLoading(false)
    }
  }

  const fetchMoreData = () => {
    let newPage = page + 1;
    let newSize = size + 20;
    setPage(newPage)
    setSize(newSize)
    getDataPost(newPage, newSize)
  }

  return (
    <div className="author-detail-contain">
      <p className="bread-scrum-post-page">
        <a href="/">Trang chủ</a>
        <i className="fa fa-angle-right bread-scrum-divider" aria-hidden="true"></i>
        <span>Author</span>
        <i className="fa fa-angle-right bread-scrum-divider post" aria-hidden="true"></i>
        <span>{dataPostAuthor.author_object.display_name}</span>
      </p>
      <UserDetail detailAuthor={dataPostAuthor.author_object} total={dataPostAuthor.total_posts} />
      <div className="title-line">
        <span className="u-oblique">{`Stories by ${dataPostAuthor.author_object.display_name}`}</span>
      </div>
      <div className="row">
        <div className="block-detail-left col-12 col-lg-8">
          {dataPostAuthor.posts && dataPostAuthor.posts.length > 0 ? <div className="block-post-by-tag row">
            <div className="block-post-data-item">
              {dataPost.map((item, index) =>
                <a key={index} href={`/blog/${item.slug}`}>
                  <div className="block-post-data-item-detail">
                    <div className="block-post-data-item-contain-image">
                      <img className="block-post-data-item-image" src={item.feature_image} alt="image-post-by-tag" width="100%" height="100%" />
                    </div>
                    <div className="block-post-data-item-content">
                      <p className="block-post-data-item-content-title" dangerouslySetInnerHTML={{ __html: item.title }}></p>
                      <p className="block-post-data-item-content-date">{item.publish_date}</p>
                      <p className="block-post-data-item-content-excerpt" dangerouslySetInnerHTML={{ __html: item.excerpt }}></p>
                    </div>
                  </div>
                </a>
              )}
              {loading ? <div className="spinner">
                <Spinner color="primary" />
              </div> : <div className="box-view-more">
                <p className="button-view-more" onClick={() => fetchMoreData()}>
                  View more
                </p>
              </div>}
            </div>
          </div> : null}
        </div>
        <div className="block-body-right col-12 col-lg-4">
          <WidgetDataByBank dataInterest={dataInterest} />
          {/* <StockHome /> */}
          <VietlotData tableVietlot={tableVietlot} />
        </div>
      </div>
    </div>
  )
}