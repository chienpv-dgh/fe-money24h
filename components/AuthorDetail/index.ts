import AuthorDetail from "./author-page";
import UserDetail from "./detail-user";

export {
    AuthorDetail,
    UserDetail
}