import CheckInterestBankPage from "./check-interest-rate-page";
import BoxChooseBank from "./box-choose-bank";
import TableDataInterestByBank from "./table-data-interest-by-bank";

export {
  CheckInterestBankPage,
  BoxChooseBank,
  TableDataInterestByBank
}