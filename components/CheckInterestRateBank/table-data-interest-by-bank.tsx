import React, { Component } from 'react';
import { Table } from 'reactstrap';

interface IRecipeProps {
  dataTableInterestRateBank?: any;
  savingRateAllBank?: any;
  savingRateByBank?: any;
}
interface IRecipeState {
  dataSort?: any;
  col?: number;
  sortType?: boolean;
  maxCol?: object;
  checkRender?: boolean;
}

class TableDataInterestByBank extends Component<IRecipeProps, IRecipeState> {
  constructor(props) {
    super(props);
    this.state = {
      dataSort: null,
      col: 5,
      sortType: true,
      maxCol: {},
      checkRender: false,
    }
  }

  checkRenderTimeDate = (date) => {
    let receiveDate = new Date(date)
    let year = receiveDate.getFullYear()
    let month = receiveDate.getMonth() + 1
    let day = receiveDate.getDate()
    let result = `${day}/${month}/${year}`

    return result
  }

  sortDataTable = async (type, col) => {

    let data = [...this.props.dataTableInterestRateBank]

    let tempCheck = [...data].sort((a, b) => b.results.length - a.results.length)

    for (let i = 0; i < tempCheck.length; i++) {
      if (tempCheck[i].results.length > 0) {
        tempCheck[i].results = tempCheck[i].results.map((item) => {
          
          if (item.result) {
            if (Number(item.result)) {
              return item.result.replace("-", "0")
            } else {
              return item.result = "0"
            }
          } else {
            if (Number(item)) {
              return item.replace("-", "0")
            } else {
              return item = "0"
            }
          }
        })
      }
    }
    let prevCol = this.state.col
    await this.setState({ col: col })

    let typeCheck = type
    if (prevCol !== col) {
      typeCheck = !type
      await this.setState({ sortType: typeCheck })
    }

    if (typeCheck == true) {
      let tempSort = [...tempCheck].sort((a, b) => {
        if (a.results.length == 0 || b.results.length == 0) {
          return
        } else {
          return Number(b.results[col]) - Number(a.results[col])
        }
      })
      for (let i = 0; i < tempSort.length; i++) {
        if (tempSort[i].results.length > 0) {
          tempSort[i].results = tempSort[i].results.map((item) => {
            if (Number(item)) {
              return item
            } else {
              return item.replace("0", "-")
            }
          })
        }
      }
      await this.setState({ dataSort: tempSort })
    } else {
      let tempSort = [...tempCheck].sort((a, b) => {
        if (a.results.length == 0 || b.results.length == 0) {
          return
        } else {
          return Number(a.results[col]) - Number(b.results[col])
        }
      })

      let tempArr = []
      let tempUnUse = []
      for (let i = 0; i < tempSort.length; i++) {
        if (tempSort[i].results.length > 0 && tempSort[i].results[col] !== "0") {
          tempArr.push(tempSort[i])
        } else {
          tempUnUse.push(tempSort[i])
        }
      }
      let totalArr = tempArr.concat(tempUnUse)
      for (let i = 0; i < totalArr.length; i++) {
        if (totalArr[i].results.length > 0) {
          totalArr[i].results = totalArr[i].results.map((item) => {
            if (Number(item)) {
              return item
            } else {
              return item.replace("0", "-")
            }
          })
        }
      }
      await this.setState({ dataSort: totalArr })
    }
  }

  showOriginalData = async () => {
    let data = this.props.dataTableInterestRateBank
    let tempCheck = [...data].sort((a, b) => b.results.length - a.results.length)
    for (let i = 0; i < tempCheck.length; i++) {
      if (tempCheck[i].results.length > 0) {
        tempCheck[i].results = tempCheck[i].results.map((item) => {
          if (item.result) {
            return item.result
          } else {
            return item
          }
        })
      }
    }
    await this.setState({ dataSort: tempCheck })
  }
  getMaxEveryCol = async (dataSort) => {
    let everyCol = { "0": 0, "1": 0, "2": 0, "3": 0, "4": 0, "5": 0, "6": 0, "7": 0, "8": 0, }
    if (dataSort !== null) {
      dataSort.map((item, index) => {
        item.results.map((subItem, indx) => {
          everyCol[indx] = Math.max(subItem !== "-" ? subItem : 0, everyCol[indx])
        })
      })
    }
    
    await this.setState({ checkRender: true, maxCol: everyCol })
  }


  componentDidMount() {
    if (this.props.savingRateByBank.length == 0) {
      this.sortDataTable(this.state.sortType, 5)
    } else {
      this.showOriginalData()
    }
  }
  render() {
    if (this.state.dataSort !== null && !this.state.checkRender) {
      this.getMaxEveryCol(this.state.dataSort);
    }

    return (
      <div className="table-data-by-bank">
        {this.props.savingRateByBank.length > 0 && <h1 className="table-data-title-by-bank">{`LÃI SUẤT GỬI TIẾT KIỆM ${this.props.savingRateByBank[0].bankName}`}</h1>}
        <div className="description-table">
          <p>{`(Cập nhật mới nhất ngày ${this.checkRenderTimeDate(new Date())})`}</p>
          <p style={{ color: "#F60707" }}>Lãi suất cuối kỳ: %/năm</p>
        </div>
        <div className="custom-table-interest-by-bank">
          <Table responsive>
            <thead>
              <tr>
                <th rowSpan={2}>Ngân hàng</th>
                <th colSpan={9}>Kỳ hạn gửi tiết kiệm (Tháng)</th>
              </tr>
              <tr>
                <th onClick={() => this.setState({ sortType: !this.state.sortType }, () => this.sortDataTable(this.state.sortType, 0))}>
                  <div className="sort-thead" style={{ display: this.props.savingRateByBank.length == 0 ? "flex" : "block" }}>
                    <span>Không<br />kỳ hạn</span>
                    {this.props.savingRateByBank.length == 0 && <div className="sort-box-icon">
                      <i className="fa fa-caret-up" aria-hidden="true" style={{ color: this.state.col == 0 && this.state.sortType ? "#FFFFFF" : "rgba(255, 255, 255, 0.2)" }}></i>
                      <i className="fa fa-caret-down" aria-hidden="true" style={{ color: this.state.col == 0 && !this.state.sortType ? "#FFFFFF" : "rgba(255, 255, 255, 0.2)" }}></i>
                    </div>}
                  </div>
                </th>
                <th onClick={() => this.setState({ sortType: !this.state.sortType }, () => this.sortDataTable(this.state.sortType, 1))}>
                  <div className="sort-thead" style={{ display: this.props.savingRateByBank.length == 0 ? "flex" : "block" }}>
                    <span>1<br />Tháng</span>
                    {this.props.savingRateByBank.length == 0 && <div className="sort-box-icon">
                      <i className="fa fa-caret-up" aria-hidden="true" style={{ color: this.state.col == 1 && this.state.sortType ? "#FFFFFF" : "rgba(255, 255, 255, 0.2)" }}></i>
                      <i className="fa fa-caret-down" aria-hidden="true" style={{ color: this.state.col == 1 && !this.state.sortType ? "#FFFFFF" : "rgba(255, 255, 255, 0.2)" }}></i>
                    </div>}
                  </div>
                </th>
                <th onClick={() => this.setState({ sortType: !this.state.sortType }, () => this.sortDataTable(this.state.sortType, 2))}>
                  <div className="sort-thead" style={{ display: this.props.savingRateByBank.length == 0 ? "flex" : "block" }}>
                    <span>3<br />Tháng</span>
                    {this.props.savingRateByBank.length == 0 && <div className="sort-box-icon">
                      <i className="fa fa-caret-up" aria-hidden="true" style={{ color: this.state.col == 2 && this.state.sortType ? "#FFFFFF" : "rgba(255, 255, 255, 0.2)" }}></i>
                      <i className="fa fa-caret-down" aria-hidden="true" style={{ color: this.state.col == 2 && !this.state.sortType ? "#FFFFFF" : "rgba(255, 255, 255, 0.2)" }}></i>
                    </div>}
                  </div>
                </th>
                <th onClick={() => this.setState({ sortType: !this.state.sortType }, () => this.sortDataTable(this.state.sortType, 3))}>
                  <div className="sort-thead" style={{ display: this.props.savingRateByBank.length == 0 ? "flex" : "block" }}>
                    <span>6<br />Tháng</span>
                    {this.props.savingRateByBank.length == 0 && <div className="sort-box-icon">
                      <i className="fa fa-caret-up" aria-hidden="true" style={{ color: this.state.col == 3 && this.state.sortType ? "#FFFFFF" : "rgba(255, 255, 255, 0.2)" }}></i>
                      <i className="fa fa-caret-down" aria-hidden="true" style={{ color: this.state.col == 3 && !this.state.sortType ? "#FFFFFF" : "rgba(255, 255, 255, 0.2)" }}></i>
                    </div>}
                  </div>
                </th>
                <th onClick={() => this.setState({ sortType: !this.state.sortType }, () => this.sortDataTable(this.state.sortType, 4))}>
                  <div className="sort-thead" style={{ display: this.props.savingRateByBank.length == 0 ? "flex" : "block" }}>
                    <span>9<br />Tháng</span>
                    {this.props.savingRateByBank.length == 0 && <div className="sort-box-icon">
                      <i className="fa fa-caret-up" aria-hidden="true" style={{ color: this.state.col == 4 && this.state.sortType ? "#FFFFFF" : "rgba(255, 255, 255, 0.2)" }}></i>
                      <i className="fa fa-caret-down" aria-hidden="true" style={{ color: this.state.col == 4 && !this.state.sortType ? "#FFFFFF" : "rgba(255, 255, 255, 0.2)" }}></i>
                    </div>}
                  </div>
                </th>
                <th onClick={() => this.setState({ sortType: !this.state.sortType }, () => this.sortDataTable(this.state.sortType, 5))}>
                  <div className="sort-thead" style={{ display: this.props.savingRateByBank.length == 0 ? "flex" : "block" }}>
                    <span>12<br />Tháng</span>
                    {this.props.savingRateByBank.length == 0 && <div className="sort-box-icon">
                      <i className="fa fa-caret-up" aria-hidden="true" style={{ color: this.state.col == 5 && this.state.sortType ? "#FFFFFF" : "rgba(255, 255, 255, 0.2)" }}></i>
                      <i className="fa fa-caret-down" aria-hidden="true" style={{ color: this.state.col == 5 && !this.state.sortType ? "#FFFFFF" : "rgba(255, 255, 255, 0.2)" }}></i>
                    </div>}
                  </div>
                </th>
                <th onClick={() => this.setState({ sortType: !this.state.sortType }, () => this.sortDataTable(this.state.sortType, 6))}>
                  <div className="sort-thead" style={{ display: this.props.savingRateByBank.length == 0 ? "flex" : "block" }}>
                    <span>13<br />Tháng</span>
                    {this.props.savingRateByBank.length == 0 && <div className="sort-box-icon">
                      <i className="fa fa-caret-up" aria-hidden="true" style={{ color: this.state.col == 6 && this.state.sortType ? "#FFFFFF" : "rgba(255, 255, 255, 0.2)" }}></i>
                      <i className="fa fa-caret-down" aria-hidden="true" style={{ color: this.state.col == 6 && !this.state.sortType ? "#FFFFFF" : "rgba(255, 255, 255, 0.2)" }}></i>
                    </div>}
                  </div>
                </th>
                <th onClick={() => this.setState({ sortType: !this.state.sortType }, () => this.sortDataTable(this.state.sortType, 7))}>
                  <div className="sort-thead" style={{ display: this.props.savingRateByBank.length == 0 ? "flex" : "block" }}>
                    <span>18<br />Tháng</span>
                    {this.props.savingRateByBank.length == 0 && <div className="sort-box-icon">
                      <i className="fa fa-caret-up" aria-hidden="true" style={{ color: this.state.col == 7 && this.state.sortType ? "#FFFFFF" : "rgba(255, 255, 255, 0.2)" }}></i>
                      <i className="fa fa-caret-down" aria-hidden="true" style={{ color: this.state.col == 7 && !this.state.sortType ? "#FFFFFF" : "rgba(255, 255, 255, 0.2)" }}></i>
                    </div>}
                  </div>
                </th>
                <th onClick={() => this.setState({ sortType: !this.state.sortType }, () => this.sortDataTable(this.state.sortType, 8))}>
                  <div className="sort-thead" style={{ display: this.props.savingRateByBank.length == 0 ? "flex" : "block" }}>
                    <span>24<br />Tháng</span>
                    {this.props.savingRateByBank.length == 0 && <div className="sort-box-icon">
                      <i className="fa fa-caret-up" aria-hidden="true" style={{ color: this.state.col == 8 && this.state.sortType ? "#FFFFFF" : "rgba(255, 255, 255, 0.2)" }}></i>
                      <i className="fa fa-caret-down" aria-hidden="true" style={{ color: this.state.col == 8 && !this.state.sortType ? "#FFFFFF" : "rgba(255, 255, 255, 0.2)" }}></i>
                    </div>}
                  </div>
                </th>
              </tr>
            </thead>
            <tbody>
              {this.state.dataSort && this.state.dataSort.map((item, index) => (
                <tr key={index}>
                  <td>{item.bankName ? <div><a href={`/lai-suat-gui-tiet-kiem-${item.slug}`}>{item.bankName}</a></div> : "-"}</td>
                  {item.results && item.results.length > 0 ? item.results.map((subItem, indx) => (
                    <td style={{ color: (subItem * 100) / 100 === (this.state.maxCol[indx] * 100) / 100 ? "red" : "#212529" }}
                      key={indx}>{Number(subItem) ? `${(subItem * 100) / 100}%` : `${subItem}`}</td>
                  ))
                    : <td colSpan={9}>Đang cập nhật...</td>}
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
      </div>
    )
  }
}

export default TableDataInterestByBank;