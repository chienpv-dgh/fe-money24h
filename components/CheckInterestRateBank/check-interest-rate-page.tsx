import React, { Component } from 'react';
import dynamic from "next/dynamic";
import { BoxChooseBank, TableDataInterestByBank } from '.';
import { ContentPageSaving } from '../SavingInterest';
import PopularInterestBank from './popular-bank';

const ArticleInterested = dynamic(() => import('../widgets/ArticleInterested/article-interested'));
const GlobalToolBox = dynamic(() => import("../widgets/GlobalToolBox"));
export interface quoteObject{
  fully_diluted_market_cap: number;
  last_updated: string;
  market_cap: number;
  market_cap_dominance: number;
  percent_change_1h: number;
  percent_change_7d: number;
  percent_change_24h: number;
  percent_change_30d: number;
  percent_change_60d: number;
  percent_change_90d: number;
  price: number;
  volume_24h: number;
  volume_change_24h: number;
}
interface IRecipeProps {
  lastestPost?: any;
  listBankOps?: any;
  savingRateAllBank?: any;
  savingRateByBank?: any;
  dataTableBank?: any;
  postOfPage?: any;
  stockCode?: string;
  detalNewsPost?: any;
  dataCrypto: {
    status: {
        timestamp: string;
        total_count: number;
    };
    data: {
      circulating_supply: number;
      cmc_rank: number;
      id: number;
      last_updated: string;
      max_supply: number;
      total_supply: number;
      name: string;
      num_market_pairs: number;
      slug: string;
      symbol: string;
      quote: quoteObject[];
    }[];
  };
}

interface IRecipeState {
  dataTableInterestRateBank?: any;
}

class CheckInterestBankPage extends Component<IRecipeProps, IRecipeState> {
  constructor(props) {
    super(props);
    this.state = {
      dataTableInterestRateBank: null,
    }
  }

  getDataTable = (data) => {
    if (data) {
      this.setState({ dataTableInterestRateBank: data })
    }
  }

  render() {
    return (
      <div className="interest-rate-of-bank-page-container">
        <BoxChooseBank getDataTable={(data) => this.getDataTable(data)} listBankOps={this.props.listBankOps} dataTableInterestRateBank={this.props.dataTableBank} savingRateByBank={this.props.savingRateByBank} />
        {this.props.dataTableBank && <TableDataInterestByBank dataTableInterestRateBank={this.props.dataTableBank} savingRateAllBank={this.props.savingRateAllBank} savingRateByBank={this.props.savingRateByBank} />}
        <PopularInterestBank dataBank={this.props.savingRateAllBank} />
        <GlobalToolBox />
        <div className="divider-saving-interest-page"></div>
        <ContentPageSaving showAuthor={false} AllPosts={this.props.lastestPost} postOfPage={this.props.detalNewsPost && this.props.detalNewsPost.detail_post ? this.props.detalNewsPost.detail_post : null} stockCode={this.props.stockCode}  dataCrypto={this.props.dataCrypto}/>
        <div className="block-interested-article">
          <p className="interested-article-box-highlight">CÓ THỂ BẠN QUAN TÂM</p>
          <div className="divider-box-highlight"></div>
          <ArticleInterested AllPosts={this.props.lastestPost} />
        </div>
      </div>
    )
  }
}

export default CheckInterestBankPage;