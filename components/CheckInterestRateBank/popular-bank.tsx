import React, { Component } from 'react';

interface IRecipeProps {
  dataBank?: any;
}

interface IRecipeState {
  dataChooseBank?: string[];
  dataBank?: any;
  dataMonth?: string[];
}

class PopularInterestBank extends Component<IRecipeProps, IRecipeState> {
  constructor(props) {
    super(props);
    this.state = {
      dataBank: this.props.dataBank,
      dataMonth: ['Không kỳ hạn', '1 Tháng', '3 Tháng', '6 Tháng', '9 Tháng', '12 Tháng', '13 Tháng', '18 Tháng', '24 Tháng'],
      dataChooseBank: ['Agribank', 'Vietinbank', 'BIDV', 'Vietcombank', 'Sacombank']
    }
  }

  getDataBankByName = (bankName) => {
    let databank = [...this.state.dataBank]
    let getBank = databank.filter((bank) => bank.bankName.includes(bankName))

    if (getBank && getBank.length > 0) {
      return getBank[0]
    } else {
      return null
    }
  }

  getLargetsRateBank = (bankRate) => {
 
    if (bankRate && bankRate.results) {
      let useArr = []
      let dataBankRate = bankRate.results.map((item) => {
        if(item && item.result && Number(item.result)) {
          useArr.push(item.result)
        }
      })

      let rateSort = useArr.sort((a, b) => b - a)
      return rateSort[0]
    } else {
      return null
    }
  } 

  getRateDetail = (bankRate, index) => {
    if (bankRate && bankRate.results) {
      return bankRate.results[index].result
    } else {
      return "-"
    }
  }

  render() {
    
    return (
      <div className="popular-interest-rate">
        <p className='popular-interest-rate-title'>TOP 6 Lãi suất tiền gửi các ngân hàng hiện nay được quan tâm nhất</p>
        {
          this.state.dataChooseBank.map((bank, index) => (
            this.getDataBankByName(bank) && <div key={index} className='popular-bank-item'>
              <p className='popular-bank-item-title'>
                <a href={`/lai-suat-gui-tiet-kiem-${this.getDataBankByName(bank).slug}`}>{`Lãi suất gửi tiết kiệm ngân hàng ${bank}`}</a>
                {` cao nhất: ${this.getLargetsRateBank(this.getDataBankByName(bank))}%`}
              </p>

              <div className='popular-bank-item-table'>
                <div className='box-rate' style={{ backgroundColor: "#f8f8f8"}}>
                  <div className='box-rate-item'>
                    <p>{"Kỳ hạn gửi"}</p>
                  </div>
                  <div className='box-rate-item'>
                    <p>{"Lãi suất (%)"}</p>
                  </div>
                </div>
                {
                  this.state.dataMonth.map((item, indx) => (
                    <div key={indx} className='box-rate' style={{ backgroundColor: indx % 2 !== 0 ? "#f8f8f8" : "#ffffff"}}>
                      <div className='box-rate-item'>
                        <p>{item}</p>
                      </div>
                      <div className='box-rate-item'>
                        <p>{this.getRateDetail(this.getDataBankByName(bank), indx)}</p>
                      </div>
                    </div>
                  ))
                }
              </div>
            </div>
          ))
        }
      </div>
    )
  }
}

export default PopularInterestBank;