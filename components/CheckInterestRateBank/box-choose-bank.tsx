import React, { Component } from 'react';
import { ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';

interface IRecipeProps {
  listBankOps?: any;
  getDataTable?: Function;
  dataTableInterestRateBank?: any;
  savingRateByBank?: any;
}

interface IRecipeState {
  dropdownOpen?: boolean;
  nameBank?: string;
  idBank?: number;
  slugBank?: string;
  searchName?: string;
  arrResult?: any;
}

class BoxChooseBank extends Component<IRecipeProps,IRecipeState> {
  constructor(props) {
    super(props);
    this.state = {
      dropdownOpen: false,
      nameBank: "",
      idBank: null,
      slugBank: "",
      searchName: "",
      arrResult: []
    };
  }

  searchBank = (value) => {
    let arrResult = []
    this.props.listBankOps.map((item) => {
      if(item.searchName) {
        let arrSearch = item.searchName
        if (arrSearch.toLowerCase().search(value.toLowerCase()) !== -1) {
          arrResult.push(item)
        }
      }
    })
    this.setState({ arrResult: arrResult})
  }

  currentMonth = () => {
    const today = new Date();
    const month = today.getMonth();
    return ` tháng ${month + 1}`;
  }

  // getNameBank = (bank) => {
  //   if(bank && bank.length > 0) {
  //     let bankName = bank[0].bankName.split("Ngân hàng").pop()
  //     return bankName
  //   } else return ""
  // }

  render() {
    return (
      <div className="block-choose-bank">
        <p className="bread-scrum">
          <a href="/">Trang chủ</a>
          <i className="fa fa-angle-right bread-scrum-divider" aria-hidden="true"></i>
          <a href={`/tag/${"ngan-hang"}`}>{"Ngân hàng"}</a>
          <i className="fa fa-angle-right bread-scrum-divider post" aria-hidden="true"></i>
          {this.props.savingRateByBank.length == 0 && <span>{"Lãi suất gửi tiết kiệm ngân hàng" + this.currentMonth()}</span>}
          {this.props.savingRateByBank.length > 0 && <a className="tag-link" href={"/lai-suat-gui-tiet-kiem-ngan-hang"}>{"Lãi suất gửi tiết kiệm ngân hàng" + this.currentMonth()}</a>}
          {this.props.savingRateByBank.length > 0 && <i className="fa fa-angle-right bread-scrum-divider post" aria-hidden="true"></i>}
          <span>{this.props.savingRateByBank.length > 0 ? this.props.savingRateByBank[0].bankName : ""}</span>
        </p>
        <div className="contain-choose-bank">
          {this.props.savingRateByBank.length == 0 && <h1 className="choose-bank-main-title">LÃI SUẤT GỬI TIẾT KIỆM NGÂN HÀNG {this.currentMonth().toUpperCase()}</h1>}
          {this.props.savingRateByBank.length > 0 && <p className="choose-bank-main-title">LÃI SUẤT GỬI TIẾT KIỆM NGÂN HÀNG {this.currentMonth().toUpperCase()}</p>}
          <div className="box-choose-bank">
            <p className="choose-bank-label">Ngân hàng *</p>
            <ButtonDropdown className="custom-select-box" isOpen={this.state.dropdownOpen} toggle={() => this.setState({dropdownOpen: !this.state.dropdownOpen})}>
              <DropdownToggle className="custom-select">
                {this.state.nameBank ? this.state.nameBank : this.props.savingRateByBank.length > 0 ? this.props.savingRateByBank[0].bankName : "Chọn ngân hàng"}
              </DropdownToggle>
              <DropdownMenu className="custom-select-menu-box customScroll">
                <div className="box-search-item">
                  <img className="search-image" src="/images/search-icon.svg" alt="search-icon"/>
                  <input type="text" placeholder="Nhập tên, mã ngân hàng" className="custom-search-box" onChange={(e) => this.setState({searchName: e.target.value}, () => this.searchBank(this.state.searchName))}/>
                </div>
                {this.props.listBankOps ?
                  this.state.arrResult.length > 0 ? this.state.arrResult.map((item, index) => (<DropdownItem onClick={() => this.setState({nameBank: item.bankName, idBank: item.id})} key={index} className="menu-box-item"><a href={`/lai-suat-gui-tiet-kiem-${item.slug}`} className="item-tag-link">{item.fullName}</a></DropdownItem>)) : this.props.listBankOps.map((item, index) => (<DropdownItem onClick={() => this.setState({nameBank: item.bankName, idBank: item.id})} key={index} className="menu-box-item"><a href={`/lai-suat-gui-tiet-kiem-${item.slug}`} className="item-tag-link">{item.fullName}</a></DropdownItem>))
                : 
                  <DropdownItem className="menu-box-item">Oops! Data not found.</DropdownItem>
                }
              </DropdownMenu>
              <img className="custom-select-image" src="/images/icon-custom-select.svg" alt="icon-custom-select"/>
            </ButtonDropdown>
          </div>
        </div>
      </div>
    )
  }
}

export default BoxChooseBank;