tocbot.init({
  tocSelector: '.toc',
  contentSelector: '.render-html-page',
  hasInnerContainers: true
});

const compactText = (selector, dodaichuoi) => {
  $(document).ready(function () {
    $(selector).each(function () {
      var text = $(this).html();
      if (text.length > dodaichuoi) {
        text = text.substring(0, dodaichuoi);
        $(this).html(text + ' ...');
      }
    })
  })
}

compactText('.block-body-homepage .block-body-right .interest-bank-block .box-table-data-intterest-bank.borrow-data .data-interest-by-bank .data-interest-table td:last-child', 10)