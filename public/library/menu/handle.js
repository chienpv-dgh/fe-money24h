$(".memu-item-contain-link").on("click", function () {
  
  if ($(this).hasClass("active-item-parent")) {
    $(this).removeClass("active-item-parent");
    $(this).siblings(".sub-item-contain").removeClass("active-sub-item");
  } else {
    $(".memu-item-contain-link").removeClass("active-item-parent")
    $(".sub-item-contain").removeClass("active-sub-item")
    $(".sub-item-contain-link").removeClass("active-item-sub")
    $(".item-child-box").removeClass("active-child-box")
    $(this).addClass("active-item-parent");
    $(this).siblings(".sub-item-contain").addClass("active-sub-item");
  }
})

$(".sub-item-contain-link").on("click", function () {
  if ($(this).hasClass("active-item-sub")) {
    $(this).removeClass("active-item-sub");
    $(this).siblings(".item-child-box").removeClass("active-child-box");
  } else {
    $(".sub-item-contain-link").removeClass("active-item-sub")
    $(".item-child-box").removeClass("active-child-box")
    $(this).addClass("active-item-sub");
    $(this).siblings(".item-child-box").addClass("active-child-box");
  }
})

