/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,

  webpack: (config, { isServer }) => {
    if (!isServer) {
      config.resolve.fallback.fs = false;
    }
    return config
  },

  images: {
    domains: ['money24h.vn', 'admin.money24h.vn'],
  },

  async redirects() {

    return [
      {
        source: "/cong-cu-tinh-ti-gia-ngoai-te",
        destination: '/cong-cu-tinh-ty-gia-ngoai-te',
        permanent: true,
      },
      {
        source: "/blog/3-cach-chuyen-tien-qua-dien-thoai-nhanh-chong",
        destination: '/blog/3-cach-chuyen-tien-dien-thoai-nhanh-chong',
        permanent: true,
      },
      {
        source: "/blog/cach-kich-hoat-the-atm-don-gian-trong-5-phut",
        destination: '/blog/cach-kich-hoat-the-atm-don-gian-nhanh-chong',
        permanent: true,
      },
      {
        source: "/blog/7-cach-mo-tiem-lam-giau-voi-200-trieu-tien-hai-ra-tien",
        destination: '/blog/14-cach-mo-tiem-lam-giau-voi-200-trieu-tien-hai-ra-tien',
        permanent: true,
      },
      {
        source: "/blog/bao-hiem",
        destination: '/tag/bao-hiem',
        permanent: true,
      },
      {
        source: "/bat-dong-san",
        destination: '/tag/bat-dong-san',
        permanent: true,
      },
      {
        source: "/ngan-hang",
        destination: '/tag/ngan-hang',
        permanent: true,
      },
      {
        source: "/lam-giau",
        destination: '/tag/lam-giau',
        permanent: true,
      },
      {
        source: "/thong-tin-ngan-hang",
        destination: '/tag/thong-tin-ngan-hang',
        permanent: true,
      },
      {
        source: "/chung-khoan",
        destination: '/tag/chung-khoan',
        permanent: true,
      },
      {
        source: "/money-weekly",
        destination: '/tag/money-weekly',
        permanent: true,
      },
      {
        source: "/author/truong",
        destination: '/author/bichngan',
        permanent: true,
      },
      {
        source: "/author/nguyen",
        destination: '/',
        permanent: true,
      },
      {
        source: "/author/ta",
        destination: '/',
        permanent: true,
      },
      {
        source: "/author/thanh",
        destination: '/author/thanhbui',
        permanent: true,
      },
      {
        source: "/loan-calculator",
        destination: '/cong-cu-tinh-toan-khoan-vay-ngan-hang',
        permanent: true,
      },
      {
        source: "/check-interest-rate-bank",
        destination: '/lai-suat-gui-tiet-kiem-ngan-hang',
        permanent: true,
      },
      {
        source: "/lam-gi-voi-10-trieu-dong-7-u-tuong-kinh-doanh-via-he-voi-10-trieu-thu-loi-gap-3-dang-thu",
        destination: '/blog/lam-gi-voi-10-trieu-dong-7-cau-tra-loi',
        permanent: true,
      },
      {
        source: "/vo-san-tien-so-tho-nhi-ky-canh-sat-chinh-thuc-phat-lenh-truy-na-ceo-om-2-ty-usd-bo-chay-400-000-nha-dau-co-bo-vo",
        destination: '/vo-san-tien-so-tho-nhi-ky',
        permanent: true,
      },
      {
        source: "/hoc-cach-lam-giau-voi-10-trieu-dong-tu-10-y-tuong-tuyet-voi-2",
        destination: '/hoc-cach-lam-giau-voi-10-trieu-dong-tu-12-y-tuong-tuyet-voi-2',
        permanent: true,
      },
      {
        source: "/huong-dan-cach-nap-tien-vao-the-atm-qua-cay-atm",
        destination: '/huong-dan-cach-nap-tien-qua-the-atm-tu-cay-atm',
        permanent: true,
      },
      {
        source: "/blog/vietinbank-ng-n-hang-thuong-mai-co-phan-cong-thuong-viet-nam",
        destination: '/blog/vietinbank-ngan-hang-thuong-mai-co-phan-cong-thuong-viet-nam',
        permanent: true,
      },
      {
        source: "/blog/vo-san-tien-so-tho-nhi-ky-canh-sat-chinh-thuc-phat-lenh-truy-na-ceo-om-2-ty-usd-bo-chay-400-000-nha-dau-co-bo-vo",
        destination: '/blog/vo-san-tien-so-tho-nhi-ky',
        permanent: true,
      },
      {
        source: "/chuyen-khoan-roi-ma-chua-nhan-duoc-tien",
        destination: '/blog/chuyen-khoan-roi-ma-chua-nhan-duoc-tien-cach-xu-ly',
        permanent: true,
      },
      {
        source: "/ty-gia-ngoai-te-ngan-hang-eximbank",
        destination: '/ty-gia-ngoai-te-hoi-doai-ngan-hang-hom-nay',
        permanent: true,
      },
      {
        source: "/ty-gia-ngoai-te-ngan-hang-standard-chartered",
        destination: '/ty-gia-ngoai-te-hoi-doai-ngan-hang-hom-nay',
        permanent: true,
      },
      {
        source: "/blog/huong-dan-cach-nap-tien-vao-the-atm-qua-cay-atm",
        destination: '/blog/huong-dan-cach-nap-tien-qua-the-atm-tu-cay-atm',
        permanent: true,
      },
      {
        source: "/blog/eximbank",
        destination: '/eximbank-ngan-hang-tmcp-xuat-nhap-khau-viet-nam/',
        permanent: true,
      },
      {
        source: "/blog/lai-suat-tha-noi",
        destination: '/blog/can-trong-khi-chon-hinh-thuc-lai-suat-tha-noi-cua-ngan-hang',
        permanent: true,
      },
      {
        source: "/blog/bao-hiem-nhan-tho-la-gi-co-cac-loai-hinh-bao-hiem-nhan-tho-nao-tren-thi-truong",
        destination: '/blog/bao-hiem-nhan-tho',
        permanent: true,
      },
      {
        source: "/blog/bao-hiem-xa-hoi-la-gi-phan-loai-quyen-loi-va-trach-nhiem",
        destination: '/blog/bao-hiem-xa-hoi',
        permanent: true,
      },
      {
        source: "/blog/3-cach-chuyen-tien-dien-thoai-nhanh-chong",
        destination: '/blog/cac-cach-nap-tien-dien-thoai-nhanh-chong',
        permanent: true,
      },
      {
        source: "/blog/cach-tinh-bao-hiem-xa-hoi-mot-lan-cho-nguoi-lao-dong",
        destination: '/blog/cach-tinh-bao-hiem-xa-hoi-2021',
        permanent: true,
      },
      {
        source: "/blog/cach-thanh-toan-tien-nuoc-va-tien-dien-qua-techcombank-f-st-mobile",
        destination: '/blog/cach-thanh-toan-tien-nuoc-va-tien-dien-qua-techcombank-fast-mobile',
        permanent: true,
      },
      {
        source: "/blog/100-trieu-dau-tu-vao-dau-thu-ngay-7-y-tuong-nhuong-quyen-thuong-hieu-xu-huong-kinh-doanh-sinh-loi-2021",
        destination: '/blog/100-trieu-dau-tu-vao-dau-2021',
        permanent: true,
      },
      {
        source: "/bitcoin",
        destination: '/tag/bitcoin',
        permanent: true,
      },
      {
        source: "/Pgi",
        destination: '/',
        permanent: true,
      },
      {
        source: "/check-currency-by-bank",
        destination: '/lai-suat-gui-tiet-kiem-ngan-hang',
        permanent: true,
      },
      {
        source: "/blog/0%20H",
        destination: '/',
        permanent: true,
      },
      {
        source: "/xfr",
        destination: '/',
        permanent: true,
      },
      {
        source: "/Tr7",
        destination: '/',
        permanent: true,
      },
      {
        source: "/ayr",
        destination: '/',
        permanent: true,
      },
      {
        source: "/ab2",
        destination: '/',
        permanent: true,
      },
      {
        source: "/GRZ",
        destination: '/',
        permanent: true,
      },
      {
        source: "/lai-suat-vay-ngan-hang-vbsp",
        destination: '/lai-suat-vay-ngan-hang',
        permanent: true,
      },
      {
        source: "/trending",
        destination: '/tag/trending',
        permanent: true,
      },
      {
        source: "/check-currency-all",
        destination: '/lai-suat-gui-tiet-kiem-ngan-hang',
        permanent: true,
      },
      {
        source: "/check-currency-by-money",
        destination: '/lai-suat-gui-tiet-kiem-ngan-hang',
        permanent: true,
      },
      {
        source: "/blog/[slug]",
        destination: '/',
        permanent: true,
      },
      {
        source: "/tag/[slug]",
        destination: '/',
        permanent: true,
      },
      {
        source: "/tag/%5Bslug%5D",
        destination: '/',
        permanent: true,
      },
      {
        source: "/blog/%5Bslug%5D",
        destination: '/',
        permanent: true,
      },
      {
        source: "/blog/saving-interest-rate",
        destination: '/cong-cu-tinh-tien-lai-gui-ngan-hang',
        permanent: true,
      },
      {
        source: "/blog/currentcy-calculator",
        destination: '/cong-cu-tinh-ty-gia-ngoai-te',
        permanent: true,
      },
      {
        source: '/:slug',
        destination: '/blog/:slug',
        permanent: true,
      },
      {
        source: '/:slug/amp',
        destination: '/blog/:slug',
        permanent: true,
      },
      {
        source: "/tien-ao",
        destination: '/crypto-currency',
        permanent: true,
      },
      {
        source: "/tien-ao/:slug",
        destination: '/crypto-currency-detail/:slug',
        permanent: true,
      },
    ]
  },
}

module.exports = nextConfig