import { decode } from 'html-entities';
import { SITE_URL, SITE_TITLE, SITE_DESCIPTION} from './constants';
import markdownToHtml from './markdownToHtml';

export async function generateRssItem(post) {
  const content = await markdownToHtml(post.excerpt || '')

  return `
    <item>
      <title>${post.title}</title>
      <description><![CDATA[Việt Nam]]></description>
      <pubDate>${post.publish_date}</pubDate>
      <link>${SITE_URL}/blog/${post.slug}</link>
      <guid>${SITE_URL}/blog/${post.slug}</guid>
    </item>
  `
}

export async function generateRss(posts) {
  const itemsList = await Promise.all(posts.map(generateRssItem))

  return `
    <rss version="2.0" xmlns:slash="http://purl.org/rss/1.0/modules/slash/">
      <channel>
        <title>${SITE_TITLE}</title>
        <description>${SITE_DESCIPTION}</description>
        <pubDate>${new Date().toUTCString()}</pubDate>
        <generator>Money24h</generator>
        <link>${SITE_URL}/rss</link>
        ${itemsList.join('')}
      </channel>
    </rss>
  `
}